Imports System.ComponentModel
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting

''' <summary> Container class to maintain scaling data for each chart's axes. </summary>
''' <remarks>
''' From https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart.
''' </remarks>
Public Class ChartScaleData

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="chart"> The chart. </param>
    Public Sub New(ByVal chart As Chart)
        Me._Chart = chart
    End Sub

    ''' <summary> The chart. </summary>
    Private ReadOnly _Chart As Chart

    Private _XBaseMin, _XBaseMax, _XBaseInt, _XBaseMinorInt As Double

    Private _YBaseMin, _YBaseMax, _YBaseInt, _YBaseMinorInt As Double

    ''' <summary> Gets the is zoomed. </summary>
    ''' <value> The is zoomed. </value>
    Public Property IsZoomed As Boolean = False

    ''' <summary> True if scales are valid. Only true after the chart has been drawn once. </summary>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0052:Remove unread private members", Justification:="<Pending>")>
    Private _ScalesAreValid As Boolean = False

    ''' <summary> Updates the axis base data x coordinate. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    Public Sub UpdateAxisBaseDataX()
        Dim axis As Axis = Me._Chart.ChartAreas.First.AxisX
        Me._XBaseMinorInt = axis.MinorTickMark.Interval
        Me._XBaseInt = axis.Interval
        Me._XBaseMax = axis.Maximum
        Me._XBaseMin = axis.Minimum
    End Sub

    ''' <summary> Updates the axis base data y coordinate. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    Public Sub UpdateAxisBaseDataY()
        Dim axis As Axis = Me._Chart.ChartAreas.First.AxisY
        Me._YBaseMinorInt = axis.MinorTickMark.Interval
        Me._YBaseInt = axis.Interval
        Me._YBaseMax = axis.Maximum
        Me._YBaseMin = axis.Minimum
    End Sub

    ''' <summary> Updates the axis base data. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub UpdateAxisBaseData()
        Me.UpdateAxisBaseDataX()
        Me.UpdateAxisBaseDataY()
        Me._ScalesAreValid = True
    End Sub

    ''' <summary> Resets the axis scale x coordinate. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub ResetAxisScaleX()
        Dim axis As Axis = Me._Chart.ChartAreas.First.AxisX
        axis.MinorTickMark.Interval = Me._XBaseMinorInt
        axis.Interval = Me._XBaseInt
        axis.Maximum = Me._XBaseMax
        axis.Minimum = Me._XBaseMin
    End Sub

    ''' <summary> Resets the axis scale y coordinate. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub ResetAxisScaleY()
        Dim axis As Axis = Me._Chart.ChartAreas.First.AxisY
        axis.MinorTickMark.Interval = Me._YBaseMinorInt
        axis.Interval = Me._YBaseInt
        axis.Maximum = Me._YBaseMax
        axis.Minimum = Me._YBaseMin
    End Sub

    ''' <summary> Resets the axis scale. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub ResetAxisScale()
        Me.ResetAxisScaleX()
        Me.ResetAxisScaleY()
    End Sub

    ''' <summary> Gets the chart area rectangle f. </summary>
    ''' <value> The chart area rectangle f. </value>
    Public ReadOnly Property ChartAreaRectangleF() As RectangleF
        Get
            Dim cr As Rectangle = Me._Chart.ClientRectangle
            Dim rfp As RectangleF = Me._Chart.ChartAreas.First.Position.ToRectangleF()
            ' RFP is the chart area rectangle as percentages of the entire chart ClientRectangle
            Dim chAreaX As Single = rfp.Left * cr.Width / 100.0F
            Dim chAreaY As Single = rfp.Top * cr.Height / 100.0F
            Dim chAreaW As Single = rfp.Width * cr.Width / 100.0F
            Dim chAreaH As Single = rfp.Height * cr.Height / 100.0F
            Return New RectangleF(chAreaX, chAreaY, chAreaW, chAreaH)
        End Get
    End Property

    ''' <summary> Gets the chart area rectangle. </summary>
    ''' <value> The chart area rectangle. </value>
    Public ReadOnly Property ChartAreaRectangle() As Rectangle
        Get
            Return Rectangle.Round(Me.ChartAreaRectangleF)
        End Get
    End Property

    ''' <summary> Gets the inner plot rectangle f. </summary>
    ''' <value> The inner plot rectangle f. </value>
    Public ReadOnly Property InnerPlotRectangleF() As RectangleF
        Get
            ' this is the inner plot area rectangle as percentages of the chart area rectangle 
            Dim rfi As RectangleF = Me._Chart.ChartAreas.First.InnerPlotPosition.ToRectangleF()
            Dim chArea As RectangleF = Me.ChartAreaRectangleF
            Dim ipX As Single = chArea.X + rfi.Left * chArea.Width / 100.0F
            Dim ipY As Single = chArea.Y + rfi.Top * chArea.Height / 100.0F
            Dim ipW As Single = rfi.Width * chArea.Width / 100.0F
            Dim ipH As Single = rfi.Height * chArea.Height / 100.0F
            Return New RectangleF(ipX, ipY, ipW, ipH)
        End Get
    End Property

    ''' <summary> Gets the inner plot rectangle. </summary>
    ''' <value> The inner plot rectangle. </value>
    Public ReadOnly Property InnerPlotRectangle() As Rectangle
        Get
            Return Rectangle.Round(Me.InnerPlotRectangleF)
        End Get
    End Property

End Class

