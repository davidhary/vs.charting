Imports System.ComponentModel
Imports System.Drawing
Imports System.Drawing.Printing
Imports System.Windows.Forms
Imports System.Windows.Forms.DataVisualization.Charting

''' <summary> A line chart. </summary>
''' <remarks>
''' (c) 2016 Darryl Bryk. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/29/2016. Source:
''' http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series. </para>
''' </remarks>
Public Class LineChartControl
    Inherits Chart

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#Region " IDISPOSABLE SUPPORT "

    ''' <summary> Releases unmanaged and, optionally, managed resources. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both unmanaged and managed
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._ChartArea IsNot Nothing Then
                        Me._ChartArea.Dispose()
                        Me._ChartArea = Nothing
                    End If
                    If Me.ContextMenuStrip IsNot Nothing Then
                        Me.ContextMenuStrip.Items?.Clear()
                        Me.ZoomOutToolStripMenuItem?.Dispose()
                        Me.NiceRoundNumbersToolStripMenuItem?.Dispose()
                        Me.ZoomWithGDI32ToolStripMenuItem?.Dispose()
                        Me.DefaultFormatToolStripMenuItem?.Dispose()
                        Me.SmartFormatToolStripMenuItem?.Dispose()
                        Me.WholeNumberFormatToolStripMenuItem?.Dispose()
                        Me.SeparatorToolStripItem?.Dispose()
                        Me.SeparatorToolStripItem1?.Dispose()
                    End If
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " INITIALIZE KNOWN STATE "

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="zoomable"> True if zoomable. </param>
    Public Sub InitializeKnownState(ByVal zoomable As Boolean)
        Me.InitializeKnownState(LineChartControl.ChartAreaName, zoomable)
    End Sub

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="areaName"> Name of the area. </param>
    ''' <param name="zoomable"> True if zoomable. </param>
    Public Sub InitializeKnownState(ByVal areaName As String, ByVal zoomable As Boolean)
        Me.ChartAreas.Clear()
        Me.Clear()
        Me.InitializeChartArea(areaName, zoomable)
        Me.InitializeCustomPalette()
    End Sub

#End Region

#Region " AREA "

    ''' <summary> Name of the chart area. </summary>
    Public Const ChartAreaName As String = "Area"

    ''' <summary> Gets or sets the chart area. </summary>
    ''' <value> The chart area. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property ChartArea As ChartArea

    ''' <summary> Initializes the chart area. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="areaName"> Name of the area. </param>
    ''' <param name="zoomable"> True if zoomable. </param>
    Public Sub InitializeChartArea(ByVal areaName As String, ByVal zoomable As Boolean)

        Me._ChartArea = If(Me.ChartAreas.IsUniqueName(areaName),
            Me.ChartAreas.Add(LineChartControl.ChartAreaName),
            Me.ChartAreas.Item(LineChartControl.ChartAreaName))

        ' Enable range selection and zooming
        Me.ChartArea.CursorX.IsUserEnabled = zoomable
        Me.ChartArea.CursorX.IsUserSelectionEnabled = zoomable
        Me.ChartArea.AxisX.ScaleView.Zoomable = zoomable
        Me.ChartArea.AxisX.ScrollBar.IsPositionedInside = True
        Me.ChartArea.AxisX.ScaleView.SmallScrollSize = 1
        Me.ChartArea.AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount
        Me.ChartArea.AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount

        Me.ChartArea.AxisX.LabelStyle.Format = "F0"
        Me.ChartArea.AxisX.LabelStyle.IsEndLabelVisible = True
        Me.ChartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(200, 200, 200)

        Me.ChartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(200, 200, 200)
        Me.ChartArea.AxisX.LabelStyle.Font = New Font(Me.ChartArea.AxisX.LabelStyle.Font.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisY.LabelStyle.Font = New Font(Me.ChartArea.AxisY.LabelStyle.Font.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisX.TitleFont = New Font(Me.ChartArea.AxisX.TitleFont.Name, 9, FontStyle.Regular)
        Me.ChartArea.AxisY.TitleFont = New Font(Me.ChartArea.AxisY.TitleFont.Name, 9, FontStyle.Regular)
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub Clear()
        Me.Series.Clear()
        Me.Titles.Clear()
        Me.Legends.Clear()
    End Sub

    ''' <summary> Last chart area. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <returns> A ChartArea. </returns>
    Private Function LastChartArea() As ChartArea
        Return Me.ChartAreas(Me.ChartAreas.Count() - 1)
    End Function

#End Region

#Region " AXIS: ABSCISSA "

    ''' <summary> Axis x coordinate title. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub AbscissaTitle(ByVal value As String)
        Me.ChartArea.AxisX.Title = value
    End Sub

#End Region

#Region " AXIS: ORDINATE "

    ''' <summary> Axis y coordinate title. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub OrdinateTitle(ByVal value As String)
        If String.IsNullOrEmpty(Me.ChartArea.AxisY.Title) Then
            Me.ChartArea.AxisY.Title = value
        ElseIf Not Me.ChartArea.AxisY.Title.Contains(value) Then
            Me.ChartArea.AxisY.Title = $"{Me.ChartArea.AxisY.Title}, {value}" ' Append
        End If
    End Sub

#End Region

#Region " LEGEND "

    ''' <summary> Name of the chart legend. </summary>
    Public Const MainLegendName As String = "Legend"

    ''' <summary> Gets or sets the main legend. </summary>
    ''' <value> The main legend. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property MainLegend As Legend

    ''' <summary> Adds main legend. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub AddMainLegend()
        Me._MainLegend = Me.Legends.Add(LineChartControl.MainLegendName)
        Me._MainLegend.Docking = Docking.Bottom
        Me._MainLegend.Font = New Font(Me.Legends(0).Font.FontFamily, 8) ' Font size
        Me._MainLegend.IsTextAutoFit = True
    End Sub

#End Region

#Region " LINE ANNOTATION "

    ''' <summary> Adds t=0 line annotation to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub GraphOriginLine()
        Me.GraphOriginLine("t=0")
    End Sub

    ''' <summary> Adds t=0 line annotation to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="caption"> The caption. </param>
    Public Sub GraphOriginLine(ByVal caption As String)
        Me.GraphLineAnnotations(caption, 0, Color.Black, True, ChartDashStyle.Dash)
    End Sub

    ''' <summary> Adds line annotation to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="anchorX">      The x coordinate. </param>
    ''' <param name="lineColor">    The lineColor. </param>
    ''' <param name="labelEnabled"> true to label. </param>
    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color,
                                    labelEnabled As Boolean)
        Me.GraphLineAnnotations(name, anchorX, lineColor, labelEnabled, ChartDashStyle.Solid)
    End Sub

    ''' <summary> Adds line annotation to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="name">      The name. </param>
    ''' <param name="anchorX">   The x coordinate. </param>
    ''' <param name="lineColor"> The lineColor. </param>
    ''' <param name="lineStyle"> the line style. </param>
    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color,
                                    lineStyle As ChartDashStyle)
        Me.GraphLineAnnotations(name, anchorX, lineColor, False, lineStyle)
    End Sub

    ''' <summary> Adds line annotation to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="anchorX">      The x coordinate. </param>
    ''' <param name="lineColor">    The lineColor. </param>
    ''' <param name="labelEnabled"> true to label. </param>
    ''' <param name="lineStyle">    the line style. </param>
    Public Sub GraphLineAnnotations(ByVal name As String, ByVal anchorX As Double,
                                    ByVal lineColor As Color, ByVal labelEnabled As Boolean,
                                    ByVal lineStyle As ChartDashStyle)
        For Each ann As Annotation In Me.Annotations
            If ann.Name = name Then ' Don't duplicate
                Return
            End If
        Next ann

        Dim annot As New VerticalLineAnnotation() With {
            .AxisX = Me.ChartArea.AxisX,
            .AxisY = Me.ChartArea.AxisY,
            .IsInfinitive = True,
            .ClipToChartArea = Me.ChartArea.Name,
            .LineWidth = 1,
            .LineColor = lineColor,
            .LineDashStyle = lineStyle,
            .ToolTip = name
        }
        annot.Name = annot.ToolTip
        annot.AnchorX = anchorX
        Me.Annotations.Add(annot)

        If labelEnabled Then
            Dim a As New RectangleAnnotation() With {
                .Text = name,
                .AxisX = annot.AxisX,
                .AxisY = annot.AxisY,
                .AnchorX = anchorX,
                .LineColor = Color.Transparent,
                .BackColor = Color.Transparent,
                .ForeColor = lineColor
            }

            Dim textw As Double = TextRenderer.MeasureText(name, a.Font).Width + 1.0F
            Dim texth As Double = TextRenderer.MeasureText(name, a.Font).Height + 1.0F
            a.Width = (textw / Me.Width) * 100
            a.Height = (texth / Me.Height) * 100
            a.X -= Math.Floor(0.5 * a.Width)
            Me.ChartArea.RecalculateAxesScale() ' Needed so AxisY.Maximum != NaN
            a.Y = Me.ChartArea.AxisY.Maximum
            Me.Annotations.Add(a)

            ' Handle resize - annotation sized as % of chart size
            AddHandler Me.Resize, Sub(sender, e)
                                      a.Width = (textw / Me.Width) * 100
                                      a.Height = (texth / Me.Height) * 100
                                  End Sub
        End If
    End Sub

#End Region

#Region " SERIES "

    ''' <summary> Number of series in the chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <returns> An Integer. </returns>
    Public Function SeriesCount() As Integer
        Return Me.Series.Count
    End Function

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="seriesName"> The series name. </param>
    Public Sub Clear(ByVal seriesName As String)
        If Not Me.Series.IsUniqueName(seriesName) Then Me.Series.Item(seriesName).Points.Clear()
    End Sub

    ''' <summary> Next series name. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <returns> A String. </returns>
    Public Function NextSeriesName() As String
        Return $"Series#{Me.SeriesCount + 1}"
    End Function

    ''' <summary> Adds the series. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="seriesName"> The series name. </param>
    ''' <returns> The Series. </returns>
    Public Function AddSeries(ByVal seriesName As String) As Series
        Dim ser As New Series(Me.NextSeriesName)
        If Me.Series.IsUniqueName(seriesName) Then
            Me.Series.Add(ser)
        Else
            ser = Me.Series.Item(seriesName)
        End If
        Return ser
    End Function

    ''' <summary> Adds the series. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="seriesName">  The series name. </param>
    ''' <param name="chartArea">   The chart area. </param>
    ''' <param name="seriesColor"> The series color. </param>
    ''' <returns> The Series. </returns>
    Public Function AddSeries(ByVal seriesName As String, ByVal chartArea As String, ByVal seriesColor As Color) As Series
        Dim ser As New Series(Me.NextSeriesName)
        If Me.Series.IsUniqueName(seriesName) Then
            Me.Series.Add(ser)
        Else
            ser = Me.Series.Item(seriesName)
        End If
        ser.ChartArea = chartArea
        ser.Color = seriesColor
        Return ser
    End Function

    ''' <summary> Adds the series. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <returns> The Series. </returns>
    Public Function AddSeries() As Series
        Return Me.AddSeries(Me.NextSeriesName, Me.LastChartArea.Name, Me.NextSeriesColor)
    End Function

#End Region

#Region " SERIES: BIND "

    ''' <summary> Bind line series. Call <see cref="Chart.DataBind()"/> to update the chart. </summary>
    ''' <remarks> David, 2020-06-27. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName"> The series name. </param>
    ''' <param name="chartType">  Type of the chart. </param>
    ''' <param name="points">     The points. </param>
    ''' <returns> The Series. </returns>
    Public Function BindLineSeries(ByVal seriesName As String, ByVal chartType As SeriesChartType,
                                   ByVal points As IList(Of Windows.Point)) As Series
        If points Is Nothing Then Throw New ArgumentNullException(NameOf(points))
        If String.IsNullOrWhiteSpace(seriesName) Then Throw New ArgumentNullException(NameOf(seriesName))
        Dim ser As Series
        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            If Me.Series.IsUniqueName(seriesName) Then
                Me.Series.Add(New Series(seriesName))
                ser = Me.Series.Item(seriesName)
            Else
                ser = Me.Series.Item(seriesName)
                ser.Points.Clear()
            End If
            ser.ChartType = chartType
            If Me.ChartAreas.Count() > 1 Then ' Bind to last chart area
                ser.ChartArea = Me.ChartAreas(Me.ChartAreas.Count() - 1).Name
            End If
            Me.DataSource = points
            ser.XValueMember = NameOf(Windows.Point.X)
            ser.YValueMembers = NameOf(Windows.Point.Y)
        Catch e1 As ArgumentException ' Handle redundant series
            Return Nothing
        Catch
            Throw
        End Try
        System.Windows.Forms.Cursor.Current = Cursors.Default
        Return ser
    End Function

#End Region

#Region " SERIES: DRAW "

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName"> The series name. </param>
    ''' <param name="chartType">  Type of the chart. </param>
    ''' <param name="points">     The points. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal chartType As SeriesChartType,
                                    ByVal points As IList(Of Windows.Point)) As Series
        If points Is Nothing OrElse Not points.Any Then Throw New ArgumentNullException(NameOf(points))
        If String.IsNullOrWhiteSpace(seriesName) Then Throw New ArgumentNullException(NameOf(seriesName))
        Dim ser As Series
        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            If Me.Series.IsUniqueName(seriesName) Then
                Me.Series.Add(New Series(seriesName))
                ser = Me.Series.Item(seriesName)
            Else
                ser = Me.Series.Item(seriesName)
                ser.Points.Clear()
            End If
            ser.ChartType = chartType
            If Me.ChartAreas.Count() > 1 Then ' Bind to last chart area
                ser.ChartArea = Me.ChartAreas(Me.ChartAreas.Count() - 1).Name
            End If
            Me.DataSource = points
            ser.XValueMember = NameOf(Windows.Point.X)
            ser.YValueMembers = NameOf(Windows.Point.Y)

            ' set points one-by-one
            ' For Each p As Windows.Point In points
            '     ser.Points.Add(New DataPoint(p.X, p.Y))
            ' Next

        Catch e1 As ArgumentException ' Handle redundant series
            Return Nothing
        Catch
            Throw
        End Try
        System.Windows.Forms.Cursor.Current = Cursors.Default
        Return ser
    End Function

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName">     The series name. </param>
    ''' <param name="chartType">      Type of the chart. </param>
    ''' <param name="abscissaValues"> The x coordinate values. </param>
    ''' <param name="ordinateValues"> The y coordinate values. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal chartType As SeriesChartType,
                                    ByVal abscissaValues() As Double, ByVal ordinateValues() As Double) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Return Me.GraphLineSeries(seriesName, chartType, abscissaValues, ordinateValues, False, Color.Empty, True)
    End Function

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName">      The series name. </param>
    ''' <param name="chartType">       Type of the chart. </param>
    ''' <param name="abscissaValues">  The x coordinate values. </param>
    ''' <param name="ordinateValues">  The y coordinate values. </param>
    ''' <param name="title">           The title. </param>
    ''' <param name="abscissaTitle">   The abscissa title. </param>
    ''' <param name="ordinateTitle">   The ordinate title. </param>
    ''' <param name="addMinMaxLabels"> true to add minimum and maximum labels. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal chartType As SeriesChartType,
                                    ByVal abscissaValues() As Double, ByVal ordinateValues() As Double,
                                    ByVal title As String, ByVal abscissaTitle As String, ByVal ordinateTitle As String,
                                    ByVal addMinMaxLabels As Boolean) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Me.AddTitle(title, Docking.Top)
        Me.OrdinateTitle(ordinateTitle)
        Me.AbscissaTitle(abscissaTitle)
        Return Me.GraphLineSeries(seriesName, chartType, abscissaValues, ordinateValues, addMinMaxLabels, Color.Empty, True)
    End Function

    ''' <summary> Adds line series to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName">        The series name. </param>
    ''' <param name="chartType">         Type of the chart. </param>
    ''' <param name="abscissaValues">    The x coordinate values. </param>
    ''' <param name="ordinateValues">    The y coordinate values. </param>
    ''' <param name="addMinMaxLabels">   true to add minimum and maximum labels. </param>
    ''' <param name="seriesColor">       The series color. </param>
    ''' <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphLineSeries(ByVal seriesName As String, ByVal chartType As SeriesChartType,
                                    ByVal abscissaValues() As Double, ByVal ordinateValues() As Double,
                                    ByVal addMinMaxLabels As Boolean, ByVal seriesColor As Color,
                                    ByVal isVisibleInLegend As Boolean) As Series

        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        Dim ser As New Series(seriesName)

        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Try
            Me.Series.Add(ser)
            ser.ChartType = chartType
            If Me.ChartAreas.Count() >= 1 Then ' Bind to last chart area
                ser.ChartArea = Me.ChartAreas(Me.ChartAreas.Count() - 1).Name
            End If
            ser.Points.DataBindXY(abscissaValues, ordinateValues)
        Catch e1 As ArgumentException ' Handle redundant series
            Return Nothing
        Catch
            Throw
        End Try

        ser.IsVisibleInLegend = isVisibleInLegend
        ser.Color = seriesColor

        If addMinMaxLabels Then ' Add min, max labels
            Me.ApplyPaletteColors() ' Force color assign so labels match
            LineChartControl.SmartLabels(ser, ser.Color, ser.Color, FontStyle.Regular, 7)
            ser.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.TopLeft

            Dim p As DataPoint = ser.Points.FindMaxByValue()
            p.Label = "Max = " & p.YValues(0).ToString("F1")
            p = ser.Points.FindMinByValue()
            p.Label = "Min = " & p.YValues(0).ToString("F1")
        End If

        System.Windows.Forms.Cursor.Current = Cursors.Default

        Return ser
    End Function

    ''' <summary> Graph range series. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName">           The series name. </param>
    ''' <param name="abscissaValues">       The x coordinate values. </param>
    ''' <param name="ordinateValues">       The y coordinate values. </param>
    ''' <param name="ordinateHeightValues"> The ordinate height values. </param>
    ''' <param name="seriesColor">          The series color. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphRangeSeries(ByVal seriesName As String, ByVal abscissaValues() As Double, ByVal ordinateValues() As Double,
                                     ByVal ordinateHeightValues() As Double, ByVal seriesColor As Color) As Series
        If abscissaValues Is Nothing Then Throw New ArgumentNullException(NameOf(abscissaValues))
        If ordinateValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateValues))
        If ordinateHeightValues Is Nothing Then Throw New ArgumentNullException(NameOf(ordinateHeightValues))
        System.Windows.Forms.Cursor.Current = Cursors.WaitCursor
        Dim ser As New Series(seriesName)

        Try
            Me.Series.Add(ser)
            If Me.ChartAreas.Count() > 1 Then ' Bind to last chart area
                ser.ChartArea = Me.ChartAreas(Me.ChartAreas.Count() - 1).Name
            End If
            ser.ChartType = SeriesChartType.Range
            ser.IsVisibleInLegend = False
            ser.Color = seriesColor

            ser.Points.DataBindXY(abscissaValues, ordinateValues, ordinateHeightValues)
        Catch e1 As ArgumentException
            ' Handles redundant series
            Return Nothing
        Catch
            Throw
        End Try
        System.Windows.Forms.Cursor.Current = Cursors.Default

        Return ser
    End Function

    ''' <summary> Graphs series points style to chart. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="seriesName">        Name of the series. </param>
    ''' <param name="abscissa">          The abscissa values. </param>
    ''' <param name="ordinate">          The ordinate values. </param>
    ''' <param name="marker">            The marker. </param>
    ''' <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
    ''' <returns> The Series. </returns>
    Public Function GraphPoints(ByVal seriesName As String, ByVal abscissa() As Double,
                                ByVal ordinate() As Double, ByVal marker As ChartMarker,
                                ByVal isVisibleInLegend As Boolean) As Series
        If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
        If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
        Dim ser As New Series(seriesName)
        If Me.Series.IsUniqueName(seriesName) Then
            Me.Series.Add(ser)
        Else
            ser = Me.Series.Item(seriesName)
        End If
        ser.ChartArea = Me.LastChartArea.Name
        ser.ChartType = SeriesChartType.Point
        ser.IsVisibleInLegend = isVisibleInLegend
        'ser.MarkerBorderWidth = LineWeight;
        'ser.BorderWidth = LineWeight;
        ser.Points.DataBindXY(abscissa, ordinate)
        ser.MarkerStyle = marker.Style
        ser.MarkerSize = marker.Size
        ser.MarkerColor = marker.Color
        Return ser
    End Function

#End Region

#Region " SERIES SMART LABELS  "

    ''' <summary> Setup Smart labels. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="chartingSeries"> The charting series. </param>
    ''' <param name="labelColor">     The label color. </param>
    ''' <param name="lineColor">      The lineColor. </param>
    ''' <param name="fontStyle">      The font style. </param>
    ''' <param name="fontSize">       size of the font. </param>
    Private Shared Sub SmartLabels(ByVal chartingSeries As Series, ByVal labelColor As Color,
                                   ByVal lineColor As Color, ByVal fontStyle As FontStyle,
                                   ByVal fontSize As Integer)
        If chartingSeries Is Nothing Then Throw New ArgumentNullException(NameOf(chartingSeries))
        chartingSeries.LabelForeColor = labelColor
        chartingSeries.Font = New Font(chartingSeries.Font.Name, fontSize, fontStyle)
        chartingSeries.SmartLabelStyle.Enabled = True
        chartingSeries.SmartLabelStyle.CalloutLineColor = lineColor
        chartingSeries.SmartLabelStyle.CalloutStyle = LabelCalloutStyle.None
        chartingSeries.SmartLabelStyle.IsMarkerOverlappingAllowed = False
        chartingSeries.SmartLabelStyle.MinMovingDistance = 1
        chartingSeries.SmartLabelStyle.IsOverlappedHidden = False
        chartingSeries.SmartLabelStyle.AllowOutsidePlotArea = LabelOutsidePlotAreaStyle.No
        chartingSeries.SmartLabelStyle.CalloutLineAnchorCapStyle = LineAnchorCapStyle.None
    End Sub

#End Region

#Region " SETTINGS "

    ''' <summary> Gets or sets a list of colors of the use twenties. </summary>
    ''' <value> A list of colors of the use twenties. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property UseTwentyColors As Boolean

    ''' <summary> The series colors 10. </summary>
    ''' <remarks> plot colors (HTTPS://github.com/vega/vega/wiki/Scales#scale-range-literals)</remarks>
    Private ReadOnly _SeriesColors10 As String() = New String() {"#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b",
                                                                 "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"}

    ''' <summary> The series colors 20. </summary>
    Private ReadOnly _SeriesColors20 As String() = New String() {"#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a",
                                                                 "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94",
                                                                 "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5"}

    ''' <summary> Next series color. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <returns> A Color. </returns>
    Public Function NextSeriesColor() As Color
        Dim colors As String() = If(Me.UseTwentyColors, Me._SeriesColors20, Me._SeriesColors10)
        Return ColorTranslator.FromHtml(colors(Me.SeriesCount Mod colors.Length))
    End Function

    ''' <summary> Initializes the chart Palette. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub InitializeCustomPalette()
        Me.Palette = ChartColorPalette.None
        Me.PaletteCustomColors = New Color() {Color.Blue, Color.DarkRed, Color.OliveDrab,
                                              Color.DarkOrange, Color.Purple, Color.Turquoise,
                                              Color.Green, Color.Crimson, Color.RoyalBlue,
                                              Color.Sienna, Color.Teal, Color.YellowGreen,
                                              Color.SandyBrown, Color.DeepSkyBlue,
                                              Color.Brown, Color.Cyan}
    End Sub

#End Region

#Region " TITLE "

    ''' <summary>
    ''' Add graph title. Repeated calls append new titles if unique or if new position.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="value"> The title. </param>
    Public Sub AddTitle(ByVal value As String)
        ' Defaults to top, black
        Me.AddTitle(value, Docking.Top, Color.Black)
    End Sub

    ''' <summary>
    ''' Add graph title. Repeated calls append new titles if unique or if new position.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="value">    The title. </param>
    ''' <param name="position"> The position. </param>
    Public Sub AddTitle(ByVal value As String, ByVal position As Docking)
        Me.AddTitle(value, position, Color.Black)
    End Sub

    ''' <summary>
    ''' Add graph title. Repeated calls append new titles if unique or if new position.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="value">      The title. </param>
    ''' <param name="position">   The position. </param>
    ''' <param name="titleColor"> The title color. </param>
    Public Sub AddTitle(ByVal value As String, ByVal position As Docking, ByVal titleColor As Color)

        For Each t As Title In Me.Titles
            If t.Docking = position Then
                If Not t.Text.Contains(value) Then ' Append
                    t.Text = $"{t.Text}{vbLf}{value}"
                End If
                Return
            End If
        Next t

        ' If here, no match, add new title
        Dim newtitle As New Title(value, position)
        Me.Titles.Add(newtitle)
        newtitle.ForeColor = titleColor
        'newtitle.Font = new Font(newtitle.Font.Name, 8, FontStyle.Bold);
    End Sub

#End Region

#Region " TOOL TIP HANDLER "

    ''' <summary> Event handler. Called by chart for tool tip events. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tool tip event information. </param>
    Public Sub OnGetTooltipText(ByVal sender As Object, ByVal e As ToolTipEventArgs)
        If e Is Nothing Then Return
        If Me.ChartArea Is Nothing Then Return
        Dim h As HitTestResult = e.HitTestResult
        If Me.ChartArea.AxisX.ScaleView.Zoomable Then
            Select Case h.ChartElementType
                Case ChartElementType.Axis, ChartElementType.AxisLabels
                    e.Text = "Click-drag in graph area to zoom"
                Case ChartElementType.ScrollBarZoomReset
                    e.Text = "Zoom undo"
            End Select
        End If
        Select Case h.ChartElementType
            Case ChartElementType.DataPoint
                e.Text = $"{h.Series.Name}{ControlChars.Lf}{h.Series.Points.Item(h.PointIndex)}"
        End Select
    End Sub

#End Region

#Region " PRINT "

    ''' <summary> Prints a page. </summary>
    ''' <remarks>
    ''' The Chart control uses a PrintManager object to manage printing, which is encapsulated in the
    ''' Chart.Printing property. The PrintManager has several methods that are used for very basic
    ''' printing operations, such as PageSetup, PrintPreview and Print. These are pretty self
    ''' explanatory, although it should be said that the Print method only prints a picture of the
    ''' graph on your paper and nothing else. Although I played around with it a lot, I could not get
    ''' the Print method to print very well, if such things like borders, etc. were anything except
    ''' very simple. I found the best way to print reports based on the graph was to set up a
    ''' PrintPage routine to handle printing, and add a Delegate to handle the specific graphs
    ''' PrintPage event. In the PrintPage routine, you can design your report as you like.
    ''' </remarks>
    ''' <param name="eventArgs"> Print page event information. </param>
    Public Sub PrintPage(ByVal eventArgs As PrintPageEventArgs)
        If eventArgs Is Nothing Then Return

        ' Create a memory stream to save the chart image    
        Using stream As New System.IO.MemoryStream()

            ' Save the chart image to the stream    
            Me.SaveImage(stream, System.Drawing.Imaging.ImageFormat.Bmp)

            ' Draw the bitmap from the stream on the printer graphics
            eventArgs.Graphics.DrawImage(New Bitmap(stream), eventArgs.MarginBounds)

        End Using

    End Sub

#End Region

#Region " AXES LABELS "

    ''' <summary>
    ''' The array Round Mantissa defines bins for the value of A, from which nice round values are
    ''' provided for major and minor tick intervals. For example, the first bin is for A = 1.0, and
    ''' from that, we get the interval values of 0.20 and 0.05. The second bin is for A in (1.0,1.2],
    ''' and from that we again get the interval values of 0.20 and 0.05. The algorithm is easily
    ''' customized by changing the bin and interval values in these arrays.
    ''' </summary>
    ''' <value> The round mantissa. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RoundMantissa As Double() = New Double() {1.0R, 1.2R, 1.4R, 1.6R, 1.8R, 2.0R, 2.5R, 3.0R, 4.0R, 5.0R, 6.0R, 8.0R, 10.0R}

    ''' <summary> Gets or sets the round interval major. </summary>
    ''' <value> The round interval major. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RoundIntervalMajor As Double() = New Double() {0.2R, 0.2R, 0.2R, 0.2R, 0.2R, 0.5R, 0.5R, 0.5R, 0.5R, 1.0R, 1.0R, 2.0R, 2.0R}

    ''' <summary> The round interval minor. </summary>
    ''' <value> The round interval minor. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property RoundIntervalMinor As Double() = New Double() {0.05R, 0.05R, 0.05R, 0.05R, 0.05R, 0.1R, 0.1R, 0.1R, 0.1R, 0.2R, 0.2R, 0.5R, 0.5R}

    ''' <summary>
    ''' Gets nice round numbers for the axes. For the horizontal axis, minValue is always 0.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="minValue">      [in,out] The minimum value. </param>
    ''' <param name="maxValue">      [in,out] The maximum value. </param>
    ''' <param name="majorInterval"> [in,out] The major interval. </param>
    ''' <param name="minorInterval"> [in,out] The minor interval. </param>
    Public Sub GetNiceRoundNumbers(ByRef minValue As Double, ByRef maxValue As Double, ByRef majorInterval As Double, ByRef minorInterval As Double)
        Dim min As Double = Math.Min(minValue, maxValue)
        Dim max As Double = Math.Max(minValue, maxValue)
        Dim delta As Double = max - min 'The full range
        'Special handling for zero full range
        If delta = 0 Then
            'When min == max == 0, choose arbitrary range of 0 - 1
            If min = 0 Then
                minValue = 0
                maxValue = 1
                majorInterval = 0.2
                minorInterval = 0.5
                Return
            End If
            'min == max, but not zero, so set one to zero
            If min < 0 Then
                max = 0 'min-max are -|min| to 0
            Else
                min = 0 'min-max are 0 to +|max|
            End If
            delta = max - min
        End If

        Dim N As Integer = ChartingExtensions.Base10Exponent(delta)
        Dim tenToN As Double = Math.Pow(10, N)
        Dim A As Double = delta / tenToN
        'At this point delta = A x Exp10(N), where
        ' 1.0 <= A < 10.0 and N = integer exponent value
        'Now, based on A select a nice round interval and maximum value
        For i As Integer = 0 To Me.RoundMantissa.Length - 1
            If A <= Me.RoundMantissa(i) Then
                majorInterval = Me.RoundIntervalMajor(i) * tenToN
                minorInterval = Me.RoundIntervalMinor(i) * tenToN
                Exit For
            End If
        Next i
        minValue = majorInterval * Math.Floor(min / majorInterval)
        maxValue = majorInterval * Math.Ceiling(max / majorInterval)
    End Sub

#End Region

#Region " ZOOM "

    ''' <summary> Gets or sets or set the flag for Using GDI32 for zooming. </summary>
    ''' <value> The use GDI 32. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property UseGdi32 As Boolean = True

    ''' <summary>
    ''' Gets or sets the flag for using nice round numbers based on the <see cref="RoundMantissa"/>
    ''' values.
    ''' </summary>
    ''' <value> The use nice round numbers. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property UseNiceRoundNumbers As Boolean = True

    ''' <summary> Gets or sets the axis label format. </summary>
    ''' <value> The axis label format. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property AxisLabelFormat As AxisLabelFormat = AxisLabelFormat.Smart

    ''' <summary> Sets up the chart zoom example. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub SetupChartZoomExample()
        Me.Tag = New ChartScaleData(Me)

        Me.Series.Clear()
        Dim iMax As Integer = 5 * 360
        Dim xMax, yMax, xMin, yMin As Double
        yMin = 0
        xMin = yMin
        yMax = xMin
        xMax = yMax
        Dim jMin As Integer = -3
        Dim jMax As Integer = 3
        Dim piDiv180 As Double = Math.PI / 180D
        For j As Integer = jMin To jMax
            Dim s As New Series($"exp {j}") With {
                .IsVisibleInLegend = True,
                .IsXValueIndexed = False,
                .ChartType = SeriesChartType.FastLine,
                .BorderWidth = 2,
                .MarkerStyle = CType(j - jMin + 1, MarkerStyle), '0 = MarkerStyle.None
                .MarkerBorderWidth = 3,
                .MarkerStep = 1,
                .MarkerSize = 10
            }

            Me.Series.Add(s)
            For i As Integer = 0 To iMax - 1
                Dim mag As Double = Math.Exp((0.5 * (j * i)) / iMax)
                Dim Y As Double = mag * Math.Sin(i * piDiv180)
                If i = 0 AndAlso j = 0 Then
                    xMax = i
                    xMin = xMax
                    yMax = Y
                    yMin = yMax
                Else
                    If Y < yMin Then yMin = Y
                    If Y > yMax Then yMax = Y
                    If i < xMin Then xMin = i
                    If i > xMax Then xMax = i
                End If
                s.Points.AddXY(i, Y)
            Next i
        Next j

        Dim xInt, xMinInt, yInt, yMinInt As Double
        yMinInt = 0
        yInt = yMinInt
        xMinInt = yInt
        xInt = xMinInt
        If Me.UseNiceRoundNumbers Then
            Me.GetNiceRoundNumbers(xMin, xMax, xInt, xMinInt)
            Me.GetNiceRoundNumbers(yMin, yMax, yInt, yMinInt)
        End If
        Dim area As ChartArea = Me.ChartAreas.First
        area.AxisX.Minimum = xMin
        area.AxisX.Maximum = xMax
        area.AxisX.Interval = xInt
        area.AxisX.MinorTickMark.Interval = xMinInt
        area.AxisX.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea
        area.AxisX.MinorTickMark.Enabled = True
        area.AxisX.MinorTickMark.Size = area.AxisX.MajorTickMark.Size / 2
        area.AxisY.Minimum = yMin
        area.AxisY.Maximum = yMax
        area.AxisY.Interval = yInt
        area.AxisY.MinorTickMark.Interval = yMinInt
        area.AxisY.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea
        area.AxisY.MinorTickMark.Enabled = True
        area.AxisY.MinorTickMark.Size = area.AxisY.MajorTickMark.Size / 2
        TryCast(Me.Tag, ChartScaleData).UpdateAxisBaseData()
        ChartingExtensions.SetAxisFormats(Me.ChartAreas.First, Me.AxisLabelFormat)
    End Sub

    ''' <summary>
    ''' Toggles handling the dashed zoom rectangle when the mouse is dragged over a chart with the
    ''' CTRL key pressed. The MouseDown, MouseMove and MouseUp events handle the creation and drawing
    ''' of the Zoom Rectangle.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="addHandlerToggle"> True to add handler toggle. </param>
    Public Sub ToggleDashedZoomRectangleMouseHandlers(ByVal addHandlerToggle As Boolean)
        If addHandlerToggle Then
            AddHandler Me.MouseDown, AddressOf Me.HandleDashedZoomRectangleMouseDown
            AddHandler Me.MouseMove, AddressOf Me.HandleDashedZoomRectangleMouseMove
            AddHandler Me.MouseUp, AddressOf Me.HandleDashedZoomRectangleMouseUp
        Else
            RemoveHandler Me.MouseDown, AddressOf Me.HandleDashedZoomRectangleMouseDown
            RemoveHandler Me.MouseMove, AddressOf Me.HandleDashedZoomRectangleMouseMove
            RemoveHandler Me.MouseUp, AddressOf Me.HandleDashedZoomRectangleMouseUp
        End If
    End Sub

    ''' <summary> The zoom rectangle. </summary>
    Private _ZoomRect As Rectangle

    ''' <summary> Indicate that we are dragging. </summary>
    Private _ZoomingNow As Boolean = False

    ''' <summary> Handles the dashed zoom rectangle mouse down. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleDashedZoomRectangleMouseDown(ByVal sender As Object, ByVal e As MouseEventArgs)
        If LicenseManager.UsageMode = LicenseUsageMode.Designtime Then Return
        Dim chart As Chart = TryCast(sender, Chart)
        If chart Is Nothing Then Return
        Me.Focus()
        'Test for CTRL+Left Single Click to start displaying selection box
        If (e.Button = MouseButtons.Left) AndAlso (e.Clicks = 1) AndAlso ((ModifierKeys And Keys.Control) <> 0) Then
            Me._ZoomingNow = True
            Me._ZoomRect.Location = e.Location
            Me._ZoomRect.Height = 0
            Me._ZoomRect.Width = Me._ZoomRect.Height
            ' Draw the new selection rectangle
            ChartingExtensions.DrawZoomRect(chart, Me._ZoomRect, Me.UseGdi32)
        End If
        Me.Focus()
    End Sub

    ''' <summary> Handles the dashed zoom rectangle mouse move. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleDashedZoomRectangleMouseMove(ByVal sender As Object, ByVal e As MouseEventArgs)
        Dim chart As Chart = TryCast(sender, Chart)
        If chart Is Nothing Then Return
        If Me._ZoomingNow Then
            ' Redraw the old selection rectangle, which erases it
            ChartingExtensions.DrawZoomRect(chart, Me._ZoomRect, Me.UseGdi32)
            Me._ZoomRect.Width = e.X - Me._ZoomRect.Left
            Me._ZoomRect.Height = e.Y - Me._ZoomRect.Top
            ' Draw the new selection rectangle
            ChartingExtensions.DrawZoomRect(chart, Me._ZoomRect, Me.UseGdi32)
        End If
    End Sub

    ''' <summary> Handles the dashed zoom rectangle mouse up. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub HandleDashedZoomRectangleMouseUp(ByVal sender As Object, ByVal e As MouseEventArgs)
        Dim chart As Chart = TryCast(sender, Chart)
        If chart Is Nothing Then Return
        If Me._ZoomingNow AndAlso e.Button = MouseButtons.Left Then
            'Redraw the selection rectangle, which erases it
            ChartingExtensions.DrawZoomRect(chart, Me._ZoomRect, Me.UseGdi32)
            If (Me._ZoomRect.Width <> 0) AndAlso (Me._ZoomRect.Height <> 0) Then
                'Just in case the selection was dragged from lower right to upper left
                Me._ZoomRect = New Rectangle(Math.Min(Me._ZoomRect.Left, Me._ZoomRect.Right),
                                            Math.Min(Me._ZoomRect.Top, Me._ZoomRect.Bottom),
                                            Math.Abs(Me._ZoomRect.Width), Math.Abs(Me._ZoomRect.Height))
                ' no Shift so Zoom in.
                Me.ZoomInToZoomRect(TryCast(chart.Tag, ChartScaleData), chart.ChartAreas.First)
            End If
            Me._ZoomingNow = False
        End If
    End Sub

    ''' <summary>
    ''' This method zooms in to the area contained within the portion of zoomRect that overlaps the
    ''' chart innerPlotRectangle. zoomRect is positioned in the client rectangle of the chart in
    ''' which the zoom was initiated with a Left MouseDown event.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="chartScaleData"> Information describing the chart scale. </param>
    ''' <param name="area">           The area. </param>
    Private Sub ZoomInToZoomRect(chartScaleData As ChartScaleData, ByVal area As ChartArea)
        If Me._ZoomRect.Width = 0 OrElse Me._ZoomRect.Height = 0 Then
            Return
        End If

        Dim r As Rectangle = Me._ZoomRect

        'Get overlap of zoomRect and the innerPlotRectangle
        Dim ipr As Rectangle = chartScaleData.InnerPlotRectangle
        If Not r.IntersectsWith(ipr) Then
            Return
        End If
        r.Intersect(ipr)
        If Not chartScaleData.IsZoomed Then
            chartScaleData.IsZoomed = True
            chartScaleData.UpdateAxisBaseData()
        End If

        Me.SetZoomAxisScale(area, area.AxisX, r.Left, r.Right)
        Me.SetZoomAxisScale(area, area.AxisY, r.Bottom, r.Top)
    End Sub

    ''' <summary>
    ''' Sets the axis parameters to an non-zoomed state showing all data, and nice round numbers and
    ''' intervals for the axis labels Note: at this stage the axis is unchanged, and this has not
    ''' been tested with reversed axes.
    ''' </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="area">  The area. </param>
    ''' <param name="axis">  . </param>
    ''' <param name="pxLow"> low pixel position in chart client rectangle coordinates. </param>
    ''' <param name="pxHi">  Hi pixel position in chart client rectangle coordinates. </param>
    Private Sub SetZoomAxisScale(ByVal area As ChartArea, ByVal axis As Axis, ByVal pxLow As Integer, ByVal pxHi As Integer)
        Dim minValue As Double = Math.Max(axis.Minimum, axis.PixelPositionToValue(pxLow))
        Dim maxValue As Double = Math.Min(axis.Maximum, axis.PixelPositionToValue(pxHi))
        Dim axisInterval As Double = 0
        Dim axisIntMinor As Double = 0
        If Me.UseNiceRoundNumbers Then
            Me.GetNiceRoundNumbers(minValue, maxValue, axisInterval, axisIntMinor)
        Else
            axisInterval = (maxValue - minValue) / 5.0R
        End If
        axis.Minimum = minValue
        axis.Maximum = maxValue
        axis.Interval = axisInterval
        axis.MinorTickMark.Interval = axisIntMinor
        ChartingExtensions.SetAxisFormats(area, Me.AxisLabelFormat)
    End Sub

#End Region

#Region " CONTEXT MENU "

        Private WithEvents ZoomOutToolStripMenuItem As ToolStripMenuItem

        Private WithEvents SeparatorToolStripItem As ToolStripSeparator

        Private WithEvents NiceRoundNumbersToolStripMenuItem As ToolStripMenuItem

        Private WithEvents ZoomWithGDI32ToolStripMenuItem As ToolStripMenuItem

        Private WithEvents SeparatorToolStripItem1 As ToolStripSeparator

        Private WithEvents DefaultFormatToolStripMenuItem As ToolStripMenuItem

        Private WithEvents SmartFormatToolStripMenuItem As ToolStripMenuItem

        Private WithEvents WholeNumberFormatToolStripMenuItem As ToolStripMenuItem

    ''' <summary> Populate context menu. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    Public Sub PopulateContextMenu()
        Me.ZoomOutToolStripMenuItem = New ToolStripMenuItem With {.Text = "Zoom Out", .CheckOnClick = True}
        Me.NiceRoundNumbersToolStripMenuItem = New ToolStripMenuItem With {.Text = "Nice Round Numbers", .CheckOnClick = True}
        Me.ZoomWithGDI32ToolStripMenuItem = New ToolStripMenuItem With {.Text = "Zoom /w GDI", .CheckOnClick = True}
        Me.DefaultFormatToolStripMenuItem = New ToolStripMenuItem With {.Text = "Default Format", .CheckOnClick = True}
        Me.SmartFormatToolStripMenuItem = New ToolStripMenuItem With {.Text = "Smart Format", .CheckOnClick = True}
        Me.WholeNumberFormatToolStripMenuItem = New ToolStripMenuItem With {.Text = "Whole Number Format", .CheckOnClick = True}
        Me.SeparatorToolStripItem = New ToolStripSeparator
        Me.SeparatorToolStripItem1 = New ToolStripSeparator
        Me.ContextMenuStrip = New ContextMenuStrip
        Me.ContextMenuStrip.Items.AddRange(New ToolStripItem() {Me.ZoomOutToolStripMenuItem,
                                           Me.SeparatorToolStripItem,
                                           Me.NiceRoundNumbersToolStripMenuItem,
                                           Me.ZoomWithGDI32ToolStripMenuItem,
                                           Me.SeparatorToolStripItem1,
                                           Me.DefaultFormatToolStripMenuItem,
                                           Me.SmartFormatToolStripMenuItem,
                                           Me.WholeNumberFormatToolStripMenuItem})

    End Sub

    ''' <summary> Toggle context menu. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    Public Sub ToggleContextMenu(ByVal enabled As Boolean)
        If enabled Then
            AddHandler Me.ContextMenuStrip.Opening, AddressOf Me.HandleContextMenuStripOpening
        Else
            RemoveHandler Me.ContextMenuStrip.Opening, AddressOf Me.HandleContextMenuStripOpening
        End If
    End Sub

    ''' <summary> Handles the context menu strip opening. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Private Sub HandleContextMenuStripOpening(ByVal sender As Object, ByVal e As CancelEventArgs)
        If Me._ZoomingNow Then
            e.Cancel = True
            Return
        End If
        Me.ZoomOutToolStripMenuItem.Visible = (TryCast(Me.Tag, ChartScaleData)).IsZoomed
        Me.SeparatorToolStripItem.Visible = Me.ZoomOutToolStripMenuItem.Visible
        Me.ZoomWithGDI32ToolStripMenuItem.Checked = Me.UseGdi32
        Me.NiceRoundNumbersToolStripMenuItem.Checked = Me.UseNiceRoundNumbers
        Me.DefaultFormatToolStripMenuItem.Checked = Me.AxisLabelFormat = AxisLabelFormat.None
        Me.SmartFormatToolStripMenuItem.Checked = Me.AxisLabelFormat = AxisLabelFormat.Smart
        Me.WholeNumberFormatToolStripMenuItem.Checked = Me.AxisLabelFormat = AxisLabelFormat.WholeNumber
    End Sub

    ''' <summary> Zoom out tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ZoomOutToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ZoomOutToolStripMenuItem.Click
        TryCast(Me.Tag, ChartScaleData).ResetAxisScale()
        TryCast(Me.Tag, ChartScaleData).IsZoomed = False
    End Sub

    ''' <summary> Nice round numbers tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NiceRoundNumbersToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles NiceRoundNumbersToolStripMenuItem.Click
        Me.UseNiceRoundNumbers = Me.NiceRoundNumbersToolStripMenuItem.Checked
        Me.SetupChartZoomExample()
    End Sub

    ''' <summary> Zoom with GDI 32 tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ZoomWithGDI32ToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ZoomWithGDI32ToolStripMenuItem.Click
        Me.UseGdi32 = Me.ZoomWithGDI32ToolStripMenuItem.Checked
    End Sub

    ''' <summary> Whole number format tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub WholeNumberFormatToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles WholeNumberFormatToolStripMenuItem.Click
        Me.DefaultFormatToolStripMenuItem.Checked = False
        Me.SmartFormatToolStripMenuItem.Checked = False
        Me.WholeNumberFormatToolStripMenuItem.Checked = True
        Me.AxisLabelFormat = AxisLabelFormat.WholeNumber
        ChartingExtensions.SetAxisFormats(Me.ChartAreas.First, Me.AxisLabelFormat)
    End Sub

    ''' <summary> Smart format tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SmartFormatToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles SmartFormatToolStripMenuItem.Click
        Me.DefaultFormatToolStripMenuItem.Checked = False
        Me.SmartFormatToolStripMenuItem.Checked = True
        Me.WholeNumberFormatToolStripMenuItem.Checked = False
        Me.AxisLabelFormat = AxisLabelFormat.Smart
        ChartingExtensions.SetAxisFormats(Me.ChartAreas.First, Me.AxisLabelFormat)
    End Sub

    ''' <summary> Default format tool strip menu item click. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub DefaultFormatToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles DefaultFormatToolStripMenuItem.Click
        Me.DefaultFormatToolStripMenuItem.Checked = True
        Me.SmartFormatToolStripMenuItem.Checked = False
        Me.WholeNumberFormatToolStripMenuItem.Checked = False
        Me.AxisLabelFormat = AxisLabelFormat.None
        ChartingExtensions.SetAxisFormats(Me.ChartAreas.First, Me.AxisLabelFormat)
    End Sub

#End Region

End Class

''' <summary> A chart marker. </summary>
''' <remarks> David, 10/20/2020. </remarks>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure ChartMarker

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 10/20/2020. </remarks>
    ''' <param name="style"> The style. </param>
    ''' <param name="size">  The size. </param>
    ''' <param name="color"> The color. </param>
    Public Sub New(ByVal style As MarkerStyle, size As Integer, color As Color)
        Me.Size = size
        Me.Color = color
        Me.Style = style
    End Sub

    ''' <summary> Gets or sets the size in points. </summary>
    ''' <value> The size. </value>
    Public Property Size As Integer

    ''' <summary> Gets or sets the style. </summary>
    ''' <value> The style. </value>
    Public Property Style As MarkerStyle

    ''' <summary> Gets or sets the color. </summary>
    ''' <value> The color. </value>
    Public Property Color As Color

End Structure

''' <summary> Values that represent axis label formats. </summary>
''' <remarks> David, 10/20/2020. </remarks>
Public Enum AxisLabelFormat

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("None, String Empty")> None

    ''' <summary> An enum constant representing the whole number option. </summary>
    <Description("Whole number (F0)")> WholeNumber

    ''' <summary> An enum constant representing the smart option. </summary>
    <Description("Smart format")> Smart
End Enum
