## ISR Visuals Charting Libraries<sub>&trade;</sub>
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*3.0.7604 10/26/20*  
Breaking change.  Remove Chart initialization from the 
Line Chart Control constructor to prevent exception because the form constructor 
adds a default area.

*2.1.6667 04/03/18*  
2018 release.

*2.0.6325 04/24/17*  
Uses VS 2017 rule set. Applies camel casing to all
elements public or private. Simplifies initializing of objects and
collections.

*2.0.6083 08/27/16*  
Changes line chart to a control.

*1.0.6055 07/30/16*  
Adds line chart class.

*1.0.5167 02/23/14*  
Created.

\(C\) 2014 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Visuals Charting Libraries](https://bitbucket.org/davidhary/vs.Visuals.Charting)  
[Smooth Zoom & Round Numbers in MS Chart](https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart)  
[Speedup Chart Clear Data Points](http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points)  
[Graphing time series](http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series)
