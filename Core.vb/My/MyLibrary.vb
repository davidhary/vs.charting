﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 7/30/2016 </para>
    ''' </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 10/20/2020. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Visuals + &H1

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Charting Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Charting Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Visuals.Charting.Library"

    End Class

End Namespace

