Use [Products]

CREATE TABLE [Categories] (
	[ID] Int Not Null, 
	[CategoryName] [nvarchar](255) Not Null, 
	[State] Int Not Null, 
	[ExRate] Float Not Null, 
	[TransRate] Float Not Null,
CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

Use [Products]

CREATE TABLE [Products] (
	[ID] Int Not Null,  
	[CategoryID] Int Not Null, 
	[GranitesideID] [nvarchar](255) Not Null, 
	[MsasaID] [nvarchar](255) Not Null, 
	[ProdName] [nvarchar](255) Not Null, 
	[CostPrice] [float] NOT NULL, 
	[SalesMonth1] Int Not Null, 
	[SalesMonth2] Int Not Null, 
	[SalesMonth3] Int Not Null, 
	[SalesCurrentMonth] Int Not Null, 
	[RetailPrice] Float Not Null DEFAULT 0, 
	[State] Int Not Null,
CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [Sales] (
	[ID] [int] IDENTITY(1,1) NOT NULL, 
	[InvDate] DateTime,
CONSTRAINT [PK_Sales] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [SalesDetails] (
	[ID] [Int] NOT NULL IDENTITY(1,1), 
	[Quantity] [Int] NOT NULL, 
	[SaleID] [Int] NOT NULL, 
	[ProductID] [Int] NOT NULL, 
CONSTRAINT [PK_SalesDetails] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO







