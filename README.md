# Charting Libraries
Encapsulates the Visual Studio chart control.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)

<a name="Getting-Started"></a>
## Getting Started

Clone the repository along with its requisite repositories to their respective relative path.

### Line Chart Control VB.Net

1. Add the line chart control to a control (MeterChartControl) or a form in place of MS Chart.

2. Add handler for updating the tool tip
```
Public Class MeterChartControl
	
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        AddHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
    End Sub
	
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                If components IsNot Nothing Then
                    components.Dispose()
                End If
                If Me._Chart IsNot Nothing Then
                    RemoveHandler Me._Chart.GetToolTipText, AddressOf Me._Chart.OnGetTooltipText
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

End Class	

```

3. Initialize
```
    Public Sub InitializeKnownState()
        Me.Chart.AddTitle("Frequency Distribution")
        Me.Chart.InitializeCustomPalette()
        Me.Chart.InitializeChartArea(False)
    End Sub
```

4. Display
```
    Public Function Simulate() As Series
        Dim l As New List(Of Double)
        Dim rnd As New Random
        For i As Integer = 1 To 20000
            l.Add(rnd.NextNormal)
        Next
        Return Me.Display(l.ToArray)
    End Function

    ''' <summary> Name of the histogram series. </summary>
    Public Const HistogramSeriesName As String = "histogram"
	
    Public Function Display(ByVal values As IEnumerable(Of Double)) As Series
        Dim lowerLimit As Double = -3
        Dim upperLimit As Double = 3
        Dim count As Integer = 51
        Dim result As IEnumerable(Of Windows.Point) = values.HistogramDirect(lowerLimit, upperLimit, count)
        Dim ser As Series = Me.Chart.GraphLineSeries(MeterChartControl.HistogramSeriesName, SeriesChartType.FastLine, result)
        Me.Chart.ChartArea.AxisX.Minimum = -3
        Me.Chart.ChartArea.AxisX.Maximum = 3
        Return ser
    End Function
```

### Prerequisites

The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Charting](https://www.bitbucket.org/davidhary/vs.charting) - Charting Libraries
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries

```
git clone git@bitbucket.org:davidhary/vs.charting.git
git clone git@bitbucket.org:davidhary/vs.core.git
```
Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Core\Core
.\Libraries\VS\Visuals\Charting
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Authors"></a>
## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments

* [Code artist](https://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points) -- speedup clearing data points
* [Daryl Bryk](https://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series) -- Graphing time series
* [Its all a remix](https://www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Visuals Charting Libraries](https://bitbucket.org/davidhary/vs.Visuals.Charting)  
[Smooth Zoom & Round Numbers in MS Chart](https://www.codeproject.com/Articles/1261160/Smooth-Zoom-Round-Numbers-in-MS-Chart)  
[Speedup Chart Clear Data Points](http://www.codeproject.com/Articles/376060/Speedup-MSChart-Clear-Data-Points)  
[Graphing time series](http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series)
