# Changelog
All notable changes to these libraries will be documented in this file. 
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [3.0.7612] - 2020-11-02
* Converted to C#

## [3.0.7604] - 2020-10-26
Breaking change. 
Remove Chart initialization from the Line Chart Control constructor to prevent 
exception because the form constructor adds a default area.

## [2.1.6667] - 2018-04-03
2018 release.

## [2.0.6325 ] - 2017-04-24
Uses VS 2017 rule set. Applies camel casing to all
elements public or private. Simplifies initializing of objects and
collections.

## [2.0.6083] - 2016-08-27
Changes line chart to a control.

## [1.0.6055] - 2016-07-
Adds line chart class.

## [1.0.5167] - 2014-02-23
Created.

\(C\) 2014 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
