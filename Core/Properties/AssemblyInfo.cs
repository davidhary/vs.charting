﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Visuals.Charting.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Visuals.Charting.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Visuals.Charting.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
