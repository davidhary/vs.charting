using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Charting
{

    /// <summary> A line chart. </summary>
    /// <remarks>
    /// (c) 2016 Darryl Bryk. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-29. Source:
    /// http://www.codeproject.com/Articles/1089580/An-MSChart-Class-for-Graphing-Line-Series. </para>
    /// </remarks>
    public class LineChartControl : Chart
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public LineChartControl() : base()
        {
        }

        #region " IDISPOSABLE SUPPORT "

        /// <summary> Releases unmanaged and, optionally, managed resources. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both unmanaged and managed
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this.ChartArea is object )
                        {
                            this.ChartArea.Dispose();
                            this.ChartArea = null;
                        }

                        if ( this.ContextMenuStrip is object )
                        {
                            this.ContextMenuStrip.Items?.Clear();
                            this.ZoomOutToolStripMenuItem?.Dispose();
                            this.NiceRoundNumbersToolStripMenuItem?.Dispose();
                            this.ZoomWithGDI32ToolStripMenuItem?.Dispose();
                            this.DefaultFormatToolStripMenuItem?.Dispose();
                            this.SmartFormatToolStripMenuItem?.Dispose();
                            this.WholeNumberFormatToolStripMenuItem?.Dispose();
                            this._SeparatorToolStripItem?.Dispose();
                            this._SeparatorToolStripItem1?.Dispose();
                        }
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " INITIALIZE KNOWN STATE "

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="zoomable"> True if zoomable. </param>
        public void InitializeKnownState( bool zoomable )
        {
            this.InitializeKnownState( ChartAreaName, zoomable );
        }

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="areaName"> Name of the area. </param>
        /// <param name="zoomable"> True if zoomable. </param>
        public void InitializeKnownState( string areaName, bool zoomable )
        {
            this.ChartAreas.Clear();
            this.Clear();
            this.InitializeChartArea( areaName, zoomable );
            this.InitializeCustomPalette();
        }

        #endregion

        #region " AREA "

        /// <summary> Name of the chart area. </summary>
        public const string ChartAreaName = "Area";

        /// <summary> Gets or sets the chart area. </summary>
        /// <value> The chart area. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public ChartArea ChartArea { get; private set; }

        /// <summary> Initializes the chart area. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="areaName"> Name of the area. </param>
        /// <param name="zoomable"> True if zoomable. </param>
        public void InitializeChartArea( string areaName, bool zoomable )
        {
            this.ChartArea = this.ChartAreas.IsUniqueName( areaName ) ? this.ChartAreas.Add( ChartAreaName ) : this.ChartAreas[ChartAreaName];

            // Enable range selection and zooming
            this.ChartArea.CursorX.IsUserEnabled = zoomable;
            this.ChartArea.CursorX.IsUserSelectionEnabled = zoomable;
            this.ChartArea.AxisX.ScaleView.Zoomable = zoomable;
            this.ChartArea.AxisX.ScrollBar.IsPositionedInside = true;
            this.ChartArea.AxisX.ScaleView.SmallScrollSize = 1d;
            this.ChartArea.AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
            this.ChartArea.AxisY.IntervalAutoMode = IntervalAutoMode.FixedCount;
            this.ChartArea.AxisX.LabelStyle.Format = "F0";
            this.ChartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
            this.ChartArea.AxisX.MajorGrid.LineColor = Color.FromArgb( 200, 200, 200 );
            this.ChartArea.AxisY.MajorGrid.LineColor = Color.FromArgb( 200, 200, 200 );
            this.ChartArea.AxisX.LabelStyle.Font = new Font( this.ChartArea.AxisX.LabelStyle.Font.Name, 9f, FontStyle.Regular );
            this.ChartArea.AxisY.LabelStyle.Font = new Font( this.ChartArea.AxisY.LabelStyle.Font.Name, 9f, FontStyle.Regular );
            this.ChartArea.AxisX.TitleFont = new Font( this.ChartArea.AxisX.TitleFont.Name, 9f, FontStyle.Regular );
            this.ChartArea.AxisY.TitleFont = new Font( this.ChartArea.AxisY.TitleFont.Name, 9f, FontStyle.Regular );
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void Clear()
        {
            this.Series.Clear();
            this.Titles.Clear();
            this.Legends.Clear();
        }

        /// <summary> Last chart area. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> A ChartArea. </returns>
        private ChartArea LastChartArea()
        {
            return this.ChartAreas[this.ChartAreas.Count - 1];
        }

        #endregion

        #region " AXIS: ABSCISSA "

        /// <summary> Axis x coordinate title. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="value"> The title. </param>
        public void AbscissaTitle( string value )
        {
            this.ChartArea.AxisX.Title = value;
        }

        #endregion

        #region " AXIS: ORDINATE "

        /// <summary> Axis y coordinate title. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="value"> The title. </param>
        public void OrdinateTitle( string value )
        {
            if ( string.IsNullOrEmpty( this.ChartArea.AxisY.Title ) )
            {
                this.ChartArea.AxisY.Title = value;
            }
            else if ( !this.ChartArea.AxisY.Title.Contains( value ) )
            {
                this.ChartArea.AxisY.Title = $"{this.ChartArea.AxisY.Title}, {value}"; // Append
            }
        }

        #endregion

        #region " LEGEND "

        /// <summary> Name of the chart legend. </summary>
        public const string MainLegendName = "Legend";

        /// <summary> Gets or sets the main legend. </summary>
        /// <value> The main legend. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Legend MainLegend { get; private set; }

        /// <summary> Adds main legend. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void AddMainLegend()
        {
            this.MainLegend = this.Legends.Add( MainLegendName );
            this.MainLegend.Docking = Docking.Bottom;
            this.MainLegend.Font = new Font( this.Legends[0].Font.FontFamily, 8f ); // Font size
            this.MainLegend.IsTextAutoFit = true;
        }

        #endregion

        #region " LINE ANNOTATION "

        /// <summary> Adds t=0 line annotation to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void GraphOriginLine()
        {
            this.GraphOriginLine( "t=0" );
        }

        /// <summary> Adds t=0 line annotation to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="caption"> The caption. </param>
        public void GraphOriginLine( string caption )
        {
            this.GraphLineAnnotations( caption, 0d, Color.Black, true, ChartDashStyle.Dash );
        }

        /// <summary> Adds line annotation to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="anchorX">      The x coordinate. </param>
        /// <param name="lineColor">    The lineColor. </param>
        /// <param name="labelEnabled"> true to label. </param>
        public void GraphLineAnnotations( string name, double anchorX, Color lineColor, bool labelEnabled )
        {
            this.GraphLineAnnotations( name, anchorX, lineColor, labelEnabled, ChartDashStyle.Solid );
        }

        /// <summary> Adds line annotation to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="name">      The name. </param>
        /// <param name="anchorX">   The x coordinate. </param>
        /// <param name="lineColor"> The lineColor. </param>
        /// <param name="lineStyle"> the line style. </param>
        public void GraphLineAnnotations( string name, double anchorX, Color lineColor, ChartDashStyle lineStyle )
        {
            this.GraphLineAnnotations( name, anchorX, lineColor, false, lineStyle );
        }

        /// <summary> Adds line annotation to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="name">         The name. </param>
        /// <param name="anchorX">      The x coordinate. </param>
        /// <param name="lineColor">    The lineColor. </param>
        /// <param name="labelEnabled"> true to label. </param>
        /// <param name="lineStyle">    the line style. </param>
        public void GraphLineAnnotations( string name, double anchorX, Color lineColor, bool labelEnabled, ChartDashStyle lineStyle )
        {
            foreach ( Annotation ann in this.Annotations )
            {
                if ( (ann.Name ?? "") == (name ?? "") ) // Don't duplicate
                {
                    return;
                }
            }

            var annot = new VerticalLineAnnotation() {
                AxisX = this.ChartArea.AxisX,
                AxisY = this.ChartArea.AxisY,
                IsInfinitive = true,
                ClipToChartArea = this.ChartArea.Name,
                LineWidth = 1,
                LineColor = lineColor,
                LineDashStyle = lineStyle,
                ToolTip = name
            };
            annot.Name = annot.ToolTip;
            annot.AnchorX = anchorX;
            this.Annotations.Add( annot );
            if ( labelEnabled )
            {
                var a = new RectangleAnnotation() {
                    Text = name,
                    AxisX = annot.AxisX,
                    AxisY = annot.AxisY,
                    AnchorX = anchorX,
                    LineColor = Color.Transparent,
                    BackColor = Color.Transparent,
                    ForeColor = lineColor
                };
                double textw = TextRenderer.MeasureText( name, a.Font ).Width + 1.0f;
                double texth = TextRenderer.MeasureText( name, a.Font ).Height + 1.0f;
                a.Width = textw / this.Width * 100d;
                a.Height = texth / this.Height * 100d;
                a.X -= Math.Floor( 0.5d * a.Width );
                this.ChartArea.RecalculateAxesScale(); // Needed so AxisY.Maximum != NaN
                a.Y = this.ChartArea.AxisY.Maximum;
                this.Annotations.Add( a );

                // Handle resize - annotation sized as % of chart size
                Resize += ( sender, e ) => {
                    a.Width = textw / this.Width * 100d;
                    a.Height = texth / this.Height * 100d;
                };
            }
        }

        #endregion

        #region " SERIES "

        /// <summary> Number of series in the chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> An Integer. </returns>
        public int SeriesCount()
        {
            return this.Series.Count;
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="seriesName"> The series name. </param>
        public void Clear( string seriesName )
        {
            if ( !this.Series.IsUniqueName( seriesName ) )
                this.Series[seriesName].Points.Clear();
        }

        /// <summary> Next series name. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> A String. </returns>
        public string NextSeriesName()
        {
            return $"Series#{this.SeriesCount() + 1}";
        }

        /// <summary> Adds the series. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="seriesName"> The series name. </param>
        /// <returns> The Series. </returns>
        public Series AddSeries( string seriesName )
        {
            var ser = new Series( this.NextSeriesName() );
            if ( this.Series.IsUniqueName( seriesName ) )
            {
                this.Series.Add( ser );
            }
            else
            {
                ser = this.Series[seriesName];
            }

            return ser;
        }

        /// <summary> Adds the series. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="seriesName">  The series name. </param>
        /// <param name="chartArea">   The chart area. </param>
        /// <param name="seriesColor"> The series color. </param>
        /// <returns> The Series. </returns>
        public Series AddSeries( string seriesName, string chartArea, Color seriesColor )
        {
            var ser = new Series( this.NextSeriesName() );
            if ( this.Series.IsUniqueName( seriesName ) )
            {
                this.Series.Add( ser );
            }
            else
            {
                ser = this.Series[seriesName];
            }

            ser.ChartArea = chartArea;
            ser.Color = seriesColor;
            return ser;
        }

        /// <summary> Adds the series. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> The Series. </returns>
        public Series AddSeries()
        {
            return this.AddSeries( this.NextSeriesName(), this.LastChartArea().Name, this.NextSeriesColor() );
        }

        #endregion

        #region " SERIES: BIND "

        /// <summary> Bind line series. Call <see cref="Chart.DataBind()"/> to update the chart. </summary>
        /// <remarks> David, 2020-06-27. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName"> The series name. </param>
        /// <param name="chartType">  Type of the chart. </param>
        /// <param name="points">     The points. </param>
        /// <returns> The Series. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public Series BindLineSeries( string seriesName, SeriesChartType chartType, IList< isr.Core.Cartesian.CartesianPoint<double>> points )
        {
            if ( points is null )
                throw new ArgumentNullException( nameof( points ) );
            if ( string.IsNullOrWhiteSpace( seriesName ) )
                throw new ArgumentNullException( nameof( seriesName ) );
            Series ser;
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            try
            {
                if ( this.Series.IsUniqueName( seriesName ) )
                {
                    this.Series.Add( new Series( seriesName ) );
                    ser = this.Series[seriesName];
                }
                else
                {
                    ser = this.Series[seriesName];
                    ser.Points.Clear();
                }

                ser.ChartType = chartType;
                if ( this.ChartAreas.Count > 1 ) // Bind to last chart area
                {
                    ser.ChartArea = this.ChartAreas[this.ChartAreas.Count - 1].Name;
                }

                this.DataSource = points;
                ser.XValueMember = nameof( System.Windows.Point.X );
                ser.YValueMembers = nameof( System.Windows.Point.Y );
            }
            catch ( ArgumentException ) // Handle redundant series
            {
                return null;
            }
            catch
            {
                throw;
            }

            System.Windows.Forms.Cursor.Current = Cursors.Default;
            return ser;
        }

        #endregion

        #region " SERIES: DRAW "

        /// <summary> Adds line series to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName"> The series name. </param>
        /// <param name="chartType">  Type of the chart. </param>
        /// <param name="points">     The points. </param>
        /// <returns> The Series. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public Series GraphLineSeries( string seriesName, SeriesChartType chartType, IList<System.Windows.Point> points )
        {
            if ( points is null || !points.Any() )
                throw new ArgumentNullException( nameof( points ) );
            if ( string.IsNullOrWhiteSpace( seriesName ) )
                throw new ArgumentNullException( nameof( seriesName ) );
            Series ser;
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            try
            {
                if ( this.Series.IsUniqueName( seriesName ) )
                {
                    this.Series.Add( new Series( seriesName ) );
                    ser = this.Series[seriesName];
                }
                else
                {
                    ser = this.Series[seriesName];
                    ser.Points.Clear();
                }

                ser.ChartType = chartType;
                if ( this.ChartAreas.Count > 1 ) // Bind to last chart area
                {
                    ser.ChartArea = this.ChartAreas[this.ChartAreas.Count - 1].Name;
                }

                this.DataSource = points;
                ser.XValueMember = nameof( System.Windows.Point.X );
                ser.YValueMembers = nameof( System.Windows.Point.Y );
            }

            // set points one-by-one
            // For Each p As Windows.Point In points
            // ser.Points.Add(New DataPoint(p.X, p.Y))
            // Next

            catch ( ArgumentException ) // Handle redundant series
            {
                return null;
            }
            catch
            {
                throw;
            }

            System.Windows.Forms.Cursor.Current = Cursors.Default;
            return ser;
        }

        /// <summary> Adds line series to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName">     The series name. </param>
        /// <param name="chartType">      Type of the chart. </param>
        /// <param name="abscissaValues"> The x coordinate values. </param>
        /// <param name="ordinateValues"> The y coordinate values. </param>
        /// <returns> The Series. </returns>
        public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues )
        {
            return abscissaValues is null
                ? throw new ArgumentNullException( nameof( abscissaValues ) )
                : ordinateValues is null
                ? throw new ArgumentNullException( nameof( ordinateValues ) )
                : this.GraphLineSeries( seriesName, chartType, abscissaValues, ordinateValues, false, Color.Empty, true );
        }

        /// <summary> Adds line series to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName">      The series name. </param>
        /// <param name="chartType">       Type of the chart. </param>
        /// <param name="abscissaValues">  The x coordinate values. </param>
        /// <param name="ordinateValues">  The y coordinate values. </param>
        /// <param name="title">           The title. </param>
        /// <param name="abscissaTitle">   The abscissa title. </param>
        /// <param name="ordinateTitle">   The ordinate title. </param>
        /// <param name="addMinMaxLabels"> true to add minimum and maximum labels. </param>
        /// <returns> The Series. </returns>
        public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues, string title, string abscissaTitle, string ordinateTitle, bool addMinMaxLabels )
        {
            if ( abscissaValues is null )
                throw new ArgumentNullException( nameof( abscissaValues ) );
            if ( ordinateValues is null )
                throw new ArgumentNullException( nameof( ordinateValues ) );
            this.AddTitle( title, Docking.Top );
            this.OrdinateTitle( ordinateTitle );
            this.AbscissaTitle( abscissaTitle );
            return this.GraphLineSeries( seriesName, chartType, abscissaValues, ordinateValues, addMinMaxLabels, Color.Empty, true );
        }


        /// <summary> Adds line series to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName">        The series name. </param>
        /// <param name="chartType">         Type of the chart. </param>
        /// <param name="abscissaValues">    The x coordinate values. </param>
        /// <param name="ordinateValues">    The y coordinate values. </param>
        /// <param name="addMinMaxLabels">   true to add minimum and maximum labels. </param>
        /// <param name="seriesColor">       The series color. </param>
        /// <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
        /// <returns> The Series. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public Series GraphLineSeries( string seriesName, SeriesChartType chartType, double[] abscissaValues, double[] ordinateValues, bool addMinMaxLabels, Color seriesColor, bool isVisibleInLegend )
        {
            if ( abscissaValues is null )
                throw new ArgumentNullException( nameof( abscissaValues ) );
            if ( ordinateValues is null )
                throw new ArgumentNullException( nameof( ordinateValues ) );
            var ser = new Series( seriesName );
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            try
            {
                this.Series.Add( ser );
                ser.ChartType = chartType;
                if ( this.ChartAreas.Count >= 1 ) // Bind to last chart area
                {
                    ser.ChartArea = this.ChartAreas[this.ChartAreas.Count - 1].Name;
                }

                ser.Points.DataBindXY( abscissaValues, ordinateValues );
            }
            catch ( ArgumentException ) // Handle redundant series
            {
                return null;
            }
            catch
            {
                throw;
            }

            ser.IsVisibleInLegend = isVisibleInLegend;
            ser.Color = seriesColor;
            if ( addMinMaxLabels ) // Add min, max labels
            {
                this.ApplyPaletteColors(); // Force color assign so labels match
                SmartLabels( ser, ser.Color, ser.Color, FontStyle.Regular, 7 );
                ser.SmartLabelStyle.MovingDirection = LabelAlignmentStyles.TopLeft;
                var p = ser.Points.FindMaxByValue();
                p.Label = "Max = " + p.YValues[0].ToString( "F1" );
                p = ser.Points.FindMinByValue();
                p.Label = "Min = " + p.YValues[0].ToString( "F1" );
            }

            System.Windows.Forms.Cursor.Current = Cursors.Default;
            return ser;
        }


        /// <summary> Graph range series. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName">           The series name. </param>
        /// <param name="abscissaValues">       The x coordinate values. </param>
        /// <param name="ordinateValues">       The y coordinate values. </param>
        /// <param name="ordinateHeightValues"> The ordinate height values. </param>
        /// <param name="seriesColor">          The series color. </param>
        /// <returns> The Series. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public Series GraphRangeSeries( string seriesName, double[] abscissaValues, double[] ordinateValues, double[] ordinateHeightValues, Color seriesColor )
        {
            if ( abscissaValues is null )
                throw new ArgumentNullException( nameof( abscissaValues ) );
            if ( ordinateValues is null )
                throw new ArgumentNullException( nameof( ordinateValues ) );
            if ( ordinateHeightValues is null )
                throw new ArgumentNullException( nameof( ordinateHeightValues ) );
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            var ser = new Series( seriesName );
            try
            {
                this.Series.Add( ser );
                if ( this.ChartAreas.Count > 1 ) // Bind to last chart area
                {
                    ser.ChartArea = this.ChartAreas[this.ChartAreas.Count - 1].Name;
                }

                ser.ChartType = SeriesChartType.Range;
                ser.IsVisibleInLegend = false;
                ser.Color = seriesColor;
                ser.Points.DataBindXY( abscissaValues, ordinateValues, ordinateHeightValues );
            }
            catch ( ArgumentException )
            {
                // Handles redundant series
                return null;
            }
            catch
            {
                throw;
            }

            System.Windows.Forms.Cursor.Current = Cursors.Default;
            return ser;
        }

        /// <summary> Graphs series points style to chart. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="seriesName">        Name of the series. </param>
        /// <param name="abscissa">          The abscissa values. </param>
        /// <param name="ordinate">          The ordinate values. </param>
        /// <param name="marker">            The marker. </param>
        /// <param name="isVisibleInLegend"> true if this object is visible in legend. </param>
        /// <returns> The Series. </returns>
        public Series GraphPoints( string seriesName, double[] abscissa, double[] ordinate, ChartMarker marker, bool isVisibleInLegend )
        {
            if ( abscissa is null )
                throw new ArgumentNullException( nameof( abscissa ) );
            if ( ordinate is null )
                throw new ArgumentNullException( nameof( ordinate ) );
            var ser = new Series( seriesName );
            if ( this.Series.IsUniqueName( seriesName ) )
            {
                this.Series.Add( ser );
            }
            else
            {
                ser = this.Series[seriesName];
            }

            ser.ChartArea = this.LastChartArea().Name;
            ser.ChartType = SeriesChartType.Point;
            ser.IsVisibleInLegend = isVisibleInLegend;
            // ser.MarkerBorderWidth = LineWeight;
            // ser.BorderWidth = LineWeight;
            ser.Points.DataBindXY( abscissa, ordinate );
            ser.MarkerStyle = marker.Style;
            ser.MarkerSize = marker.Size;
            ser.MarkerColor = marker.Color;
            return ser;
        }

        #endregion

        #region " SERIES SMART LABELS  "

        /// <summary> Setup Smart labels. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="chartingSeries"> The charting series. </param>
        /// <param name="labelColor">     The label color. </param>
        /// <param name="lineColor">      The lineColor. </param>
        /// <param name="fontStyle">      The font style. </param>
        /// <param name="fontSize">       size of the font. </param>
        private static void SmartLabels( Series chartingSeries, Color labelColor, Color lineColor, FontStyle fontStyle, int fontSize )
        {
            if ( chartingSeries is null )
                throw new ArgumentNullException( nameof( chartingSeries ) );
            chartingSeries.LabelForeColor = labelColor;
            chartingSeries.Font = new Font( chartingSeries.Font.Name, fontSize, fontStyle );
            chartingSeries.SmartLabelStyle.Enabled = true;
            chartingSeries.SmartLabelStyle.CalloutLineColor = lineColor;
            chartingSeries.SmartLabelStyle.CalloutStyle = LabelCalloutStyle.None;
            chartingSeries.SmartLabelStyle.IsMarkerOverlappingAllowed = false;
            chartingSeries.SmartLabelStyle.MinMovingDistance = 1d;
            chartingSeries.SmartLabelStyle.IsOverlappedHidden = false;
            chartingSeries.SmartLabelStyle.AllowOutsidePlotArea = LabelOutsidePlotAreaStyle.No;
            chartingSeries.SmartLabelStyle.CalloutLineAnchorCapStyle = LineAnchorCapStyle.None;
        }

        #endregion

        #region " SETTINGS "

        /// <summary> Gets or sets a list of colors of the use twenties. </summary>
        /// <value> A list of colors of the use twenties. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool UseTwentyColors { get; set; }

        /// <summary> The series colors 10. </summary>
        /// <remarks> plot colors (HTTPS://github.com/vega/vega/wiki/Scales#scale-range-literals)</remarks>
        private readonly string[] _SeriesColors10 = new string[] { "#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf" };

        /// <summary> The series colors 20. </summary>
        private readonly string[] _SeriesColors20 = new string[] { "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c", "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5", "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f", "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5" };

        /// <summary> Next series color. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <returns> A Color. </returns>
        public Color NextSeriesColor()
        {
            var colors = this.UseTwentyColors ? this._SeriesColors20 : this._SeriesColors10;
            return ColorTranslator.FromHtml( colors[this.SeriesCount() % colors.Length] );
        }

        /// <summary> Initializes the chart Palette. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void InitializeCustomPalette()
        {
            this.Palette = ChartColorPalette.None;
            this.PaletteCustomColors = new Color[] { Color.Blue, Color.DarkRed, Color.OliveDrab, Color.DarkOrange, Color.Purple, Color.Turquoise, Color.Green, Color.Crimson, Color.RoyalBlue, Color.Sienna, Color.Teal, Color.YellowGreen, Color.SandyBrown, Color.DeepSkyBlue, Color.Brown, Color.Cyan };
        }

        #endregion

        #region " TITLE "

        /// <summary>
        /// Add graph title. Repeated calls append new titles if unique or if new position.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="value"> The title. </param>
        public void AddTitle( string value )
        {
            // Defaults to top, black
            this.AddTitle( value, Docking.Top, Color.Black );
        }

        /// <summary>
        /// Add graph title. Repeated calls append new titles if unique or if new position.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="value">    The title. </param>
        /// <param name="position"> The position. </param>
        public void AddTitle( string value, Docking position )
        {
            this.AddTitle( value, position, Color.Black );
        }

        /// <summary>
        /// Add graph title. Repeated calls append new titles if unique or if new position.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="value">      The title. </param>
        /// <param name="position">   The position. </param>
        /// <param name="titleColor"> The title color. </param>
        public void AddTitle( string value, Docking position, Color titleColor )
        {
            foreach ( Title t in this.Titles )
            {
                if ( t.Docking == position )
                {
                    if ( !t.Text.Contains( value ) ) // Append
                    {
                        t.Text = $"{t.Text}{Constants.vbLf}{value}";
                    }

                    return;
                }
            }

            // If here, no match, add new title
            var newtitle = new Title( value, position );
            this.Titles.Add( newtitle );
            newtitle.ForeColor = titleColor;
            // newtitle.Font = new Font(newtitle.Font.Name, 8, FontStyle.Bold);
        }

        #endregion

        #region " TOOL TIP HANDLER "

        /// <summary> Event handler. Called by chart for tool tip events. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Tool tip event information. </param>
        public void OnGetTooltipText( object sender, ToolTipEventArgs e )
        {
            if ( e is null )
                return;
            if ( this.ChartArea is null )
                return;
            var h = e.HitTestResult;
            if ( this.ChartArea.AxisX.ScaleView.Zoomable )
            {
                switch ( h.ChartElementType )
                {
                    case ChartElementType.Axis:
                    case ChartElementType.AxisLabels:
                        {
                            e.Text = "Click-drag in graph area to zoom";
                            break;
                        }

                    case ChartElementType.ScrollBarZoomReset:
                        {
                            e.Text = "Zoom undo";
                            break;
                        }
                }
            }

            switch ( h.ChartElementType )
            {
                case ChartElementType.DataPoint:
                    {
                        e.Text = $"{h.Series.Name}{ControlChars.Lf}{h.Series.Points[h.PointIndex]}";
                        break;
                    }
            }
        }

        #endregion

        #region " PRINT "

        /// <summary> Prints a page. </summary>
        /// <remarks>
        /// The Chart control uses a PrintManager object to manage printing, which is encapsulated in the
        /// Chart.Printing property. The PrintManager has several methods that are used for very basic
        /// printing operations, such as PageSetup, PrintPreview and Print. These are pretty self
        /// explanatory, although it should be said that the Print method only prints a picture of the
        /// graph on your paper and nothing else. Although I played around with it a lot, I could not get
        /// the Print method to print very well, if such things like borders, etc. were anything except
        /// very simple. I found the best way to print reports based on the graph was to set up a
        /// PrintPage routine to handle printing, and add a Delegate to handle the specific graphs
        /// PrintPage event. In the PrintPage routine, you can design your report as you like.
        /// </remarks>
        /// <param name="eventArgs"> Print page event information. </param>
        public void PrintPage( PrintPageEventArgs eventArgs )
        {
            if ( eventArgs is null )
                return;

            // Create a memory stream to save the chart image    
            using var stream = new System.IO.MemoryStream();

            // Save the chart image to the stream    
            this.SaveImage( stream, System.Drawing.Imaging.ImageFormat.Bmp );

            // Draw the bitmap from the stream on the printer graphics
            eventArgs.Graphics.DrawImage( new Bitmap( stream ), eventArgs.MarginBounds );
        }

        #endregion

        #region " AXES LABELS "

        /// <summary>
        /// The array Round Mantissa defines bins for the value of A, from which nice round values are
        /// provided for major and minor tick intervals. For example, the first bin is for A = 1.0, and
        /// from that, we get the interval values of 0.20 and 0.05. The second bin is for A in (1.0,1.2],
        /// and from that we again get the interval values of 0.20 and 0.05. The algorithm is easily
        /// customized by changing the bin and interval values in these arrays.
        /// </summary>
        /// <value> The round mantissa. </value>
        private readonly double[] _RoundMantissa = new double[] { 1.0d, 1.2d, 1.4d, 1.6d, 1.8d, 2.0d, 2.5d, 3.0d, 4.0d, 5.0d, 6.0d, 8.0d, 10.0d };

        /// <summary> Gets or sets the round interval major. </summary>
        /// <value> The round interval major. </value>
        private readonly double[] _RoundIntervalMajor = new double[] { 0.2d, 0.2d, 0.2d, 0.2d, 0.2d, 0.5d, 0.5d, 0.5d, 0.5d, 1.0d, 1.0d, 2.0d, 2.0d };

        /// <summary> The round interval minor. </summary>
        /// <value> The round interval minor. </value>
        private readonly double[] _RoundIntervalMinor = new double[] { 0.05d, 0.05d, 0.05d, 0.05d, 0.05d, 0.1d, 0.1d, 0.1d, 0.1d, 0.2d, 0.2d, 0.5d, 0.5d };

        /// <summary>
        /// Gets nice round numbers for the axes. For the horizontal axis, minValue is always 0.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="minValue">      [in,out] The minimum value. </param>
        /// <param name="maxValue">      [in,out] The maximum value. </param>
        /// <param name="majorInterval"> [in,out] The major interval. </param>
        /// <param name="minorInterval"> [in,out] The minor interval. </param>
        public void GetNiceRoundNumbers( ref double minValue, ref double maxValue, ref double majorInterval, ref double minorInterval )
        {
            double min = Math.Min( minValue, maxValue );
            double max = Math.Max( minValue, maxValue );
            double delta = max - min; // The full range
                                      // Special handling for zero full range
            if ( delta == 0d )
            {
                // When min == max == 0, choose arbitrary range of 0 - 1
                if ( min == 0d )
                {
                    minValue = 0d;
                    maxValue = 1d;
                    majorInterval = 0.2d;
                    minorInterval = 0.5d;
                    return;
                }
                // min == max, but not zero, so set one to zero
                if ( min < 0d )
                {
                    max = 0d; // min-max are -|min| to 0
                }
                else
                {
                    min = 0d;
                } // min-max are 0 to +|max|

                delta = max - min;
            }

            int N = ChartingExtensions.Methods.Base10Exponent( delta );
            double tenToN = Math.Pow( 10d, N );
            double A = delta / tenToN;
            // At this point delta = A x Exp10(N), where
            // 1.0 <= A < 10.0 and N = integer exponent value
            // Now, based on A select a nice round interval and maximum value
            for ( int i = 0, loopTo = this._RoundMantissa.Length - 1; i <= loopTo; i++ )
            {
                if ( A <= this._RoundMantissa[i] )
                {
                    majorInterval = this._RoundIntervalMajor[i] * tenToN;
                    minorInterval = this._RoundIntervalMinor[i] * tenToN;
                    break;
                }
            }

            minValue = majorInterval * Math.Floor( min / majorInterval );
            maxValue = majorInterval * Math.Ceiling( max / majorInterval );
        }

        #endregion

        #region " ZOOM "

        /// <summary> Gets or sets or set the flag for Using GDI32 for zooming. </summary>
        /// <value> The use GDI 32. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool UseGdi32 { get; set; } = true;

        /// <summary>
        /// Gets or sets the flag for using nice round numbers based on the <see cref="_RoundMantissa"/>
        /// values.
        /// </summary>
        /// <value> The use nice round numbers. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool UseNiceRoundNumbers { get; set; } = true;

        /// <summary> Gets or sets the axis label format. </summary>
        /// <value> The axis label format. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public AxisLabelFormat AxisLabelFormat { get; set; } = AxisLabelFormat.Smart;

        /// <summary> Sets up the chart zoom example. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void SetupChartZoomExample()
        {
            this.Tag = new ChartScaleData( this );
            this.Series.Clear();
            int iMax = 5 * 360;
            double xMax, yMax, xMin, yMin;
            yMin = 0d;
            xMin = yMin;
            yMax = xMin;
            xMax = yMax;
            int jMin = -3;
            int jMax = 3;
            double piDiv180 = Math.PI / 180d;
            for ( int j = jMin, loopTo = jMax; j <= loopTo; j++ )
            {
                var s = new Series( $"exp {j}" ) {
                    IsVisibleInLegend = true,
                    IsXValueIndexed = false,
                    ChartType = SeriesChartType.FastLine,
                    BorderWidth = 2,
                    MarkerStyle = ( MarkerStyle ) Conversions.ToInteger( j - jMin + 1 ), // 0 = MarkerStyle.None
                    MarkerBorderWidth = 3,
                    MarkerStep = 1,
                    MarkerSize = 10
                };
                this.Series.Add( s );
                for ( int i = 0, loopTo1 = iMax - 1; i <= loopTo1; i++ )
                {
                    double mag = Math.Exp( 0.5d * (j * i) / iMax );
                    double Y = mag * Math.Sin( i * piDiv180 );
                    if ( i == 0 && j == 0 )
                    {
                        xMax = i;
                        xMin = xMax;
                        yMax = Y;
                        yMin = yMax;
                    }
                    else
                    {
                        if ( Y < yMin )
                            yMin = Y;
                        if ( Y > yMax )
                            yMax = Y;
                        if ( i < xMin )
                            xMin = i;
                        if ( i > xMax )
                            xMax = i;
                    }

                    _ = s.Points.AddXY( i, Y );
                }
            }

            double xInt, xMinInt, yInt, yMinInt;
            yMinInt = 0d;
            yInt = yMinInt;
            xMinInt = yInt;
            xInt = xMinInt;
            if ( this.UseNiceRoundNumbers )
            {
                this.GetNiceRoundNumbers( ref xMin, ref xMax, ref xInt, ref xMinInt );
                this.GetNiceRoundNumbers( ref yMin, ref yMax, ref yInt, ref yMinInt );
            }

            var area = this.ChartAreas.First();
            area.AxisX.Minimum = xMin;
            area.AxisX.Maximum = xMax;
            area.AxisX.Interval = xInt;
            area.AxisX.MinorTickMark.Interval = xMinInt;
            area.AxisX.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
            area.AxisX.MinorTickMark.Enabled = true;
            area.AxisX.MinorTickMark.Size = area.AxisX.MajorTickMark.Size / 2f;
            area.AxisY.Minimum = yMin;
            area.AxisY.Maximum = yMax;
            area.AxisY.Interval = yInt;
            area.AxisY.MinorTickMark.Interval = yMinInt;
            area.AxisY.MinorTickMark.TickMarkStyle = TickMarkStyle.OutsideArea;
            area.AxisY.MinorTickMark.Enabled = true;
            area.AxisY.MinorTickMark.Size = area.AxisY.MajorTickMark.Size / 2f;
            (this.Tag as ChartScaleData).UpdateAxisBaseData();
            ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
        }

        /// <summary>
        /// Toggles handling the dashed zoom rectangle when the mouse is dragged over a chart with the
        /// CTRL key pressed. The MouseDown, MouseMove and MouseUp events handle the creation and drawing
        /// of the Zoom Rectangle.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="addHandlerToggle"> True to add handler toggle. </param>
        public void ToggleDashedZoomRectangleMouseHandlers( bool addHandlerToggle )
        {
            if ( addHandlerToggle )
            {
                MouseDown += this.HandleDashedZoomRectangleMouseDown;
                MouseMove += this.HandleDashedZoomRectangleMouseMove;
                MouseUp += this.HandleDashedZoomRectangleMouseUp;
            }
            else
            {
                MouseDown -= this.HandleDashedZoomRectangleMouseDown;
                MouseMove -= this.HandleDashedZoomRectangleMouseMove;
                MouseUp -= this.HandleDashedZoomRectangleMouseUp;
            }
        }

        /// <summary> The zoom rectangle. </summary>
        private Rectangle _ZoomRect;

        /// <summary> Indicate that we are dragging. </summary>
        private bool _ZoomingNow = false;

        /// <summary> Handles the dashed zoom rectangle mouse down. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Mouse event information. </param>
        private void HandleDashedZoomRectangleMouseDown( object sender, MouseEventArgs e )
        {
            if ( LicenseManager.UsageMode == LicenseUsageMode.Designtime )
                return;
            if ( sender is not Chart chart )
                return;
            _ = this.Focus();

            // Test for CTRL+Left Single Click to start displaying selection box
            if ( e.Button == MouseButtons.Left && e.Clicks == 1 && (ModifierKeys & Keys.Control) != 0 )
            {
                this._ZoomingNow = true;
                this._ZoomRect.Location = e.Location;
                this._ZoomRect.Height = 0;
                this._ZoomRect.Width = this._ZoomRect.Height;
                // Draw the new selection rectangle
                ChartingExtensions.Methods.DrawZoomRect( chart, this._ZoomRect, this.UseGdi32 );
            }

            _ = this.Focus();
        }

        /// <summary> Handles the dashed zoom rectangle mouse move. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Mouse event information. </param>
        private void HandleDashedZoomRectangleMouseMove( object sender, MouseEventArgs e )
        {
            if ( sender is not Chart chart )
                return;
            if ( this._ZoomingNow )
            {
                // Redraw the old selection rectangle, which erases it
                ChartingExtensions.Methods.DrawZoomRect( chart, this._ZoomRect, this.UseGdi32 );
                this._ZoomRect.Width = e.X - this._ZoomRect.Left;
                this._ZoomRect.Height = e.Y - this._ZoomRect.Top;
                // Draw the new selection rectangle
                ChartingExtensions.Methods.DrawZoomRect( chart, this._ZoomRect, this.UseGdi32 );
            }
        }

        /// <summary> Handles the dashed zoom rectangle mouse up. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Mouse event information. </param>
        private void HandleDashedZoomRectangleMouseUp( object sender, MouseEventArgs e )
        {
            if ( sender is not Chart chart )
                return;
            if ( this._ZoomingNow && e.Button == MouseButtons.Left )
            {
                // Redraw the selection rectangle, which erases it
                ChartingExtensions.Methods.DrawZoomRect( chart, this._ZoomRect, this.UseGdi32 );
                if ( this._ZoomRect.Width != 0 && this._ZoomRect.Height != 0 )
                {
                    // Just in case the selection was dragged from lower right to upper left
                    this._ZoomRect = new Rectangle( Math.Min( this._ZoomRect.Left, this._ZoomRect.Right ), Math.Min( this._ZoomRect.Top, this._ZoomRect.Bottom ), Math.Abs( this._ZoomRect.Width ), Math.Abs( this._ZoomRect.Height ) );
                    // no Shift so Zoom in.
                    this.ZoomInToZoomRect( chart.Tag as ChartScaleData, chart.ChartAreas.First() );
                }

                this._ZoomingNow = false;
            }
        }

        /// <summary>
        /// This method zooms in to the area contained within the portion of zoomRect that overlaps the
        /// chart innerPlotRectangle. zoomRect is positioned in the client rectangle of the chart in
        /// which the zoom was initiated with a Left MouseDown event.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="chartScaleData"> Information describing the chart scale. </param>
        /// <param name="area">           The area. </param>
        private void ZoomInToZoomRect( ChartScaleData chartScaleData, ChartArea area )
        {
            if ( this._ZoomRect.Width == 0 || this._ZoomRect.Height == 0 )
            {
                return;
            }

            var r = this._ZoomRect;

            // Get overlap of zoomRect and the innerPlotRectangle
            var ipr = chartScaleData.InnerPlotRectangle;
            if ( !r.IntersectsWith( ipr ) )
            {
                return;
            }

            r.Intersect( ipr );
            if ( !chartScaleData.IsZoomed )
            {
                chartScaleData.IsZoomed = true;
                chartScaleData.UpdateAxisBaseData();
            }

            this.SetZoomAxisScale( area, area.AxisX, r.Left, r.Right );
            this.SetZoomAxisScale( area, area.AxisY, r.Bottom, r.Top );
        }

        /// <summary>
        /// Sets the axis parameters to an non-zoomed state showing all data, and nice round numbers and
        /// intervals for the axis labels Note: at this stage the axis is unchanged, and this has not
        /// been tested with reversed axes.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="area">  The area. </param>
        /// <param name="axis">  . </param>
        /// <param name="pxLow"> low pixel position in chart client rectangle coordinates. </param>
        /// <param name="pxHi">  Hi pixel position in chart client rectangle coordinates. </param>
        private void SetZoomAxisScale( ChartArea area, Axis axis, int pxLow, int pxHi )
        {
            double minValue = Math.Max( axis.Minimum, axis.PixelPositionToValue( pxLow ) );
            double maxValue = Math.Min( axis.Maximum, axis.PixelPositionToValue( pxHi ) );
            double axisInterval = 0d;
            double axisIntMinor = 0d;
            if ( this.UseNiceRoundNumbers )
            {
                this.GetNiceRoundNumbers( ref minValue, ref maxValue, ref axisInterval, ref axisIntMinor );
            }
            else
            {
                axisInterval = (maxValue - minValue) / 5.0d;
            }

            axis.Minimum = minValue;
            axis.Maximum = maxValue;
            axis.Interval = axisInterval;
            axis.MinorTickMark.Interval = axisIntMinor;
            ChartingExtensions.Methods.SetAxisFormats( area, this.AxisLabelFormat );
        }

        #endregion

        #region " CONTEXT MENU "

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _ZoomOutToolStripMenuItem;

        private ToolStripMenuItem ZoomOutToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ZoomOutToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ZoomOutToolStripMenuItem != null )
                {
                    this._ZoomOutToolStripMenuItem.Click -= this.ZoomOutToolStripMenuItem_Click;
                }

                this._ZoomOutToolStripMenuItem = value;
                if ( this._ZoomOutToolStripMenuItem != null )
                {
                    this._ZoomOutToolStripMenuItem.Click += this.ZoomOutToolStripMenuItem_Click;
                }
            }
        }

        private ToolStripSeparator _SeparatorToolStripItem;

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _NiceRoundNumbersToolStripMenuItem;

        private ToolStripMenuItem NiceRoundNumbersToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._NiceRoundNumbersToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._NiceRoundNumbersToolStripMenuItem != null )
                {
                    this._NiceRoundNumbersToolStripMenuItem.Click -= this.NiceRoundNumbersToolStripMenuItem_Click;
                }

                this._NiceRoundNumbersToolStripMenuItem = value;
                if ( this._NiceRoundNumbersToolStripMenuItem != null )
                {
                    this._NiceRoundNumbersToolStripMenuItem.Click += this.NiceRoundNumbersToolStripMenuItem_Click;
                }
            }
        }

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _ZoomWithGDI32ToolStripMenuItem;

        private ToolStripMenuItem ZoomWithGDI32ToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ZoomWithGDI32ToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ZoomWithGDI32ToolStripMenuItem != null )
                {
                    this._ZoomWithGDI32ToolStripMenuItem.Click -= this.ZoomWithGDI32ToolStripMenuItem_Click;
                }

                this._ZoomWithGDI32ToolStripMenuItem = value;
                if ( this._ZoomWithGDI32ToolStripMenuItem != null )
                {
                    this._ZoomWithGDI32ToolStripMenuItem.Click += this.ZoomWithGDI32ToolStripMenuItem_Click;
                }
            }
        }

        /// <summary> The with events. </summary>
        private ToolStripSeparator _SeparatorToolStripItem1;

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _DefaultFormatToolStripMenuItem;

        private ToolStripMenuItem DefaultFormatToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DefaultFormatToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DefaultFormatToolStripMenuItem != null )
                {
                    this._DefaultFormatToolStripMenuItem.Click -= this.DefaultFormatToolStripMenuItem_Click;
                }

                this._DefaultFormatToolStripMenuItem = value;
                if ( this._DefaultFormatToolStripMenuItem != null )
                {
                    this._DefaultFormatToolStripMenuItem.Click += this.DefaultFormatToolStripMenuItem_Click;
                }
            }
        }

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _SmartFormatToolStripMenuItem;

        private ToolStripMenuItem SmartFormatToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SmartFormatToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SmartFormatToolStripMenuItem != null )
                {
                    this._SmartFormatToolStripMenuItem.Click -= this.SmartFormatToolStripMenuItem_Click;
                }

                this._SmartFormatToolStripMenuItem = value;
                if ( this._SmartFormatToolStripMenuItem != null )
                {
                    this._SmartFormatToolStripMenuItem.Click += this.SmartFormatToolStripMenuItem_Click;
                }
            }
        }

        /// <summary> The with events. </summary>
        private ToolStripMenuItem _WholeNumberFormatToolStripMenuItem;

        private ToolStripMenuItem WholeNumberFormatToolStripMenuItem
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._WholeNumberFormatToolStripMenuItem;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._WholeNumberFormatToolStripMenuItem != null )
                {
                    this._WholeNumberFormatToolStripMenuItem.Click -= this.WholeNumberFormatToolStripMenuItem_Click;
                }

                this._WholeNumberFormatToolStripMenuItem = value;
                if ( this._WholeNumberFormatToolStripMenuItem != null )
                {
                    this._WholeNumberFormatToolStripMenuItem.Click += this.WholeNumberFormatToolStripMenuItem_Click;
                }
            }
        }

        /// <summary> Populate context menu. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        public void PopulateContextMenu()
        {
            this.ZoomOutToolStripMenuItem = new ToolStripMenuItem() { Text = "Zoom Out", CheckOnClick = true };
            this.NiceRoundNumbersToolStripMenuItem = new ToolStripMenuItem() { Text = "Nice Round Numbers", CheckOnClick = true };
            this.ZoomWithGDI32ToolStripMenuItem = new ToolStripMenuItem() { Text = "Zoom /w GDI", CheckOnClick = true };
            this.DefaultFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Default Format", CheckOnClick = true };
            this.SmartFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Smart Format", CheckOnClick = true };
            this.WholeNumberFormatToolStripMenuItem = new ToolStripMenuItem() { Text = "Whole Number Format", CheckOnClick = true };
            this._SeparatorToolStripItem = new ToolStripSeparator();
            this._SeparatorToolStripItem1 = new ToolStripSeparator();
            this.ContextMenuStrip = new ContextMenuStrip();
            this.ContextMenuStrip.Items.AddRange( new ToolStripItem[] { this.ZoomOutToolStripMenuItem, this._SeparatorToolStripItem, this.NiceRoundNumbersToolStripMenuItem, this.ZoomWithGDI32ToolStripMenuItem, this._SeparatorToolStripItem1, this.DefaultFormatToolStripMenuItem, this.SmartFormatToolStripMenuItem, this.WholeNumberFormatToolStripMenuItem } );
        }

        /// <summary> Toggle context menu. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="enabled"> True to enable, false to disable. </param>
        public void ToggleContextMenu( bool enabled )
        {
            if ( enabled )
            {
                this.ContextMenuStrip.Opening += this.HandleContextMenuStripOpening;
            }
            else
            {
                this.ContextMenuStrip.Opening -= this.HandleContextMenuStripOpening;
            }
        }

        /// <summary> Handles the context menu strip opening. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        private void HandleContextMenuStripOpening( object sender, CancelEventArgs e )
        {
            if ( this._ZoomingNow )
            {
                e.Cancel = true;
                return;
            }

            this.ZoomOutToolStripMenuItem.Visible = (this.Tag as ChartScaleData).IsZoomed;
            this._SeparatorToolStripItem.Visible = this.ZoomOutToolStripMenuItem.Visible;
            this.ZoomWithGDI32ToolStripMenuItem.Checked = this.UseGdi32;
            this.NiceRoundNumbersToolStripMenuItem.Checked = this.UseNiceRoundNumbers;
            this.DefaultFormatToolStripMenuItem.Checked = this.AxisLabelFormat == AxisLabelFormat.None;
            this.SmartFormatToolStripMenuItem.Checked = this.AxisLabelFormat == AxisLabelFormat.Smart;
            this.WholeNumberFormatToolStripMenuItem.Checked = this.AxisLabelFormat == AxisLabelFormat.WholeNumber;
        }

        /// <summary> Zoom out tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ZoomOutToolStripMenuItem_Click( object sender, EventArgs e )
        {
            (this.Tag as ChartScaleData).ResetAxisScale();
            (this.Tag as ChartScaleData).IsZoomed = false;
        }

        /// <summary> Nice round numbers tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void NiceRoundNumbersToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.UseNiceRoundNumbers = this.NiceRoundNumbersToolStripMenuItem.Checked;
            this.SetupChartZoomExample();
        }

        /// <summary> Zoom with GDI 32 tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ZoomWithGDI32ToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.UseGdi32 = this.ZoomWithGDI32ToolStripMenuItem.Checked;
        }


        /// <summary> Whole number format tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void WholeNumberFormatToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.DefaultFormatToolStripMenuItem.Checked = false;
            this.SmartFormatToolStripMenuItem.Checked = false;
            this.WholeNumberFormatToolStripMenuItem.Checked = true;
            this.AxisLabelFormat = AxisLabelFormat.WholeNumber;
            ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
        }

        /// <summary> Smart format tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SmartFormatToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.DefaultFormatToolStripMenuItem.Checked = false;
            this.SmartFormatToolStripMenuItem.Checked = true;
            this.WholeNumberFormatToolStripMenuItem.Checked = false;
            this.AxisLabelFormat = AxisLabelFormat.Smart;
            ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
        }

        /// <summary> Default format tool strip menu item click. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void DefaultFormatToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.DefaultFormatToolStripMenuItem.Checked = true;
            this.SmartFormatToolStripMenuItem.Checked = false;
            this.WholeNumberFormatToolStripMenuItem.Checked = false;
            this.AxisLabelFormat = AxisLabelFormat.None;
            ChartingExtensions.Methods.SetAxisFormats( this.ChartAreas.First(), this.AxisLabelFormat );
        }

        #endregion

    }

    /// <summary> A chart marker. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public struct ChartMarker
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        /// <param name="style"> The style. </param>
        /// <param name="size">  The size. </param>
        /// <param name="color"> The color. </param>
        public ChartMarker( MarkerStyle style, int size, Color color )
        {
            this.Size = size;
            this.Color = color;
            this.Style = style;
        }

        /// <summary> Gets or sets the size in points. </summary>
        /// <value> The size. </value>
        public int Size { get; set; }

        /// <summary> Gets or sets the style. </summary>
        /// <value> The style. </value>
        public MarkerStyle Style { get; set; }

        /// <summary> Gets or sets the color. </summary>
        /// <value> The color. </value>
        public Color Color { get; set; }
    }

    /// <summary> Values that represent axis label formats. </summary>
    /// <remarks> David, 2020-10-20. </remarks>
    public enum AxisLabelFormat
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "None, String Empty" )]
        None,

        /// <summary> An enum constant representing the whole number option. </summary>
        [Description( "Whole number (F0)" )]
        WholeNumber,

        /// <summary> An enum constant representing the smart option. </summary>
        [Description( "Smart format" )]
        Smart
    }
}
