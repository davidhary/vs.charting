﻿
namespace isr.Visuals.Charting.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-30 </para>
    /// </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-20. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 753;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Charting Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Charting Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Visuals.Charting.Library";
    }
}