Imports System.Windows.Forms.DataVisualization.Charting
Imports System.IO
Imports System.Drawing.Printing

Friend Module GraphFunctions

    ''' <summary> The current graph. </summary>
    Private _CurrentGraph As Chart

    ''' <summary> Saves a graph. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="currentChart"> The current chart. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function SaveGraph(ByVal currentChart As Chart) As Boolean
        Dim saveDialog As New SaveFileDialog() With {
            .Filter = "Bitmap (*.bmp)|*.bmp|JPEG (*.jpg)|*.jpg|EMF (*.emf)|*.emf|PNG (*.png)|*.png|GIF (*.gif)|*.gif|TIFF (*.tif)|*.tif",
            .FilterIndex = 2,
            .RestoreDirectory = True
        }

        If saveDialog.ShowDialog() = DialogResult.OK Then ' Set image file format
            Dim format As ChartImageFormat = ChartImageFormat.Bmp
            If saveDialog.FileName.EndsWith("jpg") Then
                format = ChartImageFormat.Jpeg
            Else
                If saveDialog.FileName.EndsWith("emf") Then
                    format = ChartImageFormat.Emf
                Else
                    If saveDialog.FileName.EndsWith("gif") Then
                        format = ChartImageFormat.Gif
                    Else
                        If saveDialog.FileName.EndsWith("png") Then
                            format = ChartImageFormat.Png
                        Else
                            If saveDialog.FileName.EndsWith("tif") Then
                                format = ChartImageFormat.Tiff
                            End If
                        End If
                    End If
                End If
            End If
            currentChart.SaveImage(saveDialog.FileName, format)
            Return True
        Else
            Return False
        End If

    End Function

    ''' <summary> Copies to clipboard described by currentChart. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="currentChart"> The current chart. </param>
    Public Sub CopyToClipboard(ByVal currentChart As Chart)

        Dim stream As New System.IO.MemoryStream() ' Create a memory stream to save the chart image    
        currentChart.SaveImage(stream, System.Drawing.Imaging.ImageFormat.Bmp) ' Save the chart image to the stream    
        Dim bmp As New Bitmap(stream) ' Create a bitmap using the stream    
        Clipboard.SetDataObject(bmp) ' Save the bitmap to the clipboard    

    End Sub

    ''' <summary> Print graph. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="currentChart"> The current chart. </param>
    Public Sub PrintGraph(ByVal currentChart As Chart)

        _CurrentGraph = currentChart
        _CurrentGraph.Printing.PrintDocument = New PrintDocument
        AddHandler _CurrentGraph.Printing.PrintDocument.PrintPage, AddressOf PrintPage
        _CurrentGraph.Printing.Print(True)

    End Sub

    ''' <summary> Preview graph. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="currentChart"> The current chart. </param>
    Public Sub PreviewGraph(ByVal currentChart As Chart)

        currentChart.Printing.PrintPreview()

    End Sub

    ''' <summary> Print page. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="ev">     Print page event information. </param>
    Private Sub PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)

        '' Calculate title string position
        Dim titlePosition As New Rectangle(ev.MarginBounds.X, ev.MarginBounds.Y, ev.MarginBounds.Width, ev.MarginBounds.Height)
        Dim stream As New System.IO.MemoryStream() ' Create a memory stream to save the chart image    
        _CurrentGraph.SaveImage(stream, System.Drawing.Imaging.ImageFormat.Bmp) ' Save the chart image to the stream    
        Dim bmp As New Bitmap(stream) ' Create a bitmap using the stream    
        Dim recPic As New Rectangle(ev.MarginBounds.X, ev.MarginBounds.Y, bmp.Width, bmp.Height)
        Dim fontTitle As New Font("Times New Roman", 16)
        Dim chartTitle As String
        If _CurrentGraph.Titles.Count > 0 Then
            chartTitle = _CurrentGraph.Titles(0).Text
        End If
        'Dim titleSize As SizeF = ev.Graphics.MeasureString(chartTitle, fontTitle)
        ' titlePosition.Height = CInt(titleSize.Height)

        '' Draw charts title
        'Dim format As New StringFormat()
        'format.Alignment = StringAlignment.Center
        'ev.Graphics.DrawString(chartTitle, fontTitle, Brushes.Black, titlePosition, format)

        '' Calculate first chart position rectangle
        Dim chartPosition As New Rectangle(ev.MarginBounds.X, titlePosition.Bottom, _CurrentGraph.Size.Width, _CurrentGraph.Size.Height)

        '' Align first chart position on the page
        Dim chartWidthScale As Single = 1 ' CSng(ev.MarginBounds.Width) / 2.0F / CSng(chartPosition.Width)
        Dim chartHeightScale As Single = 1 'CSng(ev.MarginBounds.Height) / CSng(chartPosition.Height)
        chartPosition.Width = CInt(chartPosition.Width * Math.Min(chartWidthScale, chartHeightScale))
        chartPosition.Height = CInt(chartPosition.Height * Math.Min(chartWidthScale, chartHeightScale))

        ' Draw first chart on the printer graphics
        'CurrentGraph.Printing.PrintPaint(ev.Graphics, chartPosition)
        ev.Graphics.DrawImage(bmp, recPic)

    End Sub

    ''' <summary> Query if 'businessDay' is holiday. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="businessDay"> The business day. </param>
    ''' <returns> True if holiday, false if not. </returns>
    Public Function IsHoliday(ByVal businessDay As Date) As Boolean

        Dim startDate As Date = #2009-12-19#
        Dim endDate As Date = #2010-01-03#
        Return businessDay >= startDate And businessDay <= endDate

    End Function

End Module

