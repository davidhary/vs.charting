
Imports System.Windows.Forms.DataVisualization.Charting

''' <summary> Form for viewing the product sales. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Friend Class ProductSalesForm

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="currentProduct"> The current product. </param>
    Public Sub New(ByVal currentProduct As Product)

        Me.InitializeComponent()
        Me._CurrentProduct = currentProduct ' Initialize the current product

    End Sub

#End Region

#Region " CLASS VARIABLES "

    ''' <summary> The current product. </summary>
    Private ReadOnly _CurrentProduct As Product 'Current product whose results need to be shown. Passed and set through constructor.

    ''' <summary> The months. </summary>
    Private _Months As List(Of String) 'A list of the past three full calendar months plus current month by name

    ''' <summary> The start date. </summary>
    Private _StartDate As Date = #2009-11-01# 'Start Date for Demo App figures

    ''' <summary> The end date. </summary>
    Private _EndDate As Date = #2010-02-19# 'End Date for Demo App figures

#End Region

#Region " CLASS ROUTINES "

    ''' <summary> Sets the graph. </summary>
    ''' <remarks>
    ''' David, 2020-10-24. Fetches a DataTable from current product DailySales routine and sets it as
    ''' the BindingSource for the Graph. Calculates and sets result details on form.
    ''' </remarks>
    Private Sub SetGraph()

        Dim MasterSalesDataTable As DataTable = Me._CurrentProduct.DailySales(Me._StartDate, Me._EndDate, My.Settings.SqlConnectionString)
        Me.MscDailySales.DataSource = MasterSalesDataTable
        Me.MscDailySales.Series(0).XValueMember = "Date"
        Me.MscDailySales.Series(0).YValueMembers = "UnitsSold"
        Me.MscDailySales.DataBind()
        Dim dblMean As Double = Me.MscDailySales.DataManipulator.Statistics.Mean("Series1") ' uses data manipulator to return mean
        Me.LblAveSales.Text = Format(dblMean, "0.00")
        Dim lstDates As New List(Of Date)
        Dim lstValues As New List(Of Double)
        Dim intTotalUnitsSold As Integer = 0
        For i As Integer = 0 To MasterSalesDataTable.Rows.Count - 1
            intTotalUnitsSold += CInt(MasterSalesDataTable.Rows(i).Item(1))
            lstDates.Add(CDate(MasterSalesDataTable.Rows(i).Item(0)))
            lstValues.Add(dblMean)
        Next
        Me.MscDailySales.Series(1).Points.DataBindXY(lstDates.ToArray, lstValues.ToArray)
        Me.LblUnitSales.Text = CStr(intTotalUnitsSold)
        Me.LblRetailValue.Text = Format(intTotalUnitsSold * Me._CurrentProduct.RetailPrice, "0.00")

    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Form product sales load. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FrmProductSales_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Text = Me._CurrentProduct.Name
        Me._Months = New List(Of String)
        For i As Integer = 0 To 3
            Me._Months.Add(MonthName(Me._EndDate.AddMonths(-3 + i).Month) & ", " & Year(Now.AddMonths(-3 + i)))
            Me.MscUnitSales.Series(0).Points.AddXY(Me._Months(i), Me._CurrentProduct.UnitSales(i)) 'Adding points to mini Graph
        Next
        Try 'Needs to call database so use try catch
            Me.SetGraph()
        Catch ex As OleDb.OleDbException 'Only need to catch database exceptions. All other exceptions should be caught in development
            MsgBox("There was an error retrieving sales data for " & Me._CurrentProduct.Name & ". Please try again. If the problem persists, please inform your System Administrator.")
            Me.Close()
        End Try

    End Sub

    ''' <summary> Button copy click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnCopy.Click

        CopyToClipboard(Me.MscDailySales) 'Call graph function

    End Sub

    ''' <summary> Button save click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSave.Click

        Try 'Save graph routine use System.IO so might throw exception
            If SaveGraph(Me.MscDailySales) Then
                MsgBox("Graph successfully saved.")
            End If
        Catch ex As System.IO.IOException 'Only need to catch IO exceptions. All other exceptions should be caught in development
            MsgBox("There was an error saving the Graph. Please try again. If the problem persists, please inform your System Administrator.")
        End Try

    End Sub

    ''' <summary> Button close click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnQuit.Click

        Me.Close()

    End Sub

    ''' <summary> Button print preview click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click

        PreviewGraph(Me.MscDailySales) 'Call graph function

    End Sub

    ''' <summary> Button print click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPrint.Click

        Try 'Print graph routine might throw printing exception
            PrintGraph(Me.MscDailySales)
        Catch ex As System.Drawing.Printing.InvalidPrinterException 'Only need to catch Printing exceptions. All other exceptions should be caught in development
            MsgBox("There was an error printing the Report. Please try again. If the problem persists, please inform your System Administrator.")
        End Try

    End Sub

    ''' <summary> Button reset click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnReset.Click 'Set graph back to square one

        Me._StartDate = #2009-11-01#
        Me._EndDate = #2010-02-19#
        Me.SetGraph()
        Me.BtnReset.Visible = False

    End Sub

#End Region

#Region " GRAPH EVENTS "

    ''' <summary> Msc unit sales get tool tip text. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tool tip event information. </param>
    Private Sub MscUnitSales_GetToolTipText(ByVal sender As Object, ByVal e As System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs) Handles MscUnitSales.GetToolTipText

        Select Case e.HitTestResult.ChartElementType
            Case ChartElementType.DataPoint
                If e.HitTestResult.PointIndex >= 0 Then
                    e.Text = "Click to Zoom in on " & Me._Months(e.HitTestResult.PointIndex)
                End If
        End Select

    End Sub

    ''' <summary> Msc unit sales mouse down. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub MscUnitSales_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MscUnitSales.MouseDown

        Dim htrResult As HitTestResult = Me.MscUnitSales.HitTest(e.X, e.Y)
        If htrResult.ChartElementType = ChartElementType.DataPoint And htrResult.PointIndex >= 0 Then 'Check if mouse is over Data Point or Legend Item
            Select Case htrResult.PointIndex 'Each point represents one month sales
                Case 0
                    Me._StartDate = #2009-11-01# 'Set Dates
                    Me._EndDate = #2009-11-30#
                Case 1
                    Me._StartDate = #2009-12-01#
                    Me._EndDate = #2009-12-31#
                Case 2
                    Me._StartDate = #2010-01-01#
                    Me._EndDate = #2010-01-31#
                Case Else
                    Me._StartDate = #2010-02-01#
                    Me._EndDate = #2010-02-19#
            End Select
            Me.SetGraph() 'Set Up Graph
            Me.BtnReset.Visible = True 'Make sure there is a way to return to full graph
        End If

    End Sub

    ''' <summary> Msc unit sales mouse move. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub MscUnitSales_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles MscUnitSales.MouseMove

        Dim htrResult As HitTestResult = Me.MscUnitSales.HitTest(e.X, e.Y)
        For Each dp As DataPoint In Me.MscUnitSales.Series(0).Points  'Go through points setting design elements back to default
            dp.BackSecondaryColor = Color.White
            dp.BackHatchStyle = ChartHatchStyle.None
            dp.BorderWidth = 0
        Next dp
        'If users mouse hovers over a datapoint or it's equivalent Legend Item then set cursor to hand to indicate that it is a link
        'Also we use some design elements to indicate which DataPoint is active
        If htrResult.PointIndex >= 0 Then
            If htrResult.ChartElementType = ChartElementType.DataPoint Or htrResult.ChartElementType = ChartElementType.LegendItem Then
                Me.Cursor = Cursors.Hand
                Dim dpSelected As DataPoint
                dpSelected = Me.MscUnitSales.Series(0).Points(htrResult.PointIndex)
                dpSelected.BackSecondaryColor = Color.Olive
                dpSelected.BorderColor = Color.White
                dpSelected.BorderWidth = 1
            End If
        Else
            'Set cursor back to default when leaving selected datapoint
            Me.Cursor = Cursors.Default
        End If

    End Sub

#End Region

End Class
