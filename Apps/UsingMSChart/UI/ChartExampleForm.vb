Imports System.Windows.Forms.DataVisualization.Charting
Imports System.ComponentModel
Imports System.Drawing.Imaging

''' <summary> Form for viewing the chart example. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Friend Class ChartExampleForm

#Region " LOCAL VARIABLES "

    ''' <summary> The categories. </summary>
    Private _Categories As List(Of Category) 'List of Product Categories

    ''' <summary> The data points. </summary>
    Private _DataPoints As List(Of AppDataPoint)

    ''' <summary> The current category. </summary>
    Private _CurrentCategory As Category

    ''' <summary> Filename of the error log file. </summary>
    Private ReadOnly _ErrorLogFileName As String = Application.StartupPath & "\" & My.Settings.ErrorLog

    ''' <summary> The gross sales. </summary>
    Private _GrossSales As List(Of Decimal) ' Collection of decimals for Gross Sales Last 4 months

    ''' <summary> The current stage. </summary>
    Private _CurrentStage As GraphStage 'Flag to tell which type of Graph is current

    ''' <summary> The graph month. </summary>
    Private _GraphMonth As GraphMonth

#End Region

#Region " LOCAL ENUMS "

    ''' <summary> Values that represent graph stages. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Enum GraphStage

        ''' <summary> An enum constant representing the pyramid option. </summary>
        Pyramid

        ''' <summary> An enum constant representing the doughnut option. </summary>
        Doughnut

    End Enum

    ''' <summary> Values that represent graph months. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Enum GraphMonth

        ''' <summary> An enum constant representing the month 1 option. </summary>
        Month1

        ''' <summary> An enum constant representing the month 2 option. </summary>
        Month2

        ''' <summary> An enum constant representing the month 3 option. </summary>
        Month3

        ''' <summary> An enum constant representing the current option. </summary>
        Current

    End Enum

#End Region

#Region " LOCAL ROUTINES "

    ''' <summary> Sets graph month. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub SetGraphMonth() 'Sets enum GraphMonth depending on which RadioButton is checked

        If Me.Month1RadioButton.Checked = True Then
            Me._GraphMonth = GraphMonth.Month1
        ElseIf Me.Month2RadioButton.Checked Then
            Me._GraphMonth = GraphMonth.Month2
        ElseIf Me.Month3RadioButton.Checked Then
            Me._GraphMonth = GraphMonth.Month3
        Else
            Me._GraphMonth = GraphMonth.Current
        End If

    End Sub

    ''' <summary> Sets up the graph. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub SetUpGraph()

        Dim blnPercent As Boolean = Me.PercentRadioButton.Checked 'Select whether values shown as Retail or as Percent
        Me.SalesChart.Series(0).Points.Clear() 'Clear Graph
        Me.SetGraphMonth() 'Set enum GraphMonth
        Select Case Me._CurrentStage
            Case GraphStage.Doughnut
                Me.SetPercentageChoice(True)
                Me.ReturnButton.Visible = True
                Me.DetailsGrid.Columns(0).HeaderText = "Product"
                Me.SalesChart.Series(0).ChartType = SeriesChartType.Doughnut
                Me.SalesChart.Series(0)("CollectedThreshold") = CStr(Me.GroupPercentageTrackBar.Value)
                Me.SalesChart.Titles(0).Text = Me._CurrentCategory.Name.ToUpper & " - SALES BY PRODUCT"
                Me._DataPoints = Me.SetUpPiePoints(blnPercent, Me.UnitSalesRadioButton.Checked)
                Me._DataPoints.Sort()
            Case Else
                Me.SetPercentageChoice(False)
                Me.ReturnButton.Visible = False
                Me.SalesChart.Series(0).ChartType = SeriesChartType.Pyramid
                Me.SalesChart.Series(0).IsXValueIndexed = False
                Me.SalesChart.Titles(0).Text = "SALES BY CATEGORY - PERCENT OF GROSS SALES"
                Me._DataPoints = Me.SetUpPyramidPoints(blnPercent)
        End Select
        Me.DetailsGrid.Rows.Clear()
        Dim decGross As Decimal = 0D
        For intDataPoints As Integer = 0 To Me._DataPoints.Count - 1
            Me.SalesChart.Series(0).Points.AddXY(Me._DataPoints(intDataPoints).DataKey, Format(Me._DataPoints(intDataPoints).DataValue, "#,##0.00"))
            Dim strRow() As String = {CStr(Me._DataPoints(intDataPoints).DataKey), Format(Me._DataPoints(intDataPoints).DataValue, "#,##0.00")}
            decGross += CDec(Me._DataPoints(intDataPoints).DataValue)
            Me.DetailsGrid.Rows.Add(strRow)
        Next
        Me.DetailsGrid.Rows.Add()
        Dim strGross() As String = {"Total ", Format(decGross, "#,##0.00")}
        Me.DetailsGrid.Rows.Add(strGross)

    End Sub

    ''' <summary> Sets percentage choice. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="isVisible"> True if is visible, false if not. </param>
    Private Sub SetPercentageChoice(ByVal isVisible As Boolean)

        Me.GroupPercentageTrackBar.Visible = isVisible
        Me.Limen10Label.Visible = isVisible
        Me.Limen20Label.Visible = isVisible
        Me.PercentageValueLabel.Visible = isVisible
        Me.GroupPercentagePanel.Visible = isVisible

    End Sub

    ''' <summary> Sets up the pyramid points. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="isPercent"> True if is percent, false if not. </param>
    ''' <returns> A List(Of AppDataPoint) </returns>
    Private Function SetUpPyramidPoints(ByVal isPercent As Boolean) As List(Of AppDataPoint)

        Dim lstDP As New List(Of AppDataPoint)
        For intCategories As Integer = 0 To Me._Categories.Count - 1
            Dim newDP As New AppDataPoint With {
                .DataValue = If(isPercent,
                If(Me._GrossSales(Me._GraphMonth) > 0, (Me._Categories(intCategories).TotalSales(Me._GraphMonth) / Me._GrossSales(Me._GraphMonth)) * 100, 0F),
                Me._Categories(intCategories).TotalSales(Me._GraphMonth)),
                .DataKey = Me._Categories(intCategories).Name,
                .Tag = Me._Categories(intCategories)
            }
            lstDP.Add(newDP)
        Next
        Return lstDP

    End Function

    ''' <summary> Sets up the pie points. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="isPercent">   True if is percent, false if not. </param>
    ''' <param name="isUnitSales"> True if is unit sales, false if not. </param>
    ''' <returns> A List(Of AppDataPoint) </returns>
    Private Function SetUpPiePoints(ByVal isPercent As Boolean, ByVal isUnitSales As Boolean) As List(Of AppDataPoint)

        Dim lstDP As New List(Of AppDataPoint)
        For intProducts As Integer = 0 To Me._CurrentCategory.Products.Count - 1
            Dim newDP As New AppDataPoint
            If isPercent Then
                newDP.DataValue = If(Me._CurrentCategory.Products(intProducts).UnitSales(Me._GraphMonth) > 0,
                    ((Me._CurrentCategory.Products(intProducts).UnitSales(Me._GraphMonth) * Me._CurrentCategory.Products(intProducts).RetailPrice) / Me._CurrentCategory.TotalSales(Me._GraphMonth)) * 100,
                    0)
            ElseIf isUnitSales Then
                newDP.DataValue = Me._CurrentCategory.Products(intProducts).UnitSales(Me._GraphMonth)
            Else
                newDP.DataValue = Me._CurrentCategory.Products(intProducts).UnitSales(Me._GraphMonth) * Me._CurrentCategory.Products(intProducts).RetailPrice
            End If
            newDP.DataKey = Me._CurrentCategory.Products(intProducts).Name
            newDP.Tag = Me._CurrentCategory.Products(intProducts)
            lstDP.Add(newDP)
        Next
        Return lstDP

    End Function

    ''' <summary> Sets up the labels. Dynamically assign month names to Radio Buttons. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub SetupLabels()

        Me.Month1RadioButton.Text = "November, 2009" ' MonthName(Now.AddMonths(-3).Month) & ", " & Now.AddMonths(-3).Year
        Me.Month2RadioButton.Text = "December, 2009" 'MonthName(Now.AddMonths(-2).Month) & ", " & Now.AddMonths(-2).Year
        Me.Month3RadioButton.Text = "January, 2010" 'MonthName(Now.AddMonths(-1).Month) & ", " & Now.AddMonths(-1).Year
        Me.Month4RadioButton.Text = "February, 2010" 'MonthName(Now.Month) & ", " & Now.Year

    End Sub

    ''' <summary> Sets up the categories. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Private Sub SetUpCategories()
        Me._Categories = Category.GetAllCategories(My.Settings.SqlConnectionString) ' .ConString)
        Me._GrossSales = New List(Of Decimal)
        For intMonths As Integer = 0 To 3 'Instantiate lstGrossSales
            Me._GrossSales.Add(0)
        Next
        For intCategories As Integer = 0 To Me._Categories.Count - 1
            For intmonths As Integer = 0 To 3
                Me._GrossSales(intmonths) += Me._Categories(intCategories).TotalSales(intmonths)
            Next
        Next
    End Sub

#End Region

#Region " FORM CONTROL EVENTS "

    ''' <summary> Sales get tool tip text. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Tool tip event information. </param>
    Private Sub ChSales_GetToolTipText(ByVal sender As Object, ByVal e As System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs) Handles SalesChart.GetToolTipText

        Select Case e.HitTestResult.ChartElementType
            Case ChartElementType.DataPoint
                If e.HitTestResult.PointIndex >= 0 Then
                    Dim strKey As String = CStr(Me._DataPoints(e.HitTestResult.PointIndex).DataKey)
                    Dim strValue As String = Format(Me._DataPoints(e.HitTestResult.PointIndex).DataValue, "#,##0.00")
                    e.Text = strKey & ", " & strValue
                End If
        End Select

    End Sub

    ''' <summary> Sales mouse down. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub ChSales_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SalesChart.MouseDown

        Dim htrResult As HitTestResult = Me.SalesChart.HitTest(e.X, e.Y)
        If htrResult.ChartElementType = ChartElementType.DataPoint And htrResult.PointIndex >= 0 Then
            Me.SetGraphMonth()
            If Me._CurrentStage = GraphStage.Pyramid Then
                Me._CurrentStage = GraphStage.Doughnut
                Me._CurrentCategory = Me._Categories(htrResult.PointIndex)
                Me.GroupPercentageTrackBar.Value = 3
                Me.UnitSalesRadioButton.Visible = True
                Me.SetUpGraph()
            Else
                Dim newProductGraph As New ProductSalesForm(CType(Me._DataPoints(htrResult.PointIndex).Tag, Product))
                newProductGraph.Show()
            End If
        End If

    End Sub

    ''' <summary> Sales mouse move. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Mouse event information. </param>
    Private Sub ChSales_MouseMove(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles SalesChart.MouseMove

        Dim htrResult As HitTestResult = Me.SalesChart.HitTest(e.X, e.Y)
        'Go through points setting design elements back to default
        For Each dp As DataPoint In Me.SalesChart.Series(0).Points
            dp.BackSecondaryColor = Color.White
            dp.BackHatchStyle = ChartHatchStyle.None
            dp.BorderWidth = 0
        Next dp
        'If users mouse hovers over a datapoint or it's equivalent Legend Item then set cursor to hand to indicate that it is a link
        'Also we use some design elements to indicate which DataPoint is active
        If htrResult.PointIndex >= 0 Then
            If htrResult.ChartElementType = ChartElementType.DataPoint Or htrResult.ChartElementType = ChartElementType.LegendItem Then
                Me.Cursor = Cursors.Hand
                Dim dpSelected As DataPoint
                dpSelected = Me.SalesChart.Series(0).Points(htrResult.PointIndex)
                dpSelected.BackSecondaryColor = Color.Black
                dpSelected.BorderColor = Color.White
                dpSelected.BorderWidth = 1
            End If
        Else
            'Set cursor back to default when leaving selected datapoint
            Me.Cursor = Cursors.Default
        End If

    End Sub

    ''' <summary> Form chart example shown. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FrmChartExample_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            Me.SetupLabels()
            Me.SetUpCategories()
            Me.Month4RadioButton.Checked = True
            Me.PercentRadioButton.Checked = True
            Me._CurrentStage = GraphStage.Pyramid
            Me.SetUpGraph()
        Catch ex As Exception
            ex.Data.Add("@isr", "Exception loading the form.")
            ' My.Application.Logger.WriteExceptionDetails(ex, My.MyApplication.TraceEventId)
            ' If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex, isr.Core.MyMessageBoxIcon.Error) Then
            If MessageBox.Show($"Exception loading form:
{ex}", "Exception loading form", MessageBoxButtons.AbortRetryIgnore) = DialogResult.Abort Then
                Application.Exit()
            End If
        End Try

    End Sub

    ''' <summary> Button return click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReturnButton.Click

        Me._CurrentStage = GraphStage.Pyramid
        Me.UnitSalesRadioButton.Visible = False
        Me.SetUpGraph()
        Me.ReturnButton.Visible = False
        Me.SetPercentageChoice(False)

    End Sub

    ' Private Sub ChSales_PostPaint(ByVal sender As Object, ByVal e As System.Windows.Forms.DataVisualization.Charting.ChartPaintEventArgs) Handles chSales.PostPaint

    '    ' Painting series object
    '    If TypeOf sender Is System.Windows.Forms.DataVisualization.Charting.Chart Then
    '        Dim chart As System.Windows.Forms.DataVisualization.Charting.Chart = CType(sender, Chart)
    '        Dim series As Series = chart.Series(0)
    '        ' Find data point with maximum Y value
    '        For Each dp As DataPoint In series.Points
    '            Dim dataPoint As DataPoint = dp

    '            ' Load bitmap from file 
    '            Dim bitmap As System.Drawing.Image = Image.FromFile("D:\Documents\Visual Studio 2008\Projects\Code Project Articles\CodeProjectGraphArticle\CodeProjectGraphArticle\" & dp.AxisLabel.ToString & ".png")

    '            ' Set Red color as transparent
    '            Dim attrib As New ImageAttributes()
    '            attrib.SetColorKey(Color.Red, Color.Red, ColorAdjustType.Default)
    '            ' Calculates marker position depending on the data point X and Y values
    '            Dim imagePosition As RectangleF = RectangleF.Empty
    '            imagePosition.X = CSng(e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.X, dataPoint.XValue))
    '            imagePosition.Y = CSng(e.ChartGraphics.GetPositionFromAxis("ChartArea1", AxisName.Y, dataPoint.YValues(0)))
    '            imagePosition = e.ChartGraphics.GetAbsoluteRectangle(imagePosition)
    '            imagePosition.Width = bitmap.Width
    '            imagePosition.Height = bitmap.Height
    '            'imagePosition.Y -= 10
    '            ' imagePosition.X -= 1
    '            ' Draw image
    '            e.ChartGraphics.Graphics.DrawImage(bitmap, Rectangle.Round(imagePosition), 0, 0, bitmap.Width, bitmap.Height, GraphicsUnit.Pixel, attrib)
    '            ' Dispose image object
    '            bitmap.Dispose()
    '        Next dp
    '    End If
    'End Sub

    ''' <summary> Trackbar  group percentage value changed. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TbGroupPercentage_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupPercentageTrackBar.ValueChanged
        Dim strPercent As String = "Grouping Products having less than " & Me.GroupPercentageTrackBar.Value & " % of Sales"
        Me.PercentageValueLabel.Text = strPercent.ToUpper(System.Globalization.CultureInfo.CurrentCulture)
        Me.SetUpGraph()
    End Sub

    ''' <summary> Event handler. Called by RbtnGross for checked changed events. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GrossRadioButton.CheckedChanged,
                                                                                            Month1RadioButton.CheckedChanged,
                                                                                            Month2RadioButton.CheckedChanged,
                                                                                            Month3RadioButton.CheckedChanged,
                                                                                            Month4RadioButton.CheckedChanged,
                                                                                            PercentRadioButton.CheckedChanged,
                                                                                            UnitSalesRadioButton.CheckedChanged
        If CType(sender, RadioButton).Checked Then
            Me.SetUpGraph()
        End If

    End Sub

    ''' <summary> Button close click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles QuitButton.Click
        Me.Close()
    End Sub

    ''' <summary> Button save click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveButton.Click

        Try
            If SaveGraph(Me.SalesChart) Then
                MsgBox("Graph successfully saved.")
            End If
        Catch ex As System.IO.IOException
            MsgBox("There was an error saving the Graph. Please try again. If the problem persists, please call your System Administrator.")
        End Try

    End Sub

    ''' <summary> Button copy click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnCopy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CopyButton.Click

        CopyToClipboard(Me.SalesChart)

    End Sub

    ''' <summary> Button print preview click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnPrintPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PreviewButton.Click

        PreviewGraph(Me.SalesChart)

    End Sub

    ''' <summary> Button print click. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub BtnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrintButton.Click

        Try
            PrintGraph(Me.SalesChart)
        Catch ex As System.Drawing.Printing.InvalidPrinterException
            MsgBox("There was an error printing the Report. Please try again. If the problem persists, please call your System Administrator.")
        End Try

    End Sub

#End Region

End Class
