<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChartExampleForm
    Inherits Form
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim Series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim DataPoint1 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(0R, 45.0R)
        Dim DataPoint2 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(1.0R, 20.0R)
        Dim DataPoint3 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(2.0R, 20.0R)
        Dim DataPoint4 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(3.0R, 10.0R)
        Dim DataPoint5 As System.Windows.Forms.DataVisualization.Charting.DataPoint = New System.Windows.Forms.DataVisualization.Charting.DataPoint(4.0R, 5.0R)
        Dim Title1 As System.Windows.Forms.DataVisualization.Charting.Title = New System.Windows.Forms.DataVisualization.Charting.Title()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ChartExampleForm))
        Me.SalesChart = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.ChartSalesGroupBox = New System.Windows.Forms.GroupBox()
        Me.UnitSalesRadioButton = New System.Windows.Forms.RadioButton()
        Me.GrossRadioButton = New System.Windows.Forms.RadioButton()
        Me.PercentRadioButton = New System.Windows.Forms.RadioButton()
        Me.MonthsGroupBox = New System.Windows.Forms.GroupBox()
        Me.Month4RadioButton = New System.Windows.Forms.RadioButton()
        Me.Month3RadioButton = New System.Windows.Forms.RadioButton()
        Me.Month2RadioButton = New System.Windows.Forms.RadioButton()
        Me.Month1RadioButton = New System.Windows.Forms.RadioButton()
        Me.DetailsGrid = New System.Windows.Forms.DataGridView()
        Me.CategoryColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SalesColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PercentageValueLabel = New System.Windows.Forms.Label()
        Me.Limen20Label = New System.Windows.Forms.Label()
        Me.Limen10Label = New System.Windows.Forms.Label()
        Me.GroupPercentageTrackBar = New System.Windows.Forms.TrackBar()
        Me.GroupPercentagePanel = New System.Windows.Forms.Panel()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.CopyButton = New System.Windows.Forms.Button()
        Me.SaveButton = New System.Windows.Forms.Button()
        Me.PreviewButton = New System.Windows.Forms.Button()
        Me.PrintButton = New System.Windows.Forms.Button()
        Me.QuitButton = New System.Windows.Forms.Button()
        Me.ReturnButton = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        CType(Me.SalesChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ChartSalesGroupBox.SuspendLayout()
        Me.MonthsGroupBox.SuspendLayout()
        CType(Me.DetailsGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupPercentageTrackBar, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupPercentagePanel.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'MscSales
        '
        Me.SalesChart.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SalesChart.BackColor = System.Drawing.Color.Gray
        Me.SalesChart.BackSecondaryColor = System.Drawing.Color.Silver
        Me.SalesChart.BorderlineColor = System.Drawing.Color.Transparent
        Me.SalesChart.BorderlineWidth = 3
        Me.SalesChart.BorderSkin.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.SalesChart.BorderSkin.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter
        Me.SalesChart.BorderSkin.BackSecondaryColor = System.Drawing.Color.White
        Me.SalesChart.BorderSkin.BorderColor = System.Drawing.Color.Transparent
        Me.SalesChart.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.SalesChart.BorderSkin.BorderWidth = 2
        Me.SalesChart.BorderSkin.PageColor = System.Drawing.Color.Transparent
        Me.SalesChart.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.FrameTitle1
        ChartArea1.Area3DStyle.Enable3D = True
        ChartArea1.Area3DStyle.Inclination = 45
        ChartArea1.Area3DStyle.IsClustered = True
        ChartArea1.Area3DStyle.IsRightAngleAxes = False
        ChartArea1.Area3DStyle.LightStyle = System.Windows.Forms.DataVisualization.Charting.LightStyle.Realistic
        ChartArea1.Area3DStyle.Perspective = 45
        ChartArea1.Area3DStyle.PointDepth = 60
        ChartArea1.Area3DStyle.PointGapDepth = 60
        ChartArea1.Area3DStyle.Rotation = 180
        ChartArea1.Area3DStyle.WallWidth = 30
        ChartArea1.BackColor = System.Drawing.Color.Transparent
        ChartArea1.BorderColor = System.Drawing.Color.Transparent
        ChartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        ChartArea1.BorderWidth = 0
        ChartArea1.Name = "ChartArea1"
        Me.SalesChart.ChartAreas.Add(ChartArea1)
        Legend1.BackColor = System.Drawing.Color.Transparent
        Legend1.ForeColor = System.Drawing.Color.White
        Legend1.IsEquallySpacedItems = True
        Legend1.Name = "Legend1"
        Legend1.TitleAlignment = System.Drawing.StringAlignment.Far
        Legend1.TitleForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Legend1.TitleSeparatorColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.SalesChart.Legends.Add(Legend1)
        Me.SalesChart.Location = New System.Drawing.Point(256, 0)
        Me.SalesChart.Name = "MscSales"
        Me.SalesChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me.SalesChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer)), System.Drawing.Color.Navy, System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer)), System.Drawing.Color.Blue, System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer)), System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))}
        Series1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.HorizontalCenter
        Series1.BackSecondaryColor = System.Drawing.Color.White
        Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Series1.BorderWidth = 0
        Series1.ChartArea = "ChartArea1"
        Series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pyramid
        Series1.Color = System.Drawing.Color.White
        Series1.CustomProperties = "CalloutLineColor=White, Pyramid3DDrawingStyle=CircularBase, PieDrawingStyle=Conca" &
    "ve, PyramidPointGap=1.5, PieStartAngle=90"
        Series1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Series1.IsValueShownAsLabel = True
        Series1.LabelBackColor = System.Drawing.Color.Transparent
        Series1.LabelBorderColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Series1.LabelBorderWidth = 0
        Series1.LabelForeColor = System.Drawing.Color.White
        Series1.Legend = "Legend1"
        Series1.MarkerBorderColor = System.Drawing.Color.Transparent
        Series1.Name = "GDS"
        DataPoint1.AxisLabel = "Filters"
        DataPoint1.LabelBackColor = System.Drawing.Color.Transparent
        DataPoint1.LabelBorderColor = System.Drawing.Color.White
        DataPoint2.AxisLabel = "Global Lubricants"
        DataPoint2.LabelBackColor = System.Drawing.Color.Transparent
        DataPoint2.LabelBorderColor = System.Drawing.Color.White
        DataPoint3.AxisLabel = "Castrol Lubricants"
        DataPoint3.LabelBackColor = System.Drawing.Color.Transparent
        DataPoint3.LabelBorderColor = System.Drawing.Color.White
        DataPoint4.AxisLabel = "Three Way Distributors "
        DataPoint4.LabelBackColor = System.Drawing.Color.Transparent
        DataPoint4.LabelBorderColor = System.Drawing.Color.White
        DataPoint5.AxisLabel = "Additives"
        DataPoint5.LabelBackColor = System.Drawing.Color.Transparent
        DataPoint5.LabelBorderColor = System.Drawing.Color.White
        Series1.Points.Add(DataPoint1)
        Series1.Points.Add(DataPoint2)
        Series1.Points.Add(DataPoint3)
        Series1.Points.Add(DataPoint4)
        Series1.Points.Add(DataPoint5)
        Series1.SmartLabelStyle.AllowOutsidePlotArea = System.Windows.Forms.DataVisualization.Charting.LabelOutsidePlotAreaStyle.Yes
        Me.SalesChart.Series.Add(Series1)
        Me.SalesChart.Size = New System.Drawing.Size(513, 544)
        Me.SalesChart.TabIndex = 17
        Me.SalesChart.Text = "Sales"
        Title1.Alignment = System.Drawing.ContentAlignment.TopCenter
        Title1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.World, CType(0, Byte), True)
        Title1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Title1.Name = "Title1"
        Title1.ShadowOffset = 2
        Title1.Text = "SALES BY CATEGORY - PERCENT OF GROSS SALES"
        Title1.TextStyle = System.Windows.Forms.DataVisualization.Charting.TextStyle.Embed
        Me.SalesChart.Titles.Add(Title1)
        '
        'ChartSalesGroupBox
        '
        Me.ChartSalesGroupBox.Controls.Add(Me.UnitSalesRadioButton)
        Me.ChartSalesGroupBox.Controls.Add(Me.GrossRadioButton)
        Me.ChartSalesGroupBox.Controls.Add(Me.PercentRadioButton)
        Me.ChartSalesGroupBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ChartSalesGroupBox.Location = New System.Drawing.Point(12, 151)
        Me.ChartSalesGroupBox.Name = "ChartSalesGroupBox"
        Me.ChartSalesGroupBox.Size = New System.Drawing.Size(227, 97)
        Me.ChartSalesGroupBox.TabIndex = 16
        Me.ChartSalesGroupBox.TabStop = False
        Me.ChartSalesGroupBox.Text = "Chart Sales as :"
        '
        'UnitSalesRadioButton
        '
        Me.UnitSalesRadioButton.AutoSize = True
        Me.UnitSalesRadioButton.Location = New System.Drawing.Point(14, 65)
        Me.UnitSalesRadioButton.Name = "UnitSalesRadioButton"
        Me.UnitSalesRadioButton.Size = New System.Drawing.Size(73, 17)
        Me.UnitSalesRadioButton.TabIndex = 2
        Me.UnitSalesRadioButton.Text = "Unit Sales"
        Me.UnitSalesRadioButton.UseVisualStyleBackColor = True
        Me.UnitSalesRadioButton.Visible = False
        '
        'GrossRadioButton
        '
        Me.GrossRadioButton.AutoSize = True
        Me.GrossRadioButton.Location = New System.Drawing.Point(14, 42)
        Me.GrossRadioButton.Name = "GrossRadioButton"
        Me.GrossRadioButton.Size = New System.Drawing.Size(82, 17)
        Me.GrossRadioButton.TabIndex = 1
        Me.GrossRadioButton.Text = "Retail Value"
        Me.GrossRadioButton.UseVisualStyleBackColor = True
        '
        'PercentRadioButton
        '
        Me.PercentRadioButton.AutoSize = True
        Me.PercentRadioButton.Location = New System.Drawing.Point(14, 19)
        Me.PercentRadioButton.Name = "PercentRadioButton"
        Me.PercentRadioButton.Size = New System.Drawing.Size(133, 17)
        Me.PercentRadioButton.TabIndex = 0
        Me.PercentRadioButton.Text = "Percent of Gross Sales"
        Me.PercentRadioButton.UseVisualStyleBackColor = True
        '
        'MonthsGroupBox
        '
        Me.MonthsGroupBox.Controls.Add(Me.Month4RadioButton)
        Me.MonthsGroupBox.Controls.Add(Me.Month3RadioButton)
        Me.MonthsGroupBox.Controls.Add(Me.Month2RadioButton)
        Me.MonthsGroupBox.Controls.Add(Me.Month1RadioButton)
        Me.MonthsGroupBox.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.MonthsGroupBox.Location = New System.Drawing.Point(12, 12)
        Me.MonthsGroupBox.Name = "MonthsGroupBox"
        Me.MonthsGroupBox.Size = New System.Drawing.Size(227, 133)
        Me.MonthsGroupBox.TabIndex = 15
        Me.MonthsGroupBox.TabStop = False
        Me.MonthsGroupBox.Text = "Select Month"
        '
        'Month4RadioButton
        '
        Me.Month4RadioButton.AutoSize = True
        Me.Month4RadioButton.Location = New System.Drawing.Point(14, 98)
        Me.Month4RadioButton.Name = "Month4RadioButton"
        Me.Month4RadioButton.Size = New System.Drawing.Size(92, 17)
        Me.Month4RadioButton.TabIndex = 3
        Me.Month4RadioButton.Tag = "3"
        Me.Month4RadioButton.Text = "Current Month"
        Me.Month4RadioButton.UseVisualStyleBackColor = True
        '
        'Month3RadioButton
        '
        Me.Month3RadioButton.AutoSize = True
        Me.Month3RadioButton.Location = New System.Drawing.Point(14, 75)
        Me.Month3RadioButton.Name = "Month3RadioButton"
        Me.Month3RadioButton.Size = New System.Drawing.Size(61, 17)
        Me.Month3RadioButton.TabIndex = 2
        Me.Month3RadioButton.Tag = "2"
        Me.Month3RadioButton.Text = "Month3"
        Me.Month3RadioButton.UseVisualStyleBackColor = True
        '
        'Month2RadioButton
        '
        Me.Month2RadioButton.AutoSize = True
        Me.Month2RadioButton.Location = New System.Drawing.Point(14, 52)
        Me.Month2RadioButton.Name = "Month2RadioButton"
        Me.Month2RadioButton.Size = New System.Drawing.Size(61, 17)
        Me.Month2RadioButton.TabIndex = 1
        Me.Month2RadioButton.Tag = "1"
        Me.Month2RadioButton.Text = "Month2"
        Me.Month2RadioButton.UseVisualStyleBackColor = True
        '
        'Month1RadioButton
        '
        Me.Month1RadioButton.AutoSize = True
        Me.Month1RadioButton.Location = New System.Drawing.Point(14, 29)
        Me.Month1RadioButton.Name = "RbtnMonth1"
        Me.Month1RadioButton.Size = New System.Drawing.Size(61, 17)
        Me.Month1RadioButton.TabIndex = 0
        Me.Month1RadioButton.Tag = "0"
        Me.Month1RadioButton.Text = "Month1"
        Me.Month1RadioButton.UseVisualStyleBackColor = True
        '
        'DetailsGrid
        '
        Me.DetailsGrid.BackgroundColor = System.Drawing.Color.White
        Me.DetailsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DetailsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DetailsGrid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DetailsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DetailsGrid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CategoryColumn, Me.SalesColumn})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DetailsGrid.DefaultCellStyle = DataGridViewCellStyle3
        Me.DetailsGrid.EnableHeadersVisualStyles = False
        Me.DetailsGrid.GridColor = System.Drawing.Color.White
        Me.DetailsGrid.Location = New System.Drawing.Point(12, 254)
        Me.DetailsGrid.Name = "DetailsGrid"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DetailsGrid.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DetailsGrid.RowHeadersVisible = False
        Me.DetailsGrid.Size = New System.Drawing.Size(227, 278)
        Me.DetailsGrid.TabIndex = 23
        '
        'CategoryColumn
        '
        Me.CategoryColumn.HeaderText = "Category"
        Me.CategoryColumn.Name = "CategoryColumn"
        Me.CategoryColumn.Width = 135
        '
        'SalesColumn
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.SalesColumn.DefaultCellStyle = DataGridViewCellStyle2
        Me.SalesColumn.HeaderText = "Sales"
        Me.SalesColumn.Name = "SalesColumn"
        Me.SalesColumn.Width = 70
        '
        'PercentageValueLabel
        '
        Me.PercentageValueLabel.AutoSize = True
        Me.PercentageValueLabel.BackColor = System.Drawing.Color.Gray
        Me.PercentageValueLabel.ForeColor = System.Drawing.Color.White
        Me.PercentageValueLabel.Location = New System.Drawing.Point(-1, 53)
        Me.PercentageValueLabel.Name = "LblPercentageValue"
        Me.PercentageValueLabel.Size = New System.Drawing.Size(312, 13)
        Me.PercentageValueLabel.TabIndex = 33
        Me.PercentageValueLabel.Text = "GROUPING PRODUCTS  HAVING LESS THAN 3 % OF SALES"
        '
        'Limen20Label
        '
        Me.Limen20Label.AutoSize = True
        Me.Limen20Label.BackColor = System.Drawing.Color.Gray
        Me.Limen20Label.ForeColor = System.Drawing.Color.White
        Me.Limen20Label.Location = New System.Drawing.Point(272, 1)
        Me.Limen20Label.Name = "Limen20Label"
        Me.Limen20Label.Size = New System.Drawing.Size(19, 13)
        Me.Limen20Label.TabIndex = 32
        Me.Limen20Label.Text = "20"
        '
        'Limen10Label
        '
        Me.Limen10Label.AutoSize = True
        Me.Limen10Label.BackColor = System.Drawing.Color.Gray
        Me.Limen10Label.ForeColor = System.Drawing.Color.White
        Me.Limen10Label.Location = New System.Drawing.Point(22, 1)
        Me.Limen10Label.Name = "Limen10Label"
        Me.Limen10Label.Size = New System.Drawing.Size(13, 13)
        Me.Limen10Label.TabIndex = 31
        Me.Limen10Label.Text = "0"
        '
        'GroupPercentageTrackBar
        '
        Me.GroupPercentageTrackBar.AutoSize = False
        Me.GroupPercentageTrackBar.BackColor = System.Drawing.Color.Gray
        Me.GroupPercentageTrackBar.Location = New System.Drawing.Point(14, 17)
        Me.GroupPercentageTrackBar.Maximum = 20
        Me.GroupPercentageTrackBar.Name = "GroupPercentageTrackBar"
        Me.GroupPercentageTrackBar.Size = New System.Drawing.Size(281, 30)
        Me.GroupPercentageTrackBar.TabIndex = 0
        Me.GroupPercentageTrackBar.TickStyle = System.Windows.Forms.TickStyle.TopLeft
        '
        'GroupPercentagePanel
        '
        Me.GroupPercentagePanel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.GroupPercentagePanel.BackColor = System.Drawing.Color.Gray
        Me.GroupPercentagePanel.Controls.Add(Me.GroupPercentageTrackBar)
        Me.GroupPercentagePanel.Controls.Add(Me.PercentageValueLabel)
        Me.GroupPercentagePanel.Controls.Add(Me.Limen10Label)
        Me.GroupPercentagePanel.Controls.Add(Me.Limen20Label)
        Me.GroupPercentagePanel.Location = New System.Drawing.Point(286, 448)
        Me.GroupPercentagePanel.Name = "GroupPercentagePanel"
        Me.GroupPercentagePanel.Size = New System.Drawing.Size(314, 67)
        Me.GroupPercentagePanel.TabIndex = 34
        '
        'CopyButton
        '
        Me.CopyButton.BackgroundImage = CType(resources.GetObject("BtnCopy.BackgroundImage"), System.Drawing.Image)
        Me.CopyButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.CopyButton.Dock = System.Windows.Forms.DockStyle.Top
        Me.CopyButton.Location = New System.Drawing.Point(3, 16)
        Me.CopyButton.Name = "CopyButton"
        Me.CopyButton.Size = New System.Drawing.Size(42, 42)
        Me.CopyButton.TabIndex = 0
        Me._ToolTip.SetToolTip(Me.CopyButton, "Copy")
        Me.CopyButton.UseVisualStyleBackColor = True
        '
        'SaveButton
        '
        Me.SaveButton.BackgroundImage = CType(resources.GetObject("BtnSave.BackgroundImage"), System.Drawing.Image)
        Me.SaveButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.SaveButton.Dock = System.Windows.Forms.DockStyle.Top
        Me.SaveButton.Location = New System.Drawing.Point(3, 58)
        Me.SaveButton.Name = "SaveButton"
        Me.SaveButton.Size = New System.Drawing.Size(42, 42)
        Me.SaveButton.TabIndex = 1
        Me._ToolTip.SetToolTip(Me.SaveButton, "Save Graph as Image")
        Me.SaveButton.UseVisualStyleBackColor = True
        '
        'PreviewButton
        '
        Me.PreviewButton.BackgroundImage = CType(resources.GetObject("BtnPreview.BackgroundImage"), System.Drawing.Image)
        Me.PreviewButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PreviewButton.Dock = System.Windows.Forms.DockStyle.Top
        Me.PreviewButton.Location = New System.Drawing.Point(3, 100)
        Me.PreviewButton.Name = "PreviewButton"
        Me.PreviewButton.Size = New System.Drawing.Size(42, 42)
        Me.PreviewButton.TabIndex = 2
        Me._ToolTip.SetToolTip(Me.PreviewButton, "Print Preview")
        Me.PreviewButton.UseVisualStyleBackColor = True
        '
        'PrintButton
        '
        Me.PrintButton.BackgroundImage = CType(resources.GetObject("BtnPrint.BackgroundImage"), System.Drawing.Image)
        Me.PrintButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.PrintButton.Dock = System.Windows.Forms.DockStyle.Top
        Me.PrintButton.Location = New System.Drawing.Point(3, 142)
        Me.PrintButton.Name = "PrintButton"
        Me.PrintButton.Size = New System.Drawing.Size(42, 42)
        Me.PrintButton.TabIndex = 3
        Me._ToolTip.SetToolTip(Me.PrintButton, "Print")
        Me.PrintButton.UseVisualStyleBackColor = True
        '
        'QuitButton
        '
        Me.QuitButton.BackgroundImage = CType(resources.GetObject("BtnQuit.BackgroundImage"), System.Drawing.Image)
        Me.QuitButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.QuitButton.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.QuitButton.Location = New System.Drawing.Point(3, 499)
        Me.QuitButton.Name = "QuitButton"
        Me.QuitButton.Size = New System.Drawing.Size(42, 42)
        Me.QuitButton.TabIndex = 4
        Me._ToolTip.SetToolTip(Me.QuitButton, "Quit Application")
        Me.QuitButton.UseVisualStyleBackColor = True
        '
        'ReturnButton
        '
        Me.ReturnButton.BackgroundImage = CType(resources.GetObject("BtnReturn.BackgroundImage"), System.Drawing.Image)
        Me.ReturnButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.ReturnButton.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.ReturnButton.Location = New System.Drawing.Point(3, 457)
        Me.ReturnButton.Name = "ReturnButton"
        Me.ReturnButton.Size = New System.Drawing.Size(42, 42)
        Me.ReturnButton.TabIndex = 5
        Me._ToolTip.SetToolTip(Me.ReturnButton, "Return to Category View")
        Me.ReturnButton.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.ReturnButton)
        Me.GroupBox3.Controls.Add(Me.QuitButton)
        Me.GroupBox3.Controls.Add(Me.PrintButton)
        Me.GroupBox3.Controls.Add(Me.PreviewButton)
        Me.GroupBox3.Controls.Add(Me.SaveButton)
        Me.GroupBox3.Controls.Add(Me.CopyButton)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(769, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(48, 544)
        Me.GroupBox3.TabIndex = 35
        Me.GroupBox3.TabStop = False
        '
        'ChartExampleForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(817, 544)
        Me.Controls.Add(Me.GroupPercentagePanel)
        Me.Controls.Add(Me.SalesChart)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.DetailsGrid)
        Me.Controls.Add(Me.ChartSalesGroupBox)
        Me.Controls.Add(Me.MonthsGroupBox)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ChartExampleForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MS Chart Example"
        CType(Me.SalesChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ChartSalesGroupBox.ResumeLayout(False)
        Me.ChartSalesGroupBox.PerformLayout()
        Me.MonthsGroupBox.ResumeLayout(False)
        Me.MonthsGroupBox.PerformLayout()
        CType(Me.DetailsGrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupPercentageTrackBar, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupPercentagePanel.ResumeLayout(False)
        Me.GroupPercentagePanel.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents SalesChart As System.Windows.Forms.DataVisualization.Charting.Chart
    Private WithEvents ChartSalesGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents GrossRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents PercentRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents MonthsGroupBox As System.Windows.Forms.GroupBox
    Private WithEvents Month4RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents Month3RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents Month2RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents Month1RadioButton As System.Windows.Forms.RadioButton
    Private WithEvents DetailsGrid As System.Windows.Forms.DataGridView
    Private WithEvents UnitSalesRadioButton As System.Windows.Forms.RadioButton
    Private WithEvents PercentageValueLabel As System.Windows.Forms.Label
    Private WithEvents Limen20Label As System.Windows.Forms.Label
    Private WithEvents Limen10Label As System.Windows.Forms.Label
    Private WithEvents GroupPercentageTrackBar As System.Windows.Forms.TrackBar
    Private WithEvents GroupPercentagePanel As System.Windows.Forms.Panel
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip
    Private WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Private WithEvents CopyButton As System.Windows.Forms.Button
    Private WithEvents ReturnButton As System.Windows.Forms.Button
    Private WithEvents QuitButton As System.Windows.Forms.Button
    Private WithEvents PrintButton As System.Windows.Forms.Button
    Private WithEvents PreviewButton As System.Windows.Forms.Button
    Private WithEvents SaveButton As System.Windows.Forms.Button
    Private WithEvents CategoryColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Private WithEvents SalesColumn As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
