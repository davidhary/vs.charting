<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ProductSalesForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim chartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim series1 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ProductSalesForm))
        Dim chartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim series2 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Dim series3 As System.Windows.Forms.DataVisualization.Charting.Series = New System.Windows.Forms.DataVisualization.Charting.Series()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblAveSales = New System.Windows.Forms.Label()
        Me.lblRetailValue = New System.Windows.Forms.Label()
        Me.lblUnitSales = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblPercentageValue = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.mscUnitSales = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnReset = New System.Windows.Forms.Button()
        Me.btnQuit = New System.Windows.Forms.Button()
        Me.btnPrint = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnCopy = New System.Windows.Forms.Button()
        Me.mscDailySales = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.mscUnitSales, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mscDailySales, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblAveSales)
        Me.GroupBox2.Controls.Add(Me.lblRetailValue)
        Me.GroupBox2.Controls.Add(Me.lblUnitSales)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.lblPercentageValue)
        Me.GroupBox2.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(164, 100)
        Me.GroupBox2.TabIndex = 20
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Product Details :"
        '
        'lblAveSales
        '
        Me.lblAveSales.Location = New System.Drawing.Point(100, 73)
        Me.lblAveSales.Name = "lblAveSales"
        Me.lblAveSales.Size = New System.Drawing.Size(48, 13)
        Me.lblAveSales.TabIndex = 9
        Me.lblAveSales.Text = "1000"
        Me.lblAveSales.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRetailValue
        '
        Me.lblRetailValue.Location = New System.Drawing.Point(100, 51)
        Me.lblRetailValue.Name = "lblRetailValue"
        Me.lblRetailValue.Size = New System.Drawing.Size(48, 13)
        Me.lblRetailValue.TabIndex = 8
        Me.lblRetailValue.Text = "1000"
        Me.lblRetailValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblUnitSales
        '
        Me.lblUnitSales.Location = New System.Drawing.Point(100, 27)
        Me.lblUnitSales.Name = "lblUnitSales"
        Me.lblUnitSales.Size = New System.Drawing.Size(48, 13)
        Me.lblUnitSales.TabIndex = 7
        Me.lblUnitSales.Text = "1000"
        Me.lblUnitSales.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 73)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(87, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Ave Daily Sales :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Retail Value :"
        '
        'lblPercentageValue
        '
        Me.lblPercentageValue.AutoSize = True
        Me.lblPercentageValue.Location = New System.Drawing.Point(6, 27)
        Me.lblPercentageValue.Name = "lblPercentageValue"
        Me.lblPercentageValue.Size = New System.Drawing.Size(88, 13)
        Me.lblPercentageValue.TabIndex = 4
        Me.lblPercentageValue.Text = "Total Unit Sales :"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.mscUnitSales)
        Me.GroupBox1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 118)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(164, 369)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Total Unit Sales :"
        '
        'mscUnitSales
        '
        Me.mscUnitSales.BackColor = System.Drawing.Color.WhiteSmoke
        Me.mscUnitSales.BorderlineColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.mscUnitSales.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.mscUnitSales.BorderlineWidth = 2
        Me.mscUnitSales.BorderSkin.BackColor = System.Drawing.Color.Black
        Me.mscUnitSales.BorderSkin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.mscUnitSales.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.mscUnitSales.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Emboss
        ChartArea1.Area3DStyle.IsClustered = True
        ChartArea1.Area3DStyle.PointDepth = 25
        ChartArea1.Area3DStyle.PointGapDepth = 25
        ChartArea1.Area3DStyle.WallWidth = 10
        ChartArea1.AxisX.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        ChartArea1.AxisX.LineColor = System.Drawing.Color.Yellow
        ChartArea1.AxisX.TitleForeColor = System.Drawing.Color.White
        ChartArea1.AxisY.IsLabelAutoFit = False
        ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ChartArea1.AxisY.LabelStyle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        ChartArea1.AxisY.LineColor = System.Drawing.Color.Yellow
        ChartArea1.AxisY.TitleAlignment = System.Drawing.StringAlignment.Near
        ChartArea1.AxisY.TitleForeColor = System.Drawing.Color.Yellow
        ChartArea1.BackColor = System.Drawing.Color.Yellow
        ChartArea1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter
        ChartArea1.BackSecondaryColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        ChartArea1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        ChartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        ChartArea1.BorderWidth = 2
        ChartArea1.Name = "ChartArea1"
        ChartArea1.Position.Auto = False
        ChartArea1.Position.Height = 88.62751!
        ChartArea1.Position.Width = 82.05733!
        ChartArea1.Position.X = 1.0!
        ChartArea1.Position.Y = 8.0!
        ChartArea1.ShadowOffset = 3
        Me.mscUnitSales.ChartAreas.Add(ChartArea1)
        Me.mscUnitSales.Dock = System.Windows.Forms.DockStyle.Fill
        Me.mscUnitSales.Location = New System.Drawing.Point(3, 16)
        Me.mscUnitSales.Margin = New System.Windows.Forms.Padding(0)
        Me.mscUnitSales.Name = "mscUnitSales"
        Me.mscUnitSales.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Series1.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter
        Series1.BackSecondaryColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer))
        Series1.ChartArea = "ChartArea1"
        Series1.Color = System.Drawing.Color.Yellow
        Series1.IsValueShownAsLabel = True
        Series1.IsXValueIndexed = True
        Series1.LabelForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Series1.Legend = "Legend1"
        Series1.Name = "Series1"
        Series1.ShadowOffset = 5
        Me.mscUnitSales.Series.Add(Series1)
        Me.mscUnitSales.Size = New System.Drawing.Size(158, 350)
        Me.mscUnitSales.TabIndex = 1
        Me.mscUnitSales.Text = "Chart1"
        '
        'btnReset
        '
        Me.btnReset.BackgroundImage = CType(resources.GetObject("btnReset.BackgroundImage"), System.Drawing.Image)
        Me.btnReset.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnReset.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.btnReset.Location = New System.Drawing.Point(3, 403)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(42, 42)
        Me.btnReset.TabIndex = 5
        Me.ToolTip1.SetToolTip(Me.btnReset, "Reset Graph")
        Me.btnReset.UseVisualStyleBackColor = True
        Me.btnReset.Visible = False
        '
        'btnQuit
        '
        Me.btnQuit.BackgroundImage = CType(resources.GetObject("btnQuit.BackgroundImage"), System.Drawing.Image)
        Me.btnQuit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.btnQuit.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.btnQuit.Location = New System.Drawing.Point(3, 445)
        Me.btnQuit.Name = "btnQuit"
        Me.btnQuit.Size = New System.Drawing.Size(42, 42)
        Me.btnQuit.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.btnQuit, "Close Form")
        Me.btnQuit.UseVisualStyleBackColor = True
        '
        'btnPrint
        '
        Me.btnPrint.BackgroundImage = CType(resources.GetObject("btnPrint.BackgroundImage"), System.Drawing.Image)
        Me.btnPrint.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnPrint.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnPrint.Location = New System.Drawing.Point(3, 142)
        Me.btnPrint.Name = "btnPrint"
        Me.btnPrint.Size = New System.Drawing.Size(42, 42)
        Me.btnPrint.TabIndex = 3
        Me.ToolTip1.SetToolTip(Me.btnPrint, "Print")
        Me.btnPrint.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.BackgroundImage = CType(resources.GetObject("btnPreview.BackgroundImage"), System.Drawing.Image)
        Me.btnPreview.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnPreview.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnPreview.Location = New System.Drawing.Point(3, 100)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(42, 42)
        Me.btnPreview.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.btnPreview, "Print Preview")
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.BackgroundImage = CType(resources.GetObject("btnSave.BackgroundImage"), System.Drawing.Image)
        Me.btnSave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnSave.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSave.Location = New System.Drawing.Point(3, 58)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(42, 42)
        Me.btnSave.TabIndex = 1
        Me.ToolTip1.SetToolTip(Me.btnSave, "Save Graph as Image")
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnCopy
        '
        Me.btnCopy.BackgroundImage = CType(resources.GetObject("btnCopy.BackgroundImage"), System.Drawing.Image)
        Me.btnCopy.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom
        Me.btnCopy.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnCopy.Location = New System.Drawing.Point(3, 16)
        Me.btnCopy.Name = "btnCopy"
        Me.btnCopy.Size = New System.Drawing.Size(42, 42)
        Me.btnCopy.TabIndex = 0
        Me.ToolTip1.SetToolTip(Me.btnCopy, "Copy")
        Me.btnCopy.UseVisualStyleBackColor = True
        '
        'mscDailySales
        '
        Me.mscDailySales.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mscDailySales.BackColor = System.Drawing.Color.WhiteSmoke
        Me.mscDailySales.BorderlineColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.mscDailySales.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.mscDailySales.BorderlineWidth = 2
        Me.mscDailySales.BorderSkin.BackColor = System.Drawing.Color.Black
        Me.mscDailySales.BorderSkin.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.mscDailySales.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        Me.mscDailySales.BorderSkin.SkinStyle = System.Windows.Forms.DataVisualization.Charting.BorderSkinStyle.Emboss
        ChartArea2.Area3DStyle.IsClustered = True
        ChartArea2.Area3DStyle.PointDepth = 25
        ChartArea2.Area3DStyle.PointGapDepth = 25
        ChartArea2.Area3DStyle.WallWidth = 10
        ChartArea2.AxisY.ScaleView.Zoomable = False
        ChartArea2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        ChartArea2.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter
        ChartArea2.BackSecondaryColor = System.Drawing.Color.Purple
        ChartArea2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        ChartArea2.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid
        ChartArea2.BorderWidth = 2
        ChartArea2.CursorX.IsUserEnabled = True
        ChartArea2.CursorX.IsUserSelectionEnabled = True
        ChartArea2.CursorY.IsUserEnabled = True
        ChartArea2.CursorY.IsUserSelectionEnabled = True
        ChartArea2.Name = "ChartArea1"
        ChartArea2.ShadowOffset = 3
        Me.mscDailySales.ChartAreas.Add(ChartArea2)
        Me.mscDailySales.Location = New System.Drawing.Point(182, 0)
        Me.mscDailySales.Name = "mscDailySales"
        Me.mscDailySales.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Series2.BackGradientStyle = System.Windows.Forms.DataVisualization.Charting.GradientStyle.VerticalCenter
        Series2.BackSecondaryColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Series2.BorderColor = System.Drawing.Color.Purple
        Series2.ChartArea = "ChartArea1"
        Series2.Color = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Series2.IsValueShownAsLabel = True
        Series2.IsXValueIndexed = True
        Series2.Legend = "Legend1"
        Series2.Name = "Series1"
        Series2.ShadowOffset = 5
        Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Series3.BorderWidth = 3
        Series3.ChartArea = "ChartArea1"
        Series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Series3.Color = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer))
        Series3.IsXValueIndexed = True
        Series3.Name = "Series2"
        Me.mscDailySales.Series.Add(Series2)
        Me.mscDailySales.Series.Add(Series3)
        Me.mscDailySales.Size = New System.Drawing.Size(658, 490)
        Me.mscDailySales.TabIndex = 36
        Me.mscDailySales.Text = "Chart1"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnReset)
        Me.GroupBox3.Controls.Add(Me.btnQuit)
        Me.GroupBox3.Controls.Add(Me.btnPrint)
        Me.GroupBox3.Controls.Add(Me.btnPreview)
        Me.GroupBox3.Controls.Add(Me.btnSave)
        Me.GroupBox3.Controls.Add(Me.btnCopy)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupBox3.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.GroupBox3.Location = New System.Drawing.Point(840, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(48, 490)
        Me.GroupBox3.TabIndex = 37
        Me.GroupBox3.TabStop = False
        '
        'ProductSales
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(888, 490)
        Me.Controls.Add(Me.mscDailySales)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "ProductSales"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Product Sales"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.mscUnitSales, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mscDailySales, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Private WithEvents LblAveSales As System.Windows.Forms.Label
    Private WithEvents LblRetailValue As System.Windows.Forms.Label
    Private WithEvents LblUnitSales As System.Windows.Forms.Label
    Private WithEvents Label2 As System.Windows.Forms.Label
    Private WithEvents Label1 As System.Windows.Forms.Label
    Private WithEvents LblPercentageValue As System.Windows.Forms.Label
    Private WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Private WithEvents MscUnitSales As System.Windows.Forms.DataVisualization.Charting.Chart
    Private WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Private WithEvents MscDailySales As System.Windows.Forms.DataVisualization.Charting.Chart
    Private WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Private WithEvents BtnReset As System.Windows.Forms.Button
    Private WithEvents BtnQuit As System.Windows.Forms.Button
    Private WithEvents BtnPrint As System.Windows.Forms.Button
    Private WithEvents BtnPreview As System.Windows.Forms.Button
    Private WithEvents BtnSave As System.Windows.Forms.Button
    Private WithEvents BtnCopy As System.Windows.Forms.Button
End Class
