
''' <summary> Form for viewing the splash. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Class SplashForm

    Private _ChartExampleForm As ChartExampleForm

    ''' <summary> Splash timer tick. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SplashTimer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _SplashTimer.Tick
        Me._ChartExampleForm.Visible = True
        Me.Close()
    End Sub

    ''' <summary> Form load. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me._ChartExampleForm = New ChartExampleForm With {.Visible = False}
        Me._SplashTimer.Start()
    End Sub

End Class
