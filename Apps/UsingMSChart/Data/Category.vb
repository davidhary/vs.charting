
Imports System.Data.OleDb

''' <summary> Default constructor. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Friend Class Category

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="categoryID">       Identifier for the category. </param>
    ''' <param name="connectionString"> The connection string. </param>
    Public Sub New(ByVal categoryID As Integer, ByVal connectionString As String)

        Me.ID = categoryID
        Me.GetOrderCategory(connectionString)
        Me._Products = Product.GetProductsByCategory(Me.ID, connectionString)

    End Sub

#End Region

#Region " CLASS PROPERTIES "

    ''' <summary> Gets or sets the identifier. </summary>
    ''' <value> The identifier. </value>
    Public Property ID() As Integer

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property Name() As String

    ''' <summary> Gets or sets the state. </summary>
    ''' <value> The state. </value>
    Public Property State() As ApplicationState

    ''' <summary> Gets or sets the products. </summary>
    ''' <value> The products. </value>
    Public ReadOnly Property Products() As New List(Of Product)

    ''' <summary> Gets or sets the total number of sales. </summary>
    ''' <value> The total number of sales. </value>
    Public ReadOnly Property TotalSales() As New List(Of Decimal)(3)

#End Region

#Region " DATA METHODS "

    ''' <summary> Gets order category. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    <CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification:="<Pending>")>
    Private Sub GetOrderCategory(ByVal connectionString As String)

        Using conOrders As New OleDbConnection(connectionString)
            conOrders.Open()
            Using cmdDays As New OleDbCommand("SELECT c.CategoryName, c.State  FROM Categories c WHERE ID = " & Me.ID, conOrders)
                Dim dbrCategories As OleDbDataReader = cmdDays.ExecuteReader
                While dbrCategories.Read
                    Dim blnNullCheck As Boolean = False 'prepeare boolean to handle null values
                    For intFields As Integer = 0 To dbrCategories.FieldCount - 1 'Iterate through fields to check for null
                        If DBNull.Value.Equals(dbrCategories(intFields)) Then 'If db return value is null 
                            blnNullCheck = True                                            'Set boolean to true
                        End If
                    Next
                    If Not blnNullCheck Then 'If no null values returned in row then create object else goto next record
                        Me.Name = dbrCategories.GetString(0)
                        Me.State = CType(dbrCategories.GetInt32(1), ApplicationState)
                    End If
                End While
                dbrCategories.Close()
            End Using
            For intMonths As Integer = 0 To 3
                Me.TotalSales.Add(Me.GetTotalSales(CType(intMonths, SalesMonth), connectionString))
            Next
        End Using

    End Sub

    ''' <summary> Gets total sales. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="totalMonth">       The total month. </param>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> The total sales. </returns>
    <CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification:="<Pending>")>
    Private Function GetTotalSales(ByVal totalMonth As SalesMonth, ByVal connectionString As String) As Decimal

        Using conOrders As New OleDbConnection(connectionString)
            conOrders.Open()
            Dim cmdTotalSales As New OleDbCommand() With {
                .CommandText = "SELECT Sum(" & totalMonth.ToString & " * RetailPrice) FROM Products  WHERE CategoryID = " & Me.ID,
                .Connection = conOrders
            }
            Dim objResult As Object = cmdTotalSales.ExecuteScalar
            Return If(IsNumeric(objResult), CType(objResult, Decimal), 0)
        End Using

    End Function

    ''' <summary> Gets all categories. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> all categories. </returns>
    <CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification:="<Pending>")>
    Public Shared Function GetAllCategories(ByVal connectionString As String) As List(Of Category)

        Using conOrders As New OleDbConnection(connectionString)
            conOrders.Open()
            Dim lstCategories As New List(Of Category)
            Dim cmdCategories As New OleDbCommand With {.Connection = conOrders,
                .CommandText = "SELECT c.ID FROM Categories c WHERE State = " & ApplicationState.Current}
            Dim dbrCategories As OleDbDataReader = cmdCategories.ExecuteReader
            While dbrCategories.Read
                Dim newCategory As New Category(CInt(dbrCategories(0).ToString), connectionString)
                lstCategories.Add(newCategory)
            End While
            dbrCategories.Close()
            Return lstCategories
        End Using

    End Function

#End Region

End Class

''' <summary> Values that represent application states. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum ApplicationState

    ''' <summary> An enum constant representing the discontinued option. </summary>
    Discontinued

    ''' <summary> An enum constant representing the current option. </summary>
    Current
End Enum

''' <summary> Values that represent sales months. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Public Enum SalesMonth

    ''' <summary> An enum constant representing the sales month 1 option. </summary>
    SalesMonth1

    ''' <summary> An enum constant representing the sales month 2 option. </summary>
    SalesMonth2

    ''' <summary> An enum constant representing the sales month 3 option. </summary>
    SalesMonth3

    ''' <summary> An enum constant representing the sales current month option. </summary>
    SalesCurrentMonth
End Enum


