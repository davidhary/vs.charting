
Imports System.Data.OleDb

''' <summary> Default constructor. </summary>
''' <remarks> David, 2020-10-24. </remarks>
Friend Class Product

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    Public Sub New()
        For intList As Integer = 0 To 3
            Me.UnitSales.Add(0)
        Next

    End Sub

#End Region

#Region " PUBLIC PROPERTIES "

    ''' <summary> Gets or sets the identifier. </summary>
    ''' <value> The identifier. </value>
    Public Property ID() As Integer

    ''' <summary> Gets or sets the name. </summary>
    ''' <value> The name. </value>
    Public Property Name() As String

    ''' <summary> Gets or sets the identifier of the category. </summary>
    ''' <value> The identifier of the category. </value>
    Public Property CategoryID() As Integer

    ''' <summary> Gets or sets the unit sales. </summary>
    ''' <value> The unit sales. </value>
    Public Property UnitSales() As New List(Of Integer)

    ''' <summary> Gets or sets the unit sales month 1. </summary>
    ''' <value> The unit sales month 1. </value>
    Public Property UnitSalesMonth1() As Integer
        Get
            Return Me.UnitSales(0)
        End Get
        Set(ByVal value As Integer)
            Me.UnitSales(0) = value
        End Set
    End Property

    ''' <summary> Gets or sets the unit sales month 2. </summary>
    ''' <value> The unit sales month 2. </value>
    Public Property UnitSalesMonth2() As Integer
        Get
            Return Me.UnitSales(1)
        End Get
        Set(ByVal value As Integer)
            Me.UnitSales(1) = value
        End Set
    End Property

    ''' <summary> Gets or sets the unit sales month 3. </summary>
    ''' <value> The unit sales month 3. </value>
    Public Property UnitSalesMonth3() As Integer
        Get
            Return Me.UnitSales(2)
        End Get
        Set(ByVal value As Integer)
            Me.UnitSales(2) = value
        End Set
    End Property

    ''' <summary> Gets or sets the unit sales current. </summary>
    ''' <value> The unit sales current. </value>
    Public Property UnitSalesCurrent() As Integer
        Get
            Return Me.UnitSales(3)
        End Get
        Set(ByVal value As Integer)
            Me.UnitSales(3) = value
        End Set
    End Property

    ''' <summary> Gets or sets the retail price. </summary>
    ''' <value> The retail price. </value>
    Public Property RetailPrice() As Double

    ''' <summary> Gets or sets the state. </summary>
    ''' <value> The state. </value>
    Public Property State() As ApplicationState

#End Region

#Region " DATA METHODS "

    ''' <summary> Daily sales. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="startDate">        The start date. </param>
    ''' <param name="endDate">          The end date. </param>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> A DataTable. </returns>
    <CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification:="<Pending>")>
    Public Function DailySales(ByVal startDate As Date, ByVal endDate As Date, ByVal connectionString As String) As DataTable

        Using conOrders As New OleDbConnection(connectionString)
            Dim dtSales As New DataTable
            Dim dictSales As New Dictionary(Of Date, Int32)
            Dim cmdDailySales As New OleDbCommand() With {
                .CommandText = "SELECT s.InvDate as `Date`, SUM(sd.Quantity) as `Quantity` FROM SalesDetails sd RIGHT JOIN Sales s ON s.ID = sd.SaleID WHERE s.InvDate > #" & startDate & "# AND s.InvDate < #" & endDate & "# AND sd.ProductID = " & Me.ID & " GROUP BY s.InvDate",
                .Connection = conOrders
            }
            conOrders.Open()
            Dim dbrDailySales As OleDbDataReader = cmdDailySales.ExecuteReader
            While dbrDailySales.Read
                Dim dteSale As Date = dbrDailySales.GetDateTime(0)
                Dim intUnitsSold As Integer = CInt(dbrDailySales.GetDouble(1))
                dictSales.Add(dteSale, intUnitsSold)
            End While
            Dim dictFinal As New Dictionary(Of Date, Int32)
            For i As Integer = 0 To CInt(DateDiff(DateInterval.Day, startDate, endDate))
                Dim dteSales As Date = startDate.AddDays(i)
                If dteSales.DayOfWeek <> DayOfWeek.Saturday And dteSales.DayOfWeek <> DayOfWeek.Sunday And IsHoliday(dteSales) = False Then
                    dictFinal.Add(startDate.AddDays(i), 0)
                    If dictSales.ContainsKey(startDate.AddDays(i)) Then
                        dictFinal(startDate.AddDays(i)) = dictSales(startDate.AddDays(i))
                    End If
                End If
            Next
            Dim column As DataColumn
            ' Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = New DataColumn() With {
                .DataType = System.Type.GetType("System.DateTime"),
                .ColumnName = "Date"
            }
            dtSales.Columns.Add(column)
            ' Create second column.
            column = New DataColumn() With {
                .DataType = Type.GetType("System.Int32"),
                .ColumnName = "UnitsSold"
            }
            dtSales.Columns.Add(column)
            For i As Integer = 0 To dictFinal.Count - 1
                Dim row As DataRow = dtSales.NewRow
                row("Date") = dictFinal.Keys(i)
                row("UnitsSold") = dictFinal.Values(i)
                dtSales.Rows.Add(row)
            Next
            Return dtSales
        End Using

    End Function

    ''' <summary> Gets products by category. </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="categoryID">       Identifier for the category. </param>
    ''' <param name="connectionString"> The connection string. </param>
    ''' <returns> The products by category. </returns>
    <CodeAnalysis.SuppressMessage("Security", "CA2100:Review SQL queries for security vulnerabilities", Justification:="<Pending>")>
    Public Shared Function GetProductsByCategory(ByVal categoryID As Integer, ByVal connectionString As String) As List(Of Product)

        Using conProducts As New OleDbConnection(connectionString)
            conProducts.Open()
            Dim cmdProducts As New OleDbCommand() With {
                .Connection = conProducts,
                .CommandText = "SELECT ID, ProdName, SalesMonth1, SalesMonth2, SalesMonth3, SalesCurrentMonth, CategoryID, State, RetailPrice FROM Products WHERE CategoryID = " & categoryID
            }
            Dim lstProducts As New List(Of Product)
            Dim dbrProduct As OleDbDataReader = cmdProducts.ExecuteReader
            While dbrProduct.Read
                Dim blnNullCheck As Boolean = False 'prepeare boolean to handle null values
                For intFields As Integer = 0 To dbrProduct.FieldCount - 1 'Iterate through fields to check for null
                    If DBNull.Value.Equals(dbrProduct(intFields)) Then 'If db return value is null 
                        blnNullCheck = True                                           'Set boolean to true
                    End If
                Next
                If Not blnNullCheck Then 'If no null values returned in row then create object else goto next record
                    Dim newProduct As New Product() With {
                        .ID = dbrProduct.GetInt32(0),
                        .Name = dbrProduct.GetString(1),
                        .CategoryID = dbrProduct.GetInt32(6),
                        .State = CType(dbrProduct.GetInt32(7), ApplicationState),
                        .RetailPrice = dbrProduct.GetDouble(8),
                       .UnitSales = New List(Of Integer) From {
                            dbrProduct.GetInt32(2),
                            dbrProduct.GetInt32(3),
                            dbrProduct.GetInt32(4),
                            dbrProduct.GetInt32(5)}
                       }
                    lstProducts.Add(newProduct)
                End If
            End While
            dbrProduct.Close()
            Return lstProducts
        End Using

    End Function

#End Region

End Class
