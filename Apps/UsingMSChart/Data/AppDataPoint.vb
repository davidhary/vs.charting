''' <summary> An application data point. Holds various graphing points. </summary>
''' <remarks> David, 2020-10-24. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1036:Override methods on comparable types", Justification:="<Pending>")>
<CodeAnalysis.SuppressMessage("Performance", "CA1815:Override equals and operator equals on value types", Justification:="<Pending>")>
Public Structure AppDataPoint
    Implements IComparable

    ''' <summary> Gets or sets the data key. </summary>
    ''' <value> The data key. </value>
    Public Property DataKey() As Object

    ''' <summary> Gets or sets the data value. </summary>
    ''' <value> The data value. </value>
    Public Property DataValue() As Double

    ''' <summary> Gets or sets the tag. </summary>
    ''' <value> The tag. </value>
    Public Property Tag() As Object

    ''' <summary>
    ''' Compares the current instance with another object of the same type and returns an integer
    ''' that indicates whether the current instance precedes, follows, or occurs in the same position
    ''' in the sort order as the other object.
    ''' </summary>
    ''' <remarks> David, 2020-10-24. </remarks>
    ''' <param name="obj"> An object to compare with this instance. </param>
    ''' <returns>
    ''' A value that indicates the relative order of the objects being compared. The return value has
    ''' these meanings: Value Meaning Less than zero This instance precedes <paramref name="obj" />
    ''' in the sort order. Zero This instance occurs in the same position in the sort order as
    ''' <paramref name="obj" />. Greater than zero This instance follows <paramref name="obj" /> in
    ''' the sort order.
    ''' </returns>
    Public Function CompareTo(ByVal obj As Object) As Integer Implements System.IComparable.CompareTo

        If Me.DataValue < CType(obj, AppDataPoint).DataValue Then
            Return 1
        ElseIf Me.DataValue > CType(obj, AppDataPoint).DataValue Then
            Return -1
        Else
            Return 0
        End If
    End Function

End Structure
