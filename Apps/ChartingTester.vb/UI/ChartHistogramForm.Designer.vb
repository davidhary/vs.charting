<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChartHistogramForm
    Inherits System.Windows.Forms.Form

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Me._HistogramChart = New isr.Visuals.Charting.LineChartControl()
        CType(Me._HistogramChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_HistogramChart
        '
        ChartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount
        ChartArea1.AxisX.IsLabelAutoFit = False
        ChartArea1.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        ChartArea1.AxisX.LabelStyle.Format = "F0"
        ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer))
        ChartArea1.AxisX.ScaleView.SmallScrollSize = 1.0R
        ChartArea1.AxisX.ScaleView.Zoomable = False
        ChartArea1.AxisX.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        ChartArea1.AxisY.IsLabelAutoFit = False
        ChartArea1.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.FromArgb(CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer), CType(CType(200, Byte), Integer))
        ChartArea1.AxisY.TitleFont = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        ChartArea1.Name = "Area"
        Me._HistogramChart.ChartAreas.Add(ChartArea1)
        Me._HistogramChart.Dock = System.Windows.Forms.DockStyle.Fill
        Me._HistogramChart.Location = New System.Drawing.Point(0, 0)
        Me._HistogramChart.Name = "_HistogramChart"
        Me._HistogramChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me._HistogramChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.DarkRed, System.Drawing.Color.OliveDrab, System.Drawing.Color.DarkOrange, System.Drawing.Color.Purple, System.Drawing.Color.Turquoise, System.Drawing.Color.Green, System.Drawing.Color.Crimson, System.Drawing.Color.RoyalBlue, System.Drawing.Color.Sienna, System.Drawing.Color.Teal, System.Drawing.Color.YellowGreen, System.Drawing.Color.SandyBrown, System.Drawing.Color.DeepSkyBlue, System.Drawing.Color.Brown, System.Drawing.Color.Cyan}
        Me._HistogramChart.Size = New System.Drawing.Size(800, 450)
        Me._HistogramChart.TabIndex = 0
        Me._HistogramChart.Text = "Histogram Chart"
        '
        'ChartHistogramForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me._HistogramChart)
        Me.Name = "ChartHistogramForm"
        Me.Text = "ChartHistogramForm"
        CType(Me._HistogramChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _HistogramChart As LineChartControl
End Class
