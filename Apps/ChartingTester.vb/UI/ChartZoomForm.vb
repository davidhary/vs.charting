Imports isr.Visuals.Charting

''' <summary> Form for viewing the chart zoom. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public Class ChartZoomForm

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()

        Me.InitializeComponent()

        Me._ZoomChart.InitializeKnownState(False)
        Me._ZoomChart.SetupChartZoomExample()
        Me._ZoomChart.ToggleDashedZoomRectangleMouseHandlers(True)
        Me._ZoomChart.PopulateContextMenu()
        Me._ZoomChart.ToggleContextMenu(True)

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me._ZoomChart?.ToggleDashedZoomRectangleMouseHandlers(False)
                Me._ZoomChart?.ToggleContextMenu(False)
                Me.components?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


End Class
