<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ChartZoomForm
    Inherits System.Windows.Forms.Form

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._ZoomChart = New isr.Visuals.Charting.LineChartControl()
        CType(Me._ZoomChart, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ZoomChart
        '
        Me._ZoomChart.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ZoomChart.Location = New System.Drawing.Point(0, 0)
        Me._ZoomChart.Name = "_ZoomChart"
        Me._ZoomChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None
        Me._ZoomChart.PaletteCustomColors = New System.Drawing.Color() {System.Drawing.Color.Blue, System.Drawing.Color.DarkRed, System.Drawing.Color.OliveDrab, System.Drawing.Color.DarkOrange, System.Drawing.Color.Purple, System.Drawing.Color.Turquoise, System.Drawing.Color.Green, System.Drawing.Color.Crimson, System.Drawing.Color.RoyalBlue, System.Drawing.Color.Sienna, System.Drawing.Color.Teal, System.Drawing.Color.YellowGreen, System.Drawing.Color.SandyBrown, System.Drawing.Color.DeepSkyBlue, System.Drawing.Color.Brown, System.Drawing.Color.Cyan}
        Me._ZoomChart.Size = New System.Drawing.Size(800, 450)
        Me._ZoomChart.TabIndex = 0
        Me._ZoomChart.Text = "Zoom Chart"
        '
        'ChartZoomForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me._ZoomChart)
        Me.Name = "ChartZoomForm"
        Me.Text = "ChartZoomForm"
        CType(Me._ZoomChart, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _ZoomChart As LineChartControl
End Class
