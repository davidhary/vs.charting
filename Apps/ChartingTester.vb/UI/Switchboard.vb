Imports System.Runtime.CompilerServices
Imports System.ComponentModel
Imports isr.Visuals.Charting.Tester.ExceptionExtensions

''' <summary> Switches between test panels. </summary>
''' <remarks>
''' (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 4/5/2014, . based on legacy code. </para>
''' </remarks>
Public Class Switchboard

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of this class. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializeComponent()
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary> Event handler. Called by form for load events. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            Me._MessagesTextBox.AddMessage("Loading...")

            ' instantiate form objects
            Me.PopulateTestPanelSelector()

            ' set the form caption
            Me.Text = Extensions.BuildDefaultCaption("TESTER")

            ' center the form
            Me.CenterToScreen()

        Catch

            Me._MessagesTextBox.AddMessage("Exception...")

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me._MessagesTextBox.AddMessage("Loaded.")

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Closes the form and exits the application. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ExitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ExitButton.Click
        Try
            Me.Close()
        Catch ex As Exception
            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        End Try
    End Sub

    ''' <summary> Cancel button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CancelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _CancelButton.Click
        Me.Close()
    End Sub

    ''' <summary> Tests button click. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TestButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _TestButton.Click
        If True Then
            Dim fi As New System.IO.FileInfo(System.Windows.Forms.Application.ExecutablePath)
            Debug.Print(fi.FullName)
        End If
    End Sub

#End Region

#Region " SWITCH BOARD "

    ''' <summary> Descriptive Enumeration for test forms. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Enum TestPanel

        ''' <summary> An enum constant representing the chart Zoom option. </summary>
        <System.ComponentModel.Description("Chart Zoom")> ChartZoom

        ''' <summary> An enum constant representing the chart histogram option. </summary>
        <System.ComponentModel.Description("Chart Histogram")> ChartHistogram

    End Enum

    ''' <summary> Open selected items. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _OpenButton.Click

        Try

            Select Case CType(CType(Me._ActionsComboBox.SelectedItem, KeyValuePair(Of [Enum], String)).Key, TestPanel)

                Case TestPanel.ChartZoom

                    Using myForm As New ChartZoomForm
                        myForm.ShowDialog()
                    End Using

                Case TestPanel.ChartHistogram

                    Using myForm As New ChartHistogramForm
                        myForm.ShowDialog()
                    End Using

            End Select

        Catch ex As Exception

            ex.Data.Add("@isr", "Unhandled Exception.")
            MessageBox.Show(ex.ToFullBlownString, "Unhandled Exception",
                            MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)

        Finally
        End Try

    End Sub

    ''' <summary> Populates the list of test panels. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    Private Sub PopulateTestPanelSelector()

        Me._ActionsComboBox.DataSource = Nothing
        Me._ActionsComboBox.Items.Clear()
        Me._ActionsComboBox.DataSource = Extensions.ValueDescriptionPairs(GetType(TestPanel))
        Me._ActionsComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
        Me._ActionsComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)

    End Sub

#End Region

End Class

Public Module Extensions

    ''' <summary> Adds a message to 'message'. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="box">     The box control. </param>
    ''' <param name="message"> The message. </param>
    <Extension()>
    Public Sub AddMessage(ByVal box As TextBox, ByVal message As String)
        If box IsNot Nothing Then
            box.SelectionStart = box.Text.Length
            box.SelectionLength = 0
            box.SelectedText = message & Environment.NewLine
        End If
    End Sub

    ''' <summary>
    ''' Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
    ''' </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
    <Extension()>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then Return String.Empty

        Dim candidate As String = value.ToString()
        Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
        Dim attributes As DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(DescriptionAttribute), False), DescriptionAttribute())

        If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
            candidate = attributes(0).Description
        End If
        Return candidate

    End Function

    ''' <summary> Gets a Key Value Pair description item. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns> A list of. </returns>
    <Extension()>
    Public Function ValueDescriptionPair(ByVal value As System.Enum) As System.Collections.Generic.KeyValuePair(Of System.Enum, String)
        Return New System.Collections.Generic.KeyValuePair(Of System.Enum, String)(value, value.Description)
    End Function

    ''' <summary> Value description pairs. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="type"> The type. </param>
    ''' <returns> An IList. </returns>
    <Extension()>
    Public Function ValueDescriptionPairs(ByVal type As Type) As IList

        Dim keyValuePairs As ArrayList = New ArrayList()
        For Each value As System.Enum In System.Enum.GetValues(type)
            keyValuePairs.Add(value.ValueDescriptionPair())
        Next value
        Return keyValuePairs

    End Function

    ''' <summary> Builds the default caption. </summary>
    ''' <remarks> David, 2020-10-25. </remarks>
    ''' <param name="subtitle"> The subtitle. </param>
    ''' <returns> System.String. </returns>
    Public Function BuildDefaultCaption(ByVal subtitle As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.Append(My.Application.Info.Title)
        builder.Append(" ")
        builder.Append(My.Application.Info.Version.ToString)
        If My.Application.Info.Version.Major < 1 Then
            builder.Append(".")
            Select Case My.Application.Info.Version.Minor
                Case 0
                    builder.Append("Alpha")
                Case 1
                    builder.Append("Beta")
                Case 2 To 8
                    builder.Append($"RC{My.Application.Info.Version.Minor - 1}")
                Case Else
                    builder.Append("Gold")
            End Select
        End If
        If Not String.IsNullOrWhiteSpace(subtitle) Then
            builder.Append(": ")
            builder.Append(subtitle)
        End If
        Return builder.ToString

    End Function

End Module
