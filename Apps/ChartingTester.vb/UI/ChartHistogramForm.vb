Imports System.ComponentModel
Imports System.Text
Imports System.Windows.Forms.DataVisualization.Charting

Imports isr.Core.Capsule.ExceptionExtensions
Imports isr.Core.Engineering
Imports isr.Core.RandomExtensions

''' <summary> Form for viewing the chart histogram. </summary>
''' <remarks> David, 2020-10-26. </remarks>
Public Class ChartHistogramForm

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    Public Sub New()
        Me.InitializeComponent()

        Dim histogram As New HistogramBindingList(-3, 3, 31)
        histogram.Initialize()
        Dim bindingList As New BindingList(Of Windows.Point)
        Dim rnd As New Random
        For i As Integer = 1 To 10000
            Dim p As New Windows.Point(i, rnd.NextNormal)
            bindingList.Add(p)
            histogram.Update(p.Y)
        Next

        Me._HistogramChart.InitializeKnownState(False)
        Me._HistogramChart.BindLineSeries("Histogram", SeriesChartType.FastLine, histogram)
        Me._HistogramChart.ChartArea.AxisX.Minimum = -3
        Me._HistogramChart.ChartArea.AxisX.Maximum = 3
        Me._HistogramChart.ChartArea.AxisX.Interval = 1
        AddHandler histogram.ListChanged, AddressOf Me.Histogram_ListChanged


    End Sub

    ''' <summary> Event handler. Called by Histogram for list changed events. </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      List changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub Histogram_ListChanged(sender As Object, e As ListChangedEventArgs)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of Object, ListChangedEventArgs)(AddressOf Me.Histogram_ListChanged), New Object() {sender, e})
        Else
            Dim histogram As isr.Core.Engineering.HistogramBindingList = TryCast(sender, isr.Core.Engineering.HistogramBindingList)
            If histogram Is Nothing OrElse e Is Nothing Then Return
            Dim activity As String = $"handling {NameOf(isr.Core.Engineering.HistogramBindingList)}.{e.ListChangedType} change"
            Try
                If Me.InvokeRequired Then
                    Me.Invoke(New Action(Of Object, ListChangedEventArgs)(AddressOf Me.Histogram_ListChanged), New Object() {sender, e})
                Else
                    Me._HistogramChart.DataBind()
                End If
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, $"Exception occurred {activity}: {ex.ToFullBlownString}")
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-26. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                Me.components?.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


End Class
