using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Charting.Tester
{
    [DesignerGenerated()]
    public partial class ChartHistogramForm : Form
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var ChartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            _HistogramChart = new LineChartControl();
            ((System.ComponentModel.ISupportInitialize)_HistogramChart).BeginInit();
            SuspendLayout();
            // 
            // _HistogramChart
            // 
            ChartArea1.AxisX.IntervalAutoMode = System.Windows.Forms.DataVisualization.Charting.IntervalAutoMode.VariableCount;
            ChartArea1.AxisX.IsLabelAutoFit = false;
            ChartArea1.AxisX.LabelStyle.Font = new Font("Microsoft Sans Serif", 9.0f);
            ChartArea1.AxisX.LabelStyle.Format = "F0";
            ChartArea1.AxisX.MajorGrid.LineColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)));
            ChartArea1.AxisX.ScaleView.SmallScrollSize = 1.0d;
            ChartArea1.AxisX.ScaleView.Zoomable = false;
            ChartArea1.AxisX.TitleFont = new Font("Microsoft Sans Serif", 9.0f);
            ChartArea1.AxisY.IsLabelAutoFit = false;
            ChartArea1.AxisY.LabelStyle.Font = new Font("Microsoft Sans Serif", 9.0f);
            ChartArea1.AxisY.MajorGrid.LineColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)), Conversions.ToInteger(Conversions.ToByte(200)));
            ChartArea1.AxisY.TitleFont = new Font("Microsoft Sans Serif", 9.0f);
            ChartArea1.Name = "Area";
            _HistogramChart.ChartAreas.Add(ChartArea1);
            _HistogramChart.Dock = DockStyle.Fill;
            _HistogramChart.Location = new Point(0, 0);
            _HistogramChart.Name = "_HistogramChart";
            _HistogramChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            _HistogramChart.PaletteCustomColors = new Color[] { Color.Blue, Color.DarkRed, Color.OliveDrab, Color.DarkOrange, Color.Purple, Color.Turquoise, Color.Green, Color.Crimson, Color.RoyalBlue, Color.Sienna, Color.Teal, Color.YellowGreen, Color.SandyBrown, Color.DeepSkyBlue, Color.Brown, Color.Cyan };
            _HistogramChart.Size = new Size(800, 450);
            _HistogramChart.TabIndex = 0;
            _HistogramChart.Text = "Histogram Chart";
            // 
            // ChartHistogramForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(_HistogramChart);
            Name = "ChartHistogramForm";
            Text = "ChartHistogramForm";
            ((System.ComponentModel.ISupportInitialize)_HistogramChart).EndInit();
            ResumeLayout(false);
        }

        private LineChartControl _HistogramChart;
    }
}
