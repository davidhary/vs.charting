using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Visuals.Charting.Tester.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Charting.Tester
{

    /// <summary> Switches between test panels. </summary>
    /// <remarks>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-04-05, . based on legacy code. </para>
    /// </remarks>
    public partial class Switchboard
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        public Switchboard() : base()
        {
            this.InitializeComponent();
            this.__CancelButton.Name = "_CancelButton";
            this.__ExitButton.Name = "_ExitButton";
            this.__TestButton.Name = "_TestButton";
            this.__OpenButton.Name = "_OpenButton";
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Event handler. Called by form for load events. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {

                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                this._MessagesTextBox.AddMessage( "Loading..." );

                // instantiate form objects
                this.PopulateTestPanelSelector();

                // set the form caption
                this.Text = Extensions.BuildDefaultCaption( "TESTER" );

                // center the form
                this.CenterToScreen();
            }
            catch
            {
                this._MessagesTextBox.AddMessage( "Exception..." );

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this._MessagesTextBox.AddMessage( "Loaded." );
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Closes the form and exits the application. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExitButton_Click( object sender, EventArgs e )
        {
            try
            {
                this.Close();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
        }

        /// <summary> Cancel button click. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void CancelButton_Click( object sender, EventArgs e )
        {
            this.Close();
        }

        /// <summary> Tests button click. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TestButton_Click( object sender, EventArgs e )
        {
            if ( true )
            {
                var fi = new System.IO.FileInfo( Application.ExecutablePath );
                Debug.Print( fi.FullName );
            }
        }

        #endregion

        #region " SWITCH BOARD "

        /// <summary> Descriptive Enumeration for test forms. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        private enum TestPanel
        {

            /// <summary> An enum constant representing the chart Zoom option. </summary>
            [Description( "Chart Zoom" )]
            ChartZoom,

            /// <summary> An enum constant representing the chart histogram option. </summary>
            [Description( "Chart Histogram" )]
            ChartHistogram
        }

        /// <summary> Open selected items. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenButton_Click( object sender, EventArgs e )
        {
            try
            {
                switch ( ( TestPanel ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ActionsComboBox.SelectedItem).Key ) )
                {
                    case TestPanel.ChartZoom:
                        {
                            using var myForm = new ChartZoomForm();
                            _ = myForm.ShowDialog();

                            break;
                        }

                    case TestPanel.ChartHistogram:
                        {
                            using var myForm = new ChartHistogramForm();
                            _ = myForm.ShowDialog();

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
            }
        }

        /// <summary> Populates the list of test panels. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        private void PopulateTestPanelSelector()
        {
            this._ActionsComboBox.DataSource = null;
            this._ActionsComboBox.Items.Clear();
            this._ActionsComboBox.DataSource = typeof( TestPanel ).ValueDescriptionPairs();
            this._ActionsComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._ActionsComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
        }

        #endregion

    }

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2020-11-02. </remarks>
    public static class Extensions
    {

        /// <summary> Adds a message to 'message'. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="box">     The box control. </param>
        /// <param name="message"> The message. </param>
        public static void AddMessage( this TextBox box, string message )
        {
            if ( box is object )
            {
                box.SelectionStart = box.Text.Length;
                box.SelectionLength = 0;
                box.SelectedText = message + Environment.NewLine;
            }
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="value"> The <see cref="System.Enum"/> type value. </param>
        /// <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public static string Description( this Enum value )
        {
            if ( value is null )
                return string.Empty;
            string candidate = value.ToString();
            var fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary> Gets a Key Value Pair description item. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="value"> The <see cref="System.Enum"/> type value. </param>
        /// <returns> A list of. </returns>
        public static KeyValuePair<Enum, string> ValueDescriptionPair( this Enum value )
        {
            return new KeyValuePair<Enum, string>( value, value.Description() );
        }

        /// <summary> Value description pairs. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns> An IList. </returns>
        public static IList ValueDescriptionPairs( this Type type )
        {
            var keyValuePairs = new ArrayList();
            foreach ( Enum value in Enum.GetValues( type ) )
                _ = keyValuePairs.Add( value.ValueDescriptionPair() );
            return keyValuePairs;
        }

        /// <summary> Builds the default caption. </summary>
        /// <remarks> David, 2020-10-25. </remarks>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public static string BuildDefaultCaption( string subtitle )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( My.MyProject.Application.Info.Title );
            _ = builder.Append( " " );
            _ = builder.Append( My.MyProject.Application.Info.Version.ToString() );
            if ( My.MyProject.Application.Info.Version.Major < 1 )
            {
                _ = builder.Append( "." );
                switch ( My.MyProject.Application.Info.Version.Minor )
                {
                    case 0:
                        {
                            _ = builder.Append( "Alpha" );
                            break;
                        }

                    case 1:
                        {
                            _ = builder.Append( "Beta" );
                            break;
                        }

                    case var @case when 2 <= @case && @case <= 8:
                        {
                            _ = builder.Append( $"RC{My.MyProject.Application.Info.Version.Minor - 1}" );
                            break;
                        }

                    default:
                        {
                            _ = builder.Append( "Gold" );
                            break;
                        }
                }
            }

            if ( !string.IsNullOrWhiteSpace( subtitle ) )
            {
                _ = builder.Append( ": " );
                _ = builder.Append( subtitle );
            }

            return builder.ToString();
        }
    }
}
