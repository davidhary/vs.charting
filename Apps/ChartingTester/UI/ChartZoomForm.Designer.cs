using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.Visuals.Charting.Tester
{
    [DesignerGenerated()]
    public partial class ChartZoomForm : Form
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ZoomChart = new LineChartControl();
            ((System.ComponentModel.ISupportInitialize)_ZoomChart).BeginInit();
            SuspendLayout();
            // 
            // _ZoomChart
            // 
            _ZoomChart.Dock = DockStyle.Fill;
            _ZoomChart.Location = new Point(0, 0);
            _ZoomChart.Name = "_ZoomChart";
            _ZoomChart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            _ZoomChart.PaletteCustomColors = new Color[] { Color.Blue, Color.DarkRed, Color.OliveDrab, Color.DarkOrange, Color.Purple, Color.Turquoise, Color.Green, Color.Crimson, Color.RoyalBlue, Color.Sienna, Color.Teal, Color.YellowGreen, Color.SandyBrown, Color.DeepSkyBlue, Color.Brown, Color.Cyan };
            _ZoomChart.Size = new Size(800, 450);
            _ZoomChart.TabIndex = 0;
            _ZoomChart.Text = "Zoom Chart";
            // 
            // ChartZoomForm
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(_ZoomChart);
            Name = "ChartZoomForm";
            Text = "ChartZoomForm";
            ((System.ComponentModel.ISupportInitialize)_ZoomChart).EndInit();
            ResumeLayout(false);
        }

        private LineChartControl _ZoomChart;
    }
}
