using System.Diagnostics;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Visuals.Charting.Tester
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Form for viewing the chart zoom. </summary>
    /// <remarks> David, 2020-10-26. </remarks>
    public partial class ChartZoomForm
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        public ChartZoomForm()
        {
            this.InitializeComponent();
            this._ZoomChart.InitializeKnownState( false );
            this._ZoomChart.SetupChartZoomExample();
            this._ZoomChart.ToggleDashedZoomRectangleMouseHandlers( true );
            this._ZoomChart.PopulateContextMenu();
            this._ZoomChart.ToggleContextMenu( true );
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        /// resources; <see langword="false" /> to release only unmanaged
        /// resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this._ZoomChart?.ToggleDashedZoomRectangleMouseHandlers( false );
                    this._ZoomChart?.ToggleContextMenu( false );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }
    }
}
