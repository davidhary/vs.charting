﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Visuals.Charting.Tester.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public ChartHistogramForm m_ChartHistogramForm;

            public ChartHistogramForm ChartHistogramForm
            {
                [DebuggerHidden]
                get
                {
                    m_ChartHistogramForm = Create__Instance__(m_ChartHistogramForm);
                    return m_ChartHistogramForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ChartHistogramForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ChartHistogramForm);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public ChartZoomForm m_ChartZoomForm;

            public ChartZoomForm ChartZoomForm
            {
                [DebuggerHidden]
                get
                {
                    m_ChartZoomForm = Create__Instance__(m_ChartZoomForm);
                    return m_ChartZoomForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_ChartZoomForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_ChartZoomForm);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard m_Switchboard;

            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    m_Switchboard = Create__Instance__(m_Switchboard);
                    return m_Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Switchboard);
                }
            }
        }
    }
}